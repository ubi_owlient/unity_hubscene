﻿using Commands.Settings;
using Menu.Buttons;
using Service;
using UnityEngine;
using ViewModels;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SettingsView : AView
    {
        [SerializeField]
        private ImageButton _showButton = null;

        [SerializeField]
        [Tooltip("Delay in seconds.")]
        private double _settingsPanelDelay = 3.0;
        
        private Animator _settingsPanelAnim = null;
        private float _timer                = 0.0f;
        private bool _areSettingsDisplayed  = false;

        public override IViewModel model
        {
            get
            {
                return null;
            }
        }

        void Start()
        {
            _settingsPanelAnim = GetComponent<Animator>();
            
            _showButton.onClick += OnShowClick;
        }
        
        void Update()
        {
            if ( _areSettingsDisplayed )
            {
                _timer += Time.unscaledDeltaTime;
                float seconds = _timer % 60;
                if ( seconds >= _settingsPanelDelay )
                {
                    OnTimerElapsed();
                }
            }
        }

        void OnDestroy()
        {
            _showButton.onClick -= OnShowClick;
        }

        private void OnShowClick(ABaseButton button)
        {
            ShowSettingsPanel();
        }

        private void OnTimerElapsed()
        {
            if ( ServiceManager.Instance.GetService<SettingService>().isSettingContentOpened == false )
            {
                HideSettingsPanel();
            }
        }

        private void ShowSettingsPanel()
        {
            if ( _areSettingsDisplayed == false )
            {
                _settingsPanelAnim.SetTrigger( "OpenSettingsPanel" );

                _timer = 0.0f;

                _areSettingsDisplayed = true;

                ServiceManager.Instance.GetService<DispatcherService>().Dispatch( new OpenSettingCommand( this ) );
            }
        }

        private void HideSettingsPanel()
        {
            if ( _areSettingsDisplayed )
            {
                _settingsPanelAnim.SetTrigger( "CloseSettingsPanel" );
                
                _areSettingsDisplayed = false;

                ServiceManager.Instance.GetService<DispatcherService>().Dispatch( new CloseSettingCommand( this ) );
            }
        }
    }
}
