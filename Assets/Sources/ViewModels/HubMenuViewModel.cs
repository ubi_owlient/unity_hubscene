﻿using Enums;
using FeatureModels;
using Models;
using System.ComponentModel;

namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HubMenuViewModel : AViewModel
    {
        private ActivityModel _activity = null;

        public bool isOpen
        {
            get
            {
                HubModel hubModel = GetModel<HubModel>();
                if ( hubModel != null &&
                     hubModel.currentActivity == _activity )
                {
                    return true;
                }

                return false;
            }
        }

        public string menuTitle
        {
            get
            {
                if ( _activity != null )
                {
                    return _activity.title;
                }

                return string.Empty;
            }
        }

        public string menuDescription
        {
            get
            {
                if ( _activity != null )
                {
                    return _activity.description;
                }

                return string.Empty;
            }
        }

        public ActivityModel activity
        {
            get
            {
                return _activity;
            }
        }
        
        public HubMenuViewModel(ActivityEnum activity, params IFeatureModel[] models) :
        base( models )
        {
            _identifier = string.Empty;

            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                _activity = hubModel.GetActivity( activity );
                hubModel.PropertyChanged += OnHubMenuPropertyChanged;
            }
        }

        protected override void OnDispose()
        {
            base.OnDispose();

            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.PropertyChanged -= OnHubMenuPropertyChanged;
            }
        }

        private void OnHubMenuPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if ( args.PropertyName == "currentActivity" )
            {
                RaisePropertyChanged( "isOpen" );
            }
        }
    }
}
