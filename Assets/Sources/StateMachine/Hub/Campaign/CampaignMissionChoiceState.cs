﻿using Models;

namespace FSM.Hub
{
    /// <summary>
    /// Athor: jlarbi
    /// 
    /// NOTE: This will display and allow user to choose a campaign mission.
    /// </summary>
    public class CampaignMissionChoiceState : AActivityState
    {
        private CampaignModel _campaignModel = null;

        private MissionModel _selectedMission = null;

        public CampaignMissionChoiceState()
        {
            _stateName = "Mission choice";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);
            
            // Update the proper page to display another page content
            // with all campaign's missions to choose the mission.
            if ( _subMenu != null )
            {

            }
        }

        public override void OnExit()
        {
            base.OnExit();

            _selectedMission = null;
        }

        public override Transition GetTransition()
        {
            if ( _selectedMission != null )
            {
                // TO DO: Go To team selection state.
            }

            return base.GetTransition();
        }
    }
}
