﻿using Service;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class GuildManager : MonoBehaviour
    {
        [SerializeField]
        private List<string> _iconNames = new List<string>();

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<GuildService>().Initialize( 15 , _iconNames );
        }
    }
}
