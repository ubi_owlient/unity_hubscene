﻿using Menu.Buttons;
using Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Widgets;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MessageTileView : MonoBehaviour, IItemWidget
    {
        [Header("Status")]
        [SerializeField]
        private Image _status        = null;

        [SerializeField]
        private Sprite _readImage    = null;

        [SerializeField]
        private Sprite _unreadImage  = null;

        [Header("Priority")]
        [SerializeField]
        private Image _priorityImage = null;

        [Header("Info")]
        [SerializeField]
        private Text   _from         = null;

        [SerializeField]
        private Text   _title        = null;

        [SerializeField]
        private Text   _message      = null;

        [Header("Rewards")]
        [SerializeField]
        private RewardPopulator _rewards = null;

        [Header("Selection")]
        [SerializeField]
        private StandardButton _selectButton = null;

        private MessageModel _model = null;

        public MessageModel model
        {
            get
            {
                return _model;
            }
        }

        public uint id
        {
            get
            {
                if ( _model != null )
                {
                    return _model.id;
                }

                return 0;
            }
        }

        public event Action<IItemWidget> onClick;

        void Start()
        {
            if ( _selectButton != null )
            {
                _selectButton.onClick += OnMessageSelected;
            }
        }

        void OnDestroy()
        {
            Dispose();
        }

        public void SetMessage(MessageModel model)
        {
            _model = model;

            if ( model.isRead )
            {
                SetAsRead();
            }
            else
            {
                SetAsUnread();
            }

            if ( _priorityImage != null )
            {
                _priorityImage.color = ColorHelper.FromMessagePriority( model.priority );
            }

            if ( _from != null )
            {
                _from.text = model.sender;
            }

            if ( _title != null )
            {
                _title.text = model.title;
            }

            if ( _message != null )
            {
                _message.text = model.message;
            }

            if ( _rewards != null &&
                 model.rewards != null )
            {
                _rewards.Populate( model.rewards );
            }
        }

        public void SetAsRead()
        {
            if ( _status != null )
            {
                _model.isRead  = true;
                _status.sprite = _readImage;
            }
        }

        public void SetAsUnread()
        {
            if ( _status != null )
            {
                _model.isRead  = false;
                _status.sprite = _unreadImage;
            }
        }

        private void OnMessageSelected(ABaseButton button)
        {
            if ( onClick != null )
            {
                onClick( this );
            }

            SetAsRead();
        }

        public void Dispose()
        {
            if ( _selectButton != null )
            {
                _selectButton.onClick -= OnMessageSelected;
            }
        }
    }
}
