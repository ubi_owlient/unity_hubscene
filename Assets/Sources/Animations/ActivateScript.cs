﻿using Service;
using UnityEngine;
using UnityEngine.Serialization;

namespace Animations
{
    public class ActivateScript : Receiver
    {
        [FormerlySerializedAs("estVisible")]
        public bool isVisible;
        public bool samePosition = false;
        public GameObject objParam;

        public string inCreateEvent;
        public string inHideEvent;
        public string inShowEvent;
        public string[] inHideEvents;
        public string[] inShowEvents;


        // Use this for initialization
        void Start()
        {
            if (objParam == null)
            {
                objParam = this.gameObject;
            }
            if (isVisible)
            {
                objParam.SetActive(true);
            }
            else
            {
                objParam.SetActive(false);
            }

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            inCreateEvent = animService.PrepareEventName(inCreateEvent, gameObject);
            inHideEvent   = animService.PrepareEventName(inHideEvent, gameObject);
            inShowEvent   = animService.PrepareEventName(inShowEvent, gameObject);
            animService.Register(inHideEvent, this);
            animService.Register(inShowEvent, this);
            animService.Register(inCreateEvent, this);

            if (inHideEvents != null)
            {
                for (int i = 0; i < inHideEvents.Length; ++i)
                {
                    inHideEvents[i] = animService.PrepareEventName(inHideEvents[i], gameObject);
                    ServiceManager.Instance.GetService<AnimationService>().Register(inHideEvents[i], this);
                }
            }

            if (inShowEvents != null)
            {
                for (int i = 0; i < inShowEvents.Length; ++i)
                {
                    inShowEvents[i] = animService.PrepareEventName(inShowEvents[i], gameObject);
                    ServiceManager.Instance.GetService<AnimationService>().Register(inShowEvents[i], this);
                }
            }
        }
        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            animService.Unregister(inHideEvent, this);
            animService.Unregister(inShowEvent, this);
            animService.Unregister(inCreateEvent, this);
            if (inHideEvents != null)
            {
                for (int i = 0; i < inHideEvents.Length; ++i)
                {
                    animService.Unregister(inHideEvents[i], this);
                }
            }
            if (inShowEvents != null)
            {
                for (int i = 0; i < inShowEvents.Length; ++i)
                {
                    animService.Unregister(inShowEvents[i], this);
                }
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            if (inHideEvents != null)
            {
                for (int i = 0; i < inHideEvents.Length; ++i)
                {
                    if (inHideEvents[i].Equals(eventName))
                    {
                        objParam.SetActive(false);
                    }
                }
            }

            if (inShowEvents != null)
            {
                for (int i = 0; i < inShowEvents.Length; ++i)
                {
                    if (inShowEvents[i].Equals(eventName))
                    {
                        objParam.SetActive(true);
                    }
                }
            }

            if (eventName.Equals(inHideEvent))
            {
                objParam.SetActive(false);
            }

            if (eventName.Equals(inShowEvent))
            {
                if (samePosition)
                {
                    transform.position = obj.transform.position;
                    objParam.SetActive(true);
                }
                else
                {
                    objParam.SetActive(true);
                }
            }

            if (eventName.Equals(inCreateEvent))
            {

                if (samePosition)
                {
                    GameObject objVar = Instantiate(objParam) as GameObject;
                    objVar.SetActive(true);
                    objVar.transform.position = obj.transform.position;
                }
                else
                {
                    objParam.SetActive(true);
                }
            }
        }
    }
}