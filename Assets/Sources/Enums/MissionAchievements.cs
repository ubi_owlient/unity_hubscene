﻿using System;

namespace Enums
{
    /// <summary>
    /// Author: jlarbi.
    /// 
    /// Definition of the <see cref="MissionAchievements"/> enumeration.
    /// </summary>
    [Flags]
    public enum MissionAchievements : ushort
    {
        NONE            = 0x00,

        NO_DEAD         = 0x01,
        
        TRIGGERED_ORDER = 0x02,

        IN_TIME         = 0x04,

        ALL             = NO_DEAD | TRIGGERED_ORDER | IN_TIME,
    }
}
