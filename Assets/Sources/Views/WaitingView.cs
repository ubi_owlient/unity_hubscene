﻿using Service;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class WaitingView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _root = null;

        [SerializeField]
        private List<string> _waitBackground = new List<string>();

        [SerializeField]
        private Image _background = null;

        [SerializeField]
        private float _delay = 5.0f;

        private DateTime _lastUpdateTime;

        void Start()
        {
            _lastUpdateTime = DateTime.UtcNow;

            LoadingService loadingService = ServiceManager.Instance.GetService<LoadingService>();
            loadingService.showWaitingScreenRequired += OnShowWaitingScreenRequired;
            loadingService.hideWaitingScreenRequired += OnHideWaitingScreenRequired;
        }

        void OnDestroy()
        {
            LoadingService loadingService = ServiceManager.Instance.GetService<LoadingService>();
            loadingService.showWaitingScreenRequired -= OnShowWaitingScreenRequired;
            loadingService.hideWaitingScreenRequired -= OnHideWaitingScreenRequired;
        }

        void Update()
        {
            if ( _waitBackground.Count == 0 )
            {
                return;
            }

            DateTime now = DateTime.UtcNow;
            if ( (now - _lastUpdateTime).TotalSeconds > _delay )
            {
                int index = UnityEngine.Random.Range( 0, _waitBackground.Count - 1 );
                string backgroundName = _waitBackground[ index ];
                Texture2D texture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_BACKGROUND_PATH, backgroundName ) ) as Texture2D;
                _background.sprite = Sprite.Create( texture, new Rect( 0.0f, 0.0f, texture.width, texture.height ), new Vector2( 0.5f, 0.5f ) );
                _lastUpdateTime = now;
            }
        }

        private void OnShowWaitingScreenRequired()
        {
            _root.SetActive( true );
        }
        
        private void OnHideWaitingScreenRequired()
        {
            _root.SetActive( false );
        }
    }
}
