﻿using System;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IServiceManager
    {
        /// <summary>
        /// Retrieves the requested service.
        /// </summary>
        /// <param name="type">The service type to look for.</param>
        /// <returns>The found service, NULL otherwise.</returns>
        IService GetService(Type type);

        /// <summary>
        /// Retrieves the requested service.
        /// </summary>
        /// <typeparam name="T">The service type</typeparam>
        /// <returns>The found service, NULL otherwise.</returns>
        T GetService<T>() where T : class, IService;
    }
}
