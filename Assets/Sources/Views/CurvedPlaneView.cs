﻿using FeatureModels;
using System.ComponentModel;
using UnityEngine;
using ViewModels;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CurvedPlaneView : AView
    {
        private Animator _animator = null;
        private bool     _isMenuOn = false;

        private CurvedMenuViewModel _viewModel = null;

        public override IViewModel model
        {
            get
            {
                return _viewModel;
            }
        }

        void Start()
        {
            _viewModel = new CurvedMenuViewModel( GetModel<HubModel>() );
            _viewModel.PropertyChanged += OnPropertyChanged;

            _animator = GetComponent<Animator>();
        }

        void OnDestroy()
        {
            if ( _viewModel != null )
            {
                _viewModel.PropertyChanged -= OnPropertyChanged;
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
        {
            switch (eventArgs.PropertyName)
            {
                case "isVisible":
                    {
                        if ( _viewModel.isVisible )
                        {
                            gameObject.SetActive( true );
                        }
                        else
                        {
                            gameObject.SetActive( false );
                        }
                    }
                    break;
                case "step":
                    {
                        if ( _viewModel.step != null )
                        {
                            
                        }
                    }
                    break;
                case "isOpen":
                    {
                        if ( _viewModel.isOpen )
                        {
                            OpenMenu();
                        }
                        else
                        {
                            CloseMenu();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Open menu method.
        /// </summary>
        private void OpenMenu()
        {
            if ( _isMenuOn == false )
            {
                _animator.SetTrigger( "PresentCurvedMenu" );
                _isMenuOn = true;
            }
        }

        /// <summary>
        /// Close menu method.
        /// </summary>
        private void CloseMenu()
        {
            if ( _isMenuOn )
            {
                _animator.SetTrigger( "HideCurvedMenu" );
                _isMenuOn = false;
            }
        }
    }
}
