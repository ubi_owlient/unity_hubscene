﻿using UnityEngine;

namespace Menu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ActivityMenuCanvasHandler : MonoBehaviour
    {
        private const float _worldWidth  = 970.0f;
        private const float _worldHeight = 600.0f;
        private const float _worldScale  = 0.002f;

        [SerializeField]
        private bool _isWorldSpace      = true;

        private Canvas _canvas;
        private bool _previousBoolValue = true;
        
        void Start()
        {
            _canvas = GetComponentInChildren<Canvas>();
            _previousBoolValue = _isWorldSpace;
        }

        void Update()
        {
            if ( _isWorldSpace != _previousBoolValue )
            {
                if ( _isWorldSpace )
                {
                    _canvas.renderMode = RenderMode.WorldSpace;
                    RectTransform transform = _canvas.transform as RectTransform;
                    transform.localRotation = Quaternion.identity;
                    transform.localPosition = Vector3.zero;
                    transform.localScale    = new Vector3( _worldScale, _worldScale, _worldScale );
                    transform.sizeDelta     = new Vector2( _worldWidth, _worldHeight );
                }
                else
                {
                    _canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                    RectTransform transform = _canvas.transform as RectTransform;
                    transform.localRotation = Quaternion.identity;
                    transform.localPosition = Vector3.zero;
                    transform.localScale    = Vector3.one;
                }

                _previousBoolValue = _isWorldSpace;
            }
        }
    }
}
