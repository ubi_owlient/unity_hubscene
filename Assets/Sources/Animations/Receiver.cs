﻿using UnityEngine;

namespace Animations
{
    public class Receiver : MonoBehaviour
    {
        public virtual void Receive(string eventName, GameObject obj) { }

        public virtual void Success() { }
        
        public virtual void AtEachKeyPress() { }
    }
}