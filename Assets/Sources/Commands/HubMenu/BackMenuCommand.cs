﻿using ViewModels;

namespace Commands.HubMenu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class BackMenuCommand : ACommand
    {
        public BackMenuCommand(IModelPresenter sender) : 
        base(sender)
        {

        }
    }
}
