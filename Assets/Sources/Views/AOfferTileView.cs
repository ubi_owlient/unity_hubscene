﻿using Menu.Buttons;
using Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class AOfferTileView<T> : MonoBehaviour, IItemWidget where T : AOffer
    {
        [SerializeField]
        private Text _offerTitle       = null;

        [SerializeField]
        private Text _offerFrequency   = null;

        [SerializeField]
        private Text _price            = null;

        [SerializeField]
        private Image _currency        = null;
        
        [SerializeField]
        private Image _offerImage      = null;

        [SerializeField]
        private Image _offerBackground = null;

        [SerializeField]
        private Image _border          = null;

        [SerializeField]
        private Image _topBar          = null;

        [SerializeField]
        private Image _bottomBar       = null;

        [SerializeField]
        private Image _bottomBand      = null;

        [SerializeField]
        private Image _outOfStock      = null;

        [SerializeField]
        private Image _lockedOverlay   = null;

        [SerializeField]
        private StandardButton _enterButton = null;

        private T _model = null;

        public T model
        {
            get
            {
                return _model;
            }
        }

        public uint id
        {
            get
            {
                if ( _model != null )
                {
                    return _model.offerId;
                }

                return 0;
            }
        }
        
        public event Action<IItemWidget> onClick = null;

        void Start()
        {
            if ( _enterButton != null )
            {
                _enterButton.onClick += OnEnterClick;
            }
        }

        void OnDestroy()
        {
            Dispose();
        }

        public void SetOfferInfo(T model)
        {
            _model = model;

            if ( _offerTitle != null )
            {
                _offerTitle.text = model.offerTitle;
            }

            if ( _offerFrequency != null )
            {
                _offerFrequency.text = string.Format( "{0}/{1} REMAINING", Math.Max( 0, model.frequency - model.used ), model.frequency );
            }

            if ( _price != null )
            {
                _price.text = model.price.amount.ToString( "{0.##}" );
            }

            if ( _currency != null )
            {
                //_currency.sprite = model.price.;
            }

            if ( _offerImage != null &&
                 string.IsNullOrEmpty( model.offerImage ) == false )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.offerImage ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _offerImage.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }

            if ( _offerBackground != null &&
                 string.IsNullOrEmpty( model.offerBackground ) == false )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.offerBackground ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _offerBackground.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }
            
            if ( model.isLocked )
            {
                RefreshBorder( Color.grey );

                _lockedOverlay.gameObject.SetActive( true );
                _outOfStock.gameObject.SetActive( true );
            }
            else
            {
                RefreshBorder( Color.cyan );

                _lockedOverlay.gameObject.SetActive( false );
                _outOfStock.gameObject.SetActive( false );
            }

            OnCustomSetOfferInfo( model );
        }

        protected virtual void OnCustomSetOfferInfo(T model)
        {

        }

        private void RefreshBorder(Color color)
        {
            if ( _border != null )
            {
                _border.color = color;

            }

            if ( _topBar != null )
            {
                _topBar.color = color;
            }

            if ( _bottomBar != null )
            {
                _bottomBar.color = color;
            }
        }

        private void OnEnterClick(ABaseButton button)
        {
            if ( onClick != null )
            {
                onClick( this );
            }
        }

        public void Dispose()
        {
            if ( _enterButton != null )
            {
                _enterButton.onClick -= OnEnterClick;
            }

            OnCustomDispose();
        }

        protected virtual void OnCustomDispose()
        {

        }
    }
}
