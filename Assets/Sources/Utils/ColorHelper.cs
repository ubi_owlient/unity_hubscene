﻿using Enums;
using UnityEngine;

namespace Utils
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public static class ColorHelper
    {
        public static Color FromMessagePriority(MessagePriorityEnum priority)
        {
            switch (priority)
            {
                case MessagePriorityEnum.MINIMAL:
                    return Color.cyan;
                case MessagePriorityEnum.LOW:
                    return Color.yellow;
                case MessagePriorityEnum.NORMAL:
                    return Color.green;
                case MessagePriorityEnum.HIGH:
                    return Color.magenta;
                case MessagePriorityEnum.MAXIMAL:
                    return Color.red;
            }

            return Color.cyan;
        }
    }
}
