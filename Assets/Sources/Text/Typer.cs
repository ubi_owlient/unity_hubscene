﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Text))]
    public class Typer : MonoBehaviour
    {
        [SerializeField]
        private string _message = "Replace";
        
        [SerializeField]
        private float _startDelay = 1.0f;

        [SerializeField]
        private float _typeDelay = 0.01f;

        [SerializeField]
        private AudioClip _putt = null;

        private Text _text = null;

        void Awake()
        {
            _text = GetComponent<Text>();
        }
        
        private IEnumerator TypeIn()
        {
            yield return new WaitForSeconds(_startDelay);

            for (int curr = 0; curr < _message.Length; curr++)
            {
                _text.text = _message.Substring( 0, curr );
                AudioSource audioSource = GetComponent<AudioSource>();
                if ( audioSource != null &&
                     _putt )
                {
                    audioSource.PlayOneShot( _putt );
                }

                yield return new WaitForSeconds( _typeDelay );
            }
        }

        private IEnumerator TypeOff()
        {
            for ( int curr = _message.Length; curr >= 0; curr--)
            {
                _text.text = _message.Substring( 0, curr );
                yield return new WaitForSeconds( _typeDelay );
            }
        }
    }
}