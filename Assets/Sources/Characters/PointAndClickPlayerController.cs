﻿using Enums;
using Hub;
using Models;
using Service;
using System;
using UnityEngine;
using UnityEngine.AI;

namespace Characters
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class PointAndClickPlayerController : MonoBehaviour
    {
        [SerializeField]
        private Texture2D        _crosshair        = null;
        
        [SerializeField]
        private float            _rotationSpeed    = 1.0f;

        private bool                 _hasTarget        = false;
        private NavMeshAgent         _agent            = null;
        private ThirdPersonCharacter _character        = null;
        private Vector3?             _lookAt           = null;

        public NavMeshAgent agent
        {
            get
            {
                return _agent;
            }
        }

        void Start()
        {
            _agent     = this.GetComponent<NavMeshAgent>();
            _agent.updateRotation = false;

            _character = this.GetComponent<ThirdPersonCharacter>();
            
            ServiceManager.Instance.GetService<ActivityService>().onActivityChanged += OnActivityChanged;
        }
        
        void OnDestroy()
        {
            ServiceManager.Instance.GetService<ActivityService>().onActivityChanged -= OnActivityChanged;
        }

        void Update()
        {
            if ( _agent.pathPending )
            {
                return;
            }

            if ( _hasTarget == true &&
                 _agent.remainingDistance > _agent.stoppingDistance )
            {
                _character.Move( _agent.desiredVelocity, false, false );
            }
            else
            {
                // Stop movements.
                _character.Move( Vector3.zero, false, false );
                _hasTarget = false;
                _agent.isStopped = true;

                // Orientate.
                if ( Orientate( _lookAt ) == false )
                {
                    _lookAt = null;
                }
            }
        }

        public void Move(Vector3 position, Vector3 lookAt)
        {
            _agent.isStopped = true;

            if ( ServiceManager.Instance.GetService<SettingService>().AnimationEnabled )
            {
                if ( _agent.SetDestination( position ) )
                {
                    _agent.isStopped = false;

                    _lookAt = lookAt;
                    _hasTarget = true;
                }
            }
            else
            {
                // Pops loading screen.
                ServiceManager.Instance.GetService<LoadingService>().TriggerFadingScreen( 2.0, delegate { Teleport( position, lookAt ); });
            }
        }

        private bool Orientate(Vector3? lookAt)
        {
            if ( lookAt.HasValue )
            {
                GameObject player = ServiceManager.Instance.GetService<PlayerService>().player;

                Vector3 direction = (lookAt.Value - player.transform.position).normalized;
                Quaternion finalOrientation = Quaternion.LookRotation( new Vector3( direction.x, 0, direction.z ) );

                float remainingAngle = Quaternion.Angle( player.transform.rotation, finalOrientation );
                if ( Math.Abs( remainingAngle ) <= float.Epsilon )
                {
                    return false;
                }

                // Smooth the rotation to apply to reach the proper orientation.
                player.transform.rotation = Quaternion.Slerp( player.transform.rotation, finalOrientation, _rotationSpeed * Time.deltaTime );
                
                return true;
            }

            return false;
        }

        private void OnActivityChanged(ActivityService sender, ActivityEnum activity, ActivityModel selected)
        {
            if ( selected != null )
            {
                Debug.Log( string.Format( "Clicked on {0} activity.", selected.activity ) );

                Move( selected.hubPosition, selected.hubLookAt );
            }
        }
        
        private void Teleport(Vector3 position, Vector3 lookAt)
        {
            GameObject player = ServiceManager.Instance.GetService<PlayerService>().player;

            // Teleport.
            player.transform.position = position;
            _agent.Warp( position );

            // Orientate the player.
            player.transform.LookAt( lookAt );
        }
        
	    void OnGUI() 
	    {
            if ( ServiceManager.Instance.GetService<PauseService>().isPaused )
            {
                return;
            }

		    if (_crosshair)
		    {
                GUI.DrawTexture(new Rect(Screen.width / 2 - (_crosshair.width * 0.5f),
                                             Screen.height / 2 - (_crosshair.height * 0.5f),
                                             _crosshair.width, _crosshair.height), _crosshair);
            }
	    }
    }
}
