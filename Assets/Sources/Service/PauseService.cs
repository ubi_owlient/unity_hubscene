﻿using System;
using UnityEngine;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class PauseService : AFeatureService
    {
        private bool _isPaused = false;
        
        public event Action<PauseService, bool> pauseChanged = null;

        /// <summary>
        /// Gets the flag indicating whether the game is paused or not.
        /// </summary>
        public bool isPaused
        {
            get
            {
                return _isPaused;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PauseService"/> class.
        /// </summary>
        public PauseService()
        {

        }

        public void Initialize()
        {

        }

        public void Pause()
        {
            Time.timeScale = 0;

            _isPaused = true;

            NotifyPauseChanged( _isPaused );
        }

        public void Resume()
        {
            Time.timeScale = 1;

            _isPaused = false;

            NotifyPauseChanged( _isPaused );
        }

        private void NotifyPauseChanged(bool paused)
        {
            if ( pauseChanged != null )
            {
                pauseChanged( this, paused );
            }
        }
    }
}
