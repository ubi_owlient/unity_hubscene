﻿using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CalendarPopupView : ASettingsPopupView
    {
        public override string id
        {
            get
            {
                return Constants.CALENDAR_POPUP_ID;
            }
        }
    }
}
