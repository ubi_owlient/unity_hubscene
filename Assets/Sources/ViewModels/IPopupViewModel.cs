﻿namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi.
    /// </summary>
    public interface IPopupViewModel : IViewModel
    {
        /// <summary>
        /// Shows the popup.
        /// </summary>
        void Show();

        /// <summary>
        /// Hides the popup.
        /// </summary>
        void Hide();
    }
}
