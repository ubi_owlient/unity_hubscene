﻿using Commands;
using Commands.Settings;
using FSM.Hub.Settings;
using Service;
using Utils;
using ViewModels;

namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SettingsRootState : ABaseState
    {
        private bool _exitSettings = false;

        private IPopupViewModel _currentSettingPopup = null;

        public SettingsRootState()
        {
            _stateName = "Setting Root";
        }

        public override void OnEnter()
        {
            base.OnEnter();


        }

        [DispatcherService.HandleCommand(commandType = typeof(CloseSettingCommand))]
        private void OnSettingClosed(ICommand command)
        {
            Owner.GetService<PauseService>().Resume();

            _exitSettings = true;
        }

        [DispatcherService.HandleCommand(commandType = typeof(SelectSettingOptionCommand))]
        private void OnEnterSettingContent(ICommand command)
        {
            SelectSettingOptionCommand selectCommand = command as SelectSettingOptionCommand;

            _currentSettingPopup = Owner.GetService<PopupService>()[ selectCommand.settingId ];
        }
        
        public override void OnExit()
        {
            base.OnExit();
            
            _exitSettings = false;

            if ( _currentSettingPopup != null )
            {
                _currentSettingPopup.Hide();
                _currentSettingPopup = null;
            }
        }

        public override Transition GetTransition()
        {
            if ( _exitSettings )
            {
                return Transition.Sibling<HubRootState>();
            }
            else if ( _currentSettingPopup != null )
            {
                Transition transition = Transition.None();

                switch ( _currentSettingPopup.id )
                {
                    case Constants.OPTIONS_POPUP_ID:
                        transition = Transition.Inner<OptionsSettingState>( _currentSettingPopup );
                        break;
                    case Constants.MAILS_POPUP_ID:
                        transition = Transition.Inner<MailsSettingState>( _currentSettingPopup );
                        break;
                    case Constants.CALENDAR_POPUP_ID:
                        transition = Transition.Inner<CalendarSettingState>( _currentSettingPopup );
                        break;
                    case Constants.MOTD_POPUP_ID:
                        transition = Transition.Inner<MOTDSettingState>( _currentSettingPopup );
                        break;
                    case Constants.CHAT_POPUP_ID:
                        transition = Transition.Inner<ChatSettingState>( _currentSettingPopup );
                        break;
                }

                _currentSettingPopup = null;

                return transition;
            }

            return base.GetTransition();
        }
    }
}
