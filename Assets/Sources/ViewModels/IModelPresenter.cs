﻿namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IModelPresenter
    {
        IViewModel model
        {
            get;
        }
    }
}
