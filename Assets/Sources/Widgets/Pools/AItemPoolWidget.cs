﻿using Extensions;
using System.Collections.Generic;
using UnityEngine;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi.
    /// </summary>
    public abstract class AItemPoolWidget<T> : AItemPool where T : MonoBehaviour, IItemWidget
    {
        private List<T> _items = new List<T>();
        
        /// <summary>
        /// Gets the item count.
        /// </summary>
        public override uint itemCount
        {
            get
            {
                return (uint)_items.Count;
            }
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        public IEnumerable<T> Items
        {
            get
            {
                return _items;
            }
        }
        
        /// <summary>
        /// Gets/Creates an item.
        /// </summary>
        public T GetItem(uint index)
        {
            if ( index < _items.Count )
            {
                return _items[ (int)index ];
            }

            if ( _itemPrefab == null )
            {
                return null;
            }

            return Next();
        }

        /// <summary>
        /// Gets the next item in the list (like a push back).
        /// </summary>
        /// <returns></returns>
        public T Next()
        {
            if ( _itemPrefab == null )
            {
                return null;
            }

            GameObject newItem = Instantiate( _itemPrefab, _itemTree.transform );
            T widget = newItem.GetComponent<T>();
            if ( widget == null )
            {
                widget = newItem.GetComponentInChildren<T>();
            }

            widget.onClick += OnItemClick;

            _items.Add( widget );

            return widget;
        }

        /// <summary>
        /// Delegate called on itel click.
        /// </summary>
        /// <param name="clicked"></param>
        private void OnItemClick(IItemWidget clicked)
        {
            NotifyItemClicked( clicked );
        }

        /// <summary>
        /// Removes an item by id.
        /// </summary>
        /// <param name="index"></param>
        public override bool RemoveItem(uint index)
        {
            if ( index >= 0 ||
                 index < _items.Count )
            {
                T widget = _items[ (int)index ];
                _items.Remove( widget );
                widget.onClick -= OnItemClick;
                widget.Dispose();
                GameObject.Destroy( widget.gameObject );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Cleans up.
        /// </summary>
        public override void CleanUp()
        {
            for (int curr = 0; curr < _items.Count; curr++)
            {
                T widget = _items[ curr ];
                widget.onClick -= OnItemClick;
                widget.Dispose();
                GameObject.Destroy( widget.gameObject );
            }
            _items.Clear();

            _itemTree.transform.ClearChildren();
        }
    }
}
