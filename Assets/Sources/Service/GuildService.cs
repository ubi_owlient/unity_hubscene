﻿using Models;
using System;
using System.Collections.Generic;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class GuildService : AFeatureService
    {
        private int _guildCount = 10;
        private IList<string> _iconNames = null;
        private Dictionary<uint, GuildModel> _guilds = new Dictionary<uint, GuildModel>();

        private List<string> _defaultName = new List<string>() { "Dragons", "Fighters", "Wild Hunt", "Silent Assassins", "Dark Shadow", "Fairytail" };
        private List<string> _defaultMotd = new List<string>() { "Hello world", "New day to even", "Start to fight", "Let's get it to rumble", "Let's fight", "Let's rock" };
        private List<string> _defaultDescription = new List<string>() { "The guild that fear nothing",
                                                                        "For those having enough courage to fight in greatest battle",
                                                                        "Beginners guild with nice guys",
                                                                        "People alone in the dark that seen some light",
                                                                        "Why not being in this guild, no one want us",
                                                                        "We know we fight for becoming legends" };

        public int guildCount
        {
            get
            {
                return _guildCount;
            }
        }

        public IEnumerable<GuildModel> guilds
        {
            get
            {
                return _guilds.Values;
            }
        }
        
        private Dictionary<string, uint> _alreadyUsedNames = new Dictionary<string, uint>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GuildService"/> class.
        /// </summary>
        public GuildService()
        {

        }

        public Action<GuildService, GuildModel> guildCreated = null;

        public void Initialize(int guildCount, IEnumerable<string> iconNames)
        {
            _iconNames = new List<string>( iconNames );

            _guildCount = guildCount;

            int iconIndex = 0;
            int iconCount = _iconNames.Count;
            for (uint curr = 0; curr < _guildCount; curr++)
            {
                int randomIndex = UnityEngine.Random.Range( 0, 5 );

                string guildName = _defaultName[ randomIndex ];
                uint count = 0;
                if ( _alreadyUsedNames.TryGetValue( guildName, out count ) )
                {
                    count += 1;
                    guildName = string.Format( "{0}_#{1}", guildName, count );
                    _alreadyUsedNames[ guildName ]= count;
                }
                else
                {
                    _alreadyUsedNames[ guildName ] = 1;
                }
                
                _guilds.Add( curr, new GuildModel( curr, guildName, _iconNames[ iconIndex ], _defaultDescription[ randomIndex ], _defaultMotd[ randomIndex ] ) );

                iconIndex++;

                if ( iconIndex >= iconCount )
                {
                    iconIndex = 0;
                }
            }
        }

        public void CreateGuild(string guildName, string iconName, string description, string motd)
        {
            GuildModel newGuild = new GuildModel( (uint)_guilds.Count, guildName, iconName, description, motd );
            _guilds.Add( (uint)_guilds.Count, newGuild );

            NotifyGuildCreated( newGuild );
        }

        private void NotifyGuildCreated(GuildModel newGuild)
        {
            if ( guildCreated != null )
            {
                guildCreated( this, newGuild );
            }
        }
    }
}
