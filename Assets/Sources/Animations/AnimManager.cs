using Service;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Animations
{
    public class AnimManager : Receiver
    {
        [System.Serializable]
        public enum eAnimType
        {
            EANIM_SCALE,
            EANIM_MOVE,
            EANIM_ROTATE,
            EANIM_FADE,
            EANIM_NOTHING,
        };

        [System.Serializable]
        public struct sAnimation
        {
            public Transform targetObject;
            public string inStartEvent;
            public string inOffEvent;
            public string[] outEndEvents;

            public eAnimType animType;
            public Vector3 source;
            public Vector3 destination;
            public float alphaSource;
            public float alphaDestination;
            public float duration;
            public int nbSteps;
            public bool backAndFore;
            public bool loop;
            public float stepMultiplicator;
        };
        public class sActivAnimation
        {
            public sAnimation curAnimation;
            public float elapsedTime;
        };

        public sAnimation[] animations;
        private Color newColor;
        protected ArrayList activAnimation;

        protected virtual void Start()
        {
            activAnimation = new ArrayList();

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < animations.Length; ++i)
            {
                if (animations[i].targetObject == null)
                    animations[i].targetObject = transform;

                animations[i].inStartEvent = animService.PrepareEventName(animations[i].inStartEvent, animations[i].targetObject.gameObject);
                animations[i].inOffEvent   = animService.PrepareEventName(animations[i].inOffEvent, animations[i].targetObject.gameObject);

                animService.Register(animations[i].inStartEvent, this);
                animService.Register(animations[i].inOffEvent, this);
                if (animations[i].inStartEvent.Length == 0)
                {
                    addAnim(i);
                }
            }
        }

        void Stop()
        {
            activAnimation.Clear();

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < animations.Length; ++i)
            {
                animService.Unregister(animations[i].inStartEvent, this);
                animService.Unregister(animations[i].inOffEvent, this);
            }
        }

        protected virtual void Update()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < activAnimation.Count; ++i)
            {
                sActivAnimation curAnim = (sActivAnimation)activAnimation[i];
                curAnim.elapsedTime += Time.deltaTime;
                float coef = curAnim.elapsedTime / curAnim.curAnimation.duration;
                if (coef > 1)
                {
                    if (curAnim.curAnimation.loop)
                    {
                        curAnim.elapsedTime = 0;
                    }
                    else
                    {
                        coef = 1;
                        activAnimation.RemoveAt(i);
                        --i;
                    }

                    if (animService != null )
                    {
                        animService.SendEvent(curAnim.curAnimation.outEndEvents, gameObject);
                    }
                }
                float baseCoef = coef;

                int step = 0;

                if (curAnim.curAnimation.nbSteps > 1)
                {
                    coef *= curAnim.curAnimation.nbSteps;
                    step = (int)coef;
                    coef -= step;
                }

                if (curAnim.curAnimation.backAndFore)
                {
                    if (coef > 0.5f)
                    {
                        coef = 1.0f - coef;
                    }
                    coef *= 2.0f;
                }

                if ((baseCoef > 0) && (curAnim.curAnimation.stepMultiplicator != 0))
                {
                    coef /= curAnim.curAnimation.stepMultiplicator * baseCoef;
                    if (coef > 1.0f)
                    {
                        coef = 1.0f;
                    }
                }

                switch (curAnim.curAnimation.animType)
                {
                    case eAnimType.EANIM_SCALE:
                        {
                            Vector3 vRes = curAnim.curAnimation.source * (1.0f - coef) + curAnim.curAnimation.destination * coef;
                            curAnim.curAnimation.targetObject.localScale = vRes;
                        }
                        break;

                    case eAnimType.EANIM_MOVE:
                        {
                            Vector3 vRes = curAnim.curAnimation.source * (1.0f - coef) + curAnim.curAnimation.destination * coef;
                            curAnim.curAnimation.targetObject.localPosition = vRes;
                        }
                        break;

                    case eAnimType.EANIM_ROTATE:
                        {
                            Vector3 vRes = curAnim.curAnimation.source * (1.0f - coef) + curAnim.curAnimation.destination * coef;
                            curAnim.curAnimation.targetObject.localEulerAngles = vRes;
                        }
                        break;

                    case eAnimType.EANIM_FADE:
                        {
                            float fAlpha = curAnim.curAnimation.alphaSource * (1.0f - coef) + curAnim.curAnimation.alphaDestination * coef;
                            Vector3 vRes = curAnim.curAnimation.source * (1.0f - coef) + curAnim.curAnimation.destination * coef;
                            newColor = new Color(vRes.x, vRes.y, vRes.z, fAlpha);

                            SpriteRenderer spr = curAnim.curAnimation.targetObject.GetComponent<Renderer>() as SpriteRenderer;
                            if (spr != null)
                                spr.color = newColor;
                            TextMesh txt = curAnim.curAnimation.targetObject.GetComponent<TextMesh>();
                            if (txt != null)
                                txt.color = newColor;
                            if ((spr == null) && (txt == null))
                            {
                                Renderer rdr = curAnim.curAnimation.targetObject.GetComponent<Renderer>();
                                if (rdr != null)
                                    rdr.material.color = newColor;
                            }
                            Image ii = curAnim.curAnimation.targetObject.GetComponent<Image>();
                            if (ii != null)
                            {
                                ii.color = newColor;
                            }
                            Text tt = curAnim.curAnimation.targetObject.GetComponent<Text>();
                            if (tt != null)
                            {
                                tt.color = newColor;
                            }
                            TextMeshProUGUI ttpro = curAnim.curAnimation.targetObject.GetComponent<TextMeshProUGUI>();
                            if (ttpro != null)
                            {
                                ttpro.color = newColor;
                            }

                        }
                        break;

                    case eAnimType.EANIM_NOTHING:
                        break;
                }
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            //Debug.Log("BUBBLE "+ eventName);

            for (int i = 0; i < animations.Length; ++i)
            {
                if (eventName.Equals(animations[i].inStartEvent))
                {
                    addAnim(i);
                }
            }
            for (int i = 0; i < activAnimation.Count; ++i)
            {
                sActivAnimation curAnim = (sActivAnimation)activAnimation[i];
                if (eventName.Equals(curAnim.curAnimation.inOffEvent))
                {
                    activAnimation.RemoveAt(i);
                    --i;
                }
            }
        }

        protected virtual void addAnim(int id)
        {
            sActivAnimation curAnim = new sActivAnimation();
            curAnim.curAnimation = animations[id];
            curAnim.elapsedTime = 0;
            activAnimation.Add(curAnim);
        }
    }
}