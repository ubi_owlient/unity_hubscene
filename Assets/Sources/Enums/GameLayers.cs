﻿namespace Types
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public enum GameLayers
    {
        Default = 0,
        TransparentFX = 1,
        IgnoreRaycast = 2,
        Unknown1 = 3,
        Water = 4,
        UI = 5,
        Unknown2 = 6,
        Unknown3 = 7,
        Player = 8,
        Crate = 9,
        HubActivity = 10,

        // Add others.
    }
}
