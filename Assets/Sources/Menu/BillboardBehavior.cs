﻿using UnityEngine;

namespace UI
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class BillboardBehavior : MonoBehaviour
    {
        [SerializeField]
        private Camera _camera = null;

        [SerializeField]
        private int _updateInterval = 3;

        void Update()
        {
            // Modify the canvas in world space so it remains in front of the camera.
            if ( _camera != null )
            {
                // Updates ONLY every _updateInterval frames.
                if ( Time.frameCount % _updateInterval == 0 )
                {
                    RotateToCamera();
                }
            }
        }

        private void RotateToCamera()
        {
            //transform.LookAt( transform.position + _camera.transform.rotation * Vector3.forward,
            //                  _camera.transform.rotation * Vector3.up );

            Vector3 toCamera = _camera.transform.position - transform.position;
            toCamera.x = toCamera.z = 0.0f; // Y axis rotation only.
            transform.LookAt( _camera.transform.position - toCamera );
            transform.Rotate( 0.0f, -180.0f, 0.0f );

            //var lookPos = _camera.transform.position - transform.position;
            //lookPos.y = 0; // Y axis rotation only.
            //var rotation = Quaternion.LookRotation(lookPos);
            //transform.rotation = rotation;
            //transform.Rotate( 0.0f, -180.0f, 0.0f );
        }
    }
}
