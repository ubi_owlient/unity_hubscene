﻿using System;
using UnityEngine;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IButtonState : IDisposable
    {
        event Action<IButtonState> callbacks;

        Sprite image
        {
            get;
        }

        Color color
        {
            get;
        }

        void NotifyListeners();
    }
}
