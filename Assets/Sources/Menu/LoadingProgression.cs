﻿using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class LoadingProgression : MonoBehaviour
    {
        [SerializeField]
        private Text _percentage = null;

        public void SetProgression(uint percentage)
        {
            if ( percentage > 100 )
            {
                percentage = 100;
            }

            if ( _percentage != null )
            {
                _percentage.text = string.Format( "{0}%", percentage.ToString() );
            }
        }
    }
}
