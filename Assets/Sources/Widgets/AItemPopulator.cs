﻿using System;
using UnityEngine;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class AItemPopulator<T> : MonoBehaviour where T : MonoBehaviour, IItemWidget
    {
        [SerializeField]
        private AItemPool _pool = null;

        public event Action<AItemPopulator<T>, T> onClick = null;

        public AItemPoolWidget<T> pool
        {
            get
            {
                return _pool as AItemPoolWidget<T>;
            }
        }

        void Start()
        {
            Initialize();
        }

        void OnDestroy()
        {
            _pool.onClick -= OnPoolItemClick;
        }

        private void Initialize()
        {
            _pool.onClick += OnPoolItemClick;

            OnCustomInitialize();
        }

        protected virtual void OnCustomInitialize()
        {

        }

        private void OnPoolItemClick(AItemPool sender, object item)
        {
            if ( onClick != null )
            {
                onClick( this, item as T );
            }
        }
    }
}
