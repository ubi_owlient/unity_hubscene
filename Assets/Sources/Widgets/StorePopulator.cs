﻿using Models;
using Service;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class StorePopulator : AItemPopulator<StoreTileView>
    {
        protected override void OnCustomInitialize()
        {
            //Populate( ServiceManager.Instance.GetService<StoreService>().storeModel );
        }

        private void Populate(IEnumerable<StoreOffer> offers)
        {
            uint count = 0;
            foreach ( StoreOffer offer in offers )
            {
                StoreTileView widget = pool.GetItem( count );
                widget.SetOfferInfo( offer );
                count++;
            }
        }
    }
}
