﻿using Models;
using System.Collections.Generic;

namespace FeatureModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class TutorialModel : AFeatureModel
    {
        private bool _isStarted  = false;
        private bool _isRunning  = false;
        private bool _isFinished = false;

        private TutorialPart _currentTutorial = null;

        private Dictionary<string, TutorialPart> _tutorialParts = new Dictionary<string, TutorialPart>();

        public TutorialPart currentTutorial
        {
            get
            {
                return _currentTutorial;
            }
            set
            {
                NotifyPropertyChanged( ref _currentTutorial, value );
            }
        }

        public bool isStarted
        {
            get
            {
                return _isStarted;
            }
            set
            {
                NotifyPropertyChanged( ref _isStarted, value );
            }
        }

        public bool isRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                NotifyPropertyChanged( ref _isRunning, value );
            }
        }

        public bool isFinished
        {
            get
            {
                return _isFinished;
            }
            set
            {
                NotifyPropertyChanged( ref _isFinished, value );
            }
        }

        public TutorialModel(params TutorialPart[] parts)
        {
            for (int curr = 0; curr < parts.Length; curr++)
            {
                TutorialPart part = parts[ curr ];
                _tutorialParts[ part.id ] = part;
            }
        }

        public TutorialPart GetPart(string id)
        {
            TutorialPart part = null;
            if ( _tutorialParts.TryGetValue( id, out part ) )
            {
                return part;
            }

            return null;
        }
    }
}
