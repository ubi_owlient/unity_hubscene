﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The Daily ops state.
    /// </summary>
    public class DailyOpsHubState : AHubState
    {
        public DailyOpsHubState()
        {
            _stateName = "DailyOps hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            
        }

        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
