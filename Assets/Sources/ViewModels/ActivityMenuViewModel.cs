﻿using FeatureModels;
using Models;
using System.ComponentModel;

namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ActivityMenuViewModel : AViewModel
    {
        public bool isOpen
        {
            get
            {
                if ( step != null )
                {
                    return true;
                }

                return false;
            }
        }

        public ActivityModel activity
        {
            get
            {
                if ( step != null )
                {
                    return step.owner;
                }

                return null;
            }
        }

        public ActivityStep step
        {
            get
            {
                HubModel hubModel = GetModel<HubModel>();
                if ( hubModel != null )
                {
                    return hubModel.currentStep;
                }

                return null;
            }
        }

        public string menuTitle
        {
            get
            {
                if ( step != null )
                {
                    return step.title;
                }

                return string.Empty;
            }
        }

        public string menuDescription
        {
            get
            {
                if ( step != null )
                {
                    return step.description;
                }

                return string.Empty;
            }
        }

        public ActivityMenuViewModel(params IFeatureModel[] models) :
        base( models )
        {
            _identifier = string.Empty;

            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.PropertyChanged += OnHubMenuPropertyChanged;
            }
        }

        protected override void OnDispose()
        {
            base.OnDispose();

            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.PropertyChanged -= OnHubMenuPropertyChanged;
            }
        }

        private void OnHubMenuPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if ( args.PropertyName == "currentStep" )
            {
                RaisePropertyChanged( "step" );
                RaisePropertyChanged( "isOpen" );
            }
        }
    }
}
