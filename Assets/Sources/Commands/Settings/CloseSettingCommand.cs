﻿using ViewModels;

namespace Commands.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CloseSettingCommand : ACommand
    {
        public CloseSettingCommand(IModelPresenter sender) : 
        base(sender)
        {

        }
    }
}
