﻿using Service;
using UnityEngine;

namespace Animations
{
    public class ZoneScript : MonoBehaviour
    {
        public GameObject[] toggles;

        public string outEntered;
        public string outExited;


        void OnTriggerEnter2D(Collider2D coll)
        {

            if (isInCollider(coll))
            {
                //Debug.Log ("Est entre dans la zone : " + coll.name);

                ServiceManager.Instance.GetService<AnimationService>().SendEvent(outEntered, this.gameObject);
            }

        }
        void OnTriggerExit2D(Collider2D coll)
        {
            if (isInCollider(coll))
            {
                //Debug.Log ("Est sorti de la zone : " + coll.name);				

                ServiceManager.Instance.GetService<AnimationService>().SendEvent(outExited, this.gameObject);
            }
        }


        private bool isInCollider(Collider2D coll)
        {
            for (int i = 0; i < toggles.Length; i++)
            {
                if (coll.gameObject.name.Equals(toggles[i].name))
                    return true;
            }
            return false;
        }
    }
}