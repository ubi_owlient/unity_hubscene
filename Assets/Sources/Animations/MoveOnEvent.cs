using Service;
using System.Collections;
using UnityEngine;

namespace Animations
{
    public class MoveOnEvent : Receiver
    {
        [System.Serializable]
        public enum MoveType
        {
            MOVE_3D_3D,
            MOVE_CANVAS_CANVAS,
            MOVE_3D_CANVAS,
            MOVE_CANVAS_3D,
        };

        #region STATIC
        private static Vector2 WorldToCanvasPosition(Vector3 position)
        {
            return Camera.main.WorldToScreenPoint(position);
        }

        private static RectTransform GetCanvasFromItem(RectTransform item)
        {
            if (item.parent == null)
                return item;
            return GetCanvasFromItem(item.parent as RectTransform);
        }
        #endregion

        public string inEventDoMove;
        public string targetObjectPath;
        public Transform targetObject;
        public Transform[] targetObjects;
        public Transform toMove;
        public MoveType type = MoveType.MOVE_3D_3D;
        public Vector3 offset = Vector3.zero;
        public bool doMoving = false;
        public bool teleportOnStart = false;
        public float movingSpeed = 1.0f;
        public string outEventStartMovingL;
        public string outEventStartMovingR;
        public string outEventStopMoving;
        public float canvasScaleMultiplicator = 1.39f;
        

        private void Start()
        {
            if (toMove == null)
            {
                toMove = transform;
            }
            if ((targetObjectPath != null) && (targetObjectPath.Length > 0))
            {
                //Debug.Log("SEARCHING FOR " + targetObjectPath);
                GameObject o = GameObject.Find(targetObjectPath);
                targetObject = o != null ? o.transform : null;
                //Debug.Log("FOUND ? " + (o != null));
            }
            if (targetObject == null)
            {
                targetObject = transform;
            }

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();

            inEventDoMove = animService.PrepareEventName(inEventDoMove, gameObject);

            animService.Register(inEventDoMove, this);
        }

        public IEnumerator GotoMono()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            Vector3 vPos = GetPosFromPosition(targetObject.position);
            Vector3 vDiff = vPos - toMove.position;
            float len = vDiff.magnitude;
            float frameDist = Time.deltaTime * movingSpeed;
            if (len < frameDist)
            {
                //Debug.Log("GOTO MONO NO DEPLACEMENT");
                toMove.position = vPos;
                yield return new WaitForSeconds(0.3f);
                animService.SendEvent(outEventStopMoving, gameObject);
            }
            else
            {
                if (!gameObject.activeInHierarchy)
                    yield break;

                animService.SendEvent(vDiff.x < 0 ? outEventStartMovingL : outEventStartMovingR, gameObject);
                yield return new WaitForEndOfFrame();
                while (true)
                {
                    if (!gameObject.activeInHierarchy)
                        yield break;

                    vDiff = vPos - toMove.position;
                    len = vDiff.magnitude;
                    vDiff = vDiff.normalized;
                    frameDist = Time.deltaTime * movingSpeed;
                    //Debug.Log(vPos + " " + toMove.position + " " + len + " " + frameDist + " " + movingSpeed + "  " + gameObject.name);
                    if (len >= frameDist)
                    {
                        toMove.position += vDiff * frameDist;
                    }
                    else
                    {
                        toMove.position = vPos;
                        break;
                    }
                    yield return new WaitForEndOfFrame();
                }

                animService.SendEvent(outEventStopMoving, gameObject);
            }
        }

        public Vector3 GetPosFromPosition(Vector3 vPos)
        {
            Vector3 target = Vector3.zero;
            switch (type)
            {
                case MoveType.MOVE_3D_3D:
                    target = vPos + offset;
                    break;
                case MoveType.MOVE_CANVAS_CANVAS:
                    target = vPos + offset * (GetCanvas(transform as RectTransform).scaleFactor * canvasScaleMultiplicator);
                    break;
                case MoveType.MOVE_3D_CANVAS:
                    target = WorldToCanvasPosition(vPos + offset);
                    break;
            };
            return target;
        }

        public void UpdatePosition()
        {
            if (doMoving)
            {
                toMove.gameObject.GetComponent<MonoBehaviour>().StopAllCoroutines();
                toMove.gameObject.GetComponent<MonoBehaviour>().StartCoroutine(targetObjects.Length > 0 ? GotoMulti() : GotoMono());
            }
            else
            {
                toMove.position = GetPosFromPosition(targetObject.position);
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            if (eventName.Equals(inEventDoMove))
            {
                UpdatePosition();
            }
        }

        private void Stop()
        {
            ServiceManager.Instance.GetService<AnimationService>().Unregister(inEventDoMove, this);
        }

        private IEnumerator GotoMulti()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < targetObjects.Length; ++i)
            {
                Vector3 vPos = GetPosFromPosition(targetObjects[i].position);
                Vector3 vDiff = vPos - toMove.position;
                float len = vDiff.magnitude;
                float frameDist = Time.deltaTime * movingSpeed;
                if ((len < frameDist) || (teleportOnStart && (i == 0)))
                {
                    toMove.position = vPos;
                    yield return new WaitForEndOfFrame();
                }
                else
                {
                    animService.SendEvent(vDiff.x < 0 ? outEventStartMovingL : outEventStartMovingR, gameObject);
                    yield return new WaitForEndOfFrame();
                    while (true)
                    {
                        vDiff = vPos - toMove.position;
                        len = vDiff.magnitude;
                        vDiff = vDiff.normalized;
                        frameDist = Time.deltaTime * movingSpeed;
                        //Debug.Log(vPos + " " + toMove.position + " " + len + " " + frameDist);
                        if (len >= frameDist)
                        {
                            toMove.position += vDiff * frameDist;
                        }
                        else
                        {
                            toMove.position = vPos;
                            break;
                        }
                        yield return new WaitForEndOfFrame();
                    }

                    animService.SendEvent(outEventStopMoving, gameObject);
                }
            }
        }

        private Canvas GetCanvas(RectTransform t)
        {
            Canvas c = t.gameObject.GetComponent<Canvas>();
            if (c != null)
            {
                return c;
            }
            if (t.parent != null)
            {
                return GetCanvas(t.parent as RectTransform);
            }
            return null;
        }
    }
}