using Service;
using UnityEngine;

namespace Animations
{
    public class EventConcatSender : Receiver
    {
        [System.Serializable]
        public enum Operator
        {
            CONCAT_INT,
            CONCAT_BOOL,
            CONCAT_STRING,
            CONCAT_RANDOM,
        };

        [System.Serializable]
        public struct EventVarToSet
        {
            public string inEvent;
            public string outEventBase;
            public string varToConcat;
            public Operator oper;
        };

        public EventVarToSet[] actions;

        void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                EventVarToSet ev = actions[i];
                ev.outEventBase = animService.PrepareEventName(ev.outEventBase, gameObject);
                ev.varToConcat  = animService.PrepareEventName(ev.varToConcat, gameObject);
                int val;
                bool result = int.TryParse(ev.varToConcat, out val);

                if (result == false)
                {
                    ev.inEvent = animService.PrepareEventName(ev.inEvent, gameObject);
                }

                actions[i] = ev;
                animService.Register(ev.inEvent, this);
            }
        }
        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                EventVarToSet ev = actions[i];
                animService.Unregister(ev.inEvent, this);
            }
        }


        public override void Receive(string eventName, GameObject obj)
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                EventVarToSet ev = actions[i];
                if (eventName.Equals(ev.inEvent))
                {
                    switch (ev.oper)
                    {
                        case Operator.CONCAT_RANDOM:
                            {
                                int val;
                                bool result = int.TryParse(ev.varToConcat, out val);
                                if (!result)
                                    val = PlayerPrefs.GetInt(ev.varToConcat, 0);
                                int val2 = Random.Range(0, val);
                                animService.SendEvent(ev.outEventBase + val2, obj);
                            }
                            break;
                        case Operator.CONCAT_INT:
                            {
                                int val;
                                bool result = int.TryParse(ev.varToConcat, out val);
                                if (!result)
                                {
                                    val = PlayerPrefs.GetInt(ev.varToConcat, 0);
                                }

                                animService.SendEvent(ev.outEventBase + val, obj);
                            }
                            break;
                        case Operator.CONCAT_BOOL:
                            {
                                int val = PlayerPrefs.GetInt(ev.varToConcat, 0);
                                animService.SendEvent(ev.outEventBase + (val != 0), obj);
                            }
                            break;
                        case Operator.CONCAT_STRING:
                            {
                                string val = PlayerPrefs.GetString(ev.varToConcat, "");
                                animService.SendEvent(ev.outEventBase + val, obj);
                            }
                            break;
                    }
                }
            }
        }
    }
}