﻿namespace Types.Notifications
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SimpleMessageNotificationDescription : ANotificationDescription
    {
        private string _message = string.Empty;

        public string message
        {
            get
            {
                return _message;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleMessageNotificationDescription"/> class.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="notificationDelay">A custom delay the notif will remain visible.</param>
        public SimpleMessageNotificationDescription(string message, float? notificationDelay = null) :
        base( notificationDelay )
        {
            _message = message;
        }
    }
}
