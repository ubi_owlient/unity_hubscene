﻿using ViewModels;

namespace Commands.Popup
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ClosePopupCommand : ACommand
    {
        public string popupId
        {
            get;
            private set;
        }

        public ClosePopupCommand(IModelPresenter sender, string popupId) :
        base(sender)
        {
            this.popupId = popupId;
        }
    }
}
