﻿using Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class GuildTileView : MonoBehaviour, IItemWidget
    {
        [SerializeField]
        private Text _guildName     = null;

        [SerializeField]
        private Button _enterButton = null;

        [SerializeField]
        private Image _guildIcon    = null;

        private GuildModel _model = null;

        public GuildModel model
        {
            get
            {
                return _model;
            }
        }

        public uint id
        {
            get
            {
                if ( _model != null )
                {
                    return _model.guildId;
                }

                return 0;
            }
        }
        
        public event Action<IItemWidget> onClick = null;

        void Start()
        {
            if ( _enterButton != null )
            {
                _enterButton.onClick.AddListener( OnGuildEnter );
            }
        }

        void OnDestroy()
        {
            Dispose();
        }

        public void SetGuildInfo(GuildModel model)
        {
            _model = model;

            if ( _guildName != null )
            {
                _guildName.text = model.guildName;
            }

            if ( _guildIcon != null )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.iconName ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _guildIcon.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }
        }

        private void OnGuildEnter()
        {
            if ( onClick != null )
            {
                onClick( this );
            }
        }

        public void Dispose()
        {
            if (_enterButton != null)
            {
                _enterButton.onClick.RemoveListener(OnGuildEnter);
            }
        }
    }
}
