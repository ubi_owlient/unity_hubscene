﻿using FeatureModels;
using System;
using System.Collections.Generic;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ModelService : ACoreService
    {
        private Dictionary<string, IFeatureModel> _modelById = new Dictionary<string, IFeatureModel>();
        private Dictionary<Type, IFeatureModel> _modelByType = new Dictionary<Type, IFeatureModel>();

        public ModelService()
        {

        }

        public void RegisterModel(IFeatureModel model)
        {
            _modelByType.Add( model.GetType(), model );
            _modelById.Add( model.id, model );
        }

        public IFeatureModel GetModel(string id)
        {
            IFeatureModel model = null;
            if ( _modelById.TryGetValue( id, out model ) )
            {
                return model;
            }

            return null;
        }

        public IFeatureModel GetModel(Type modelType)
        {
            IFeatureModel model = null;
            if ( _modelByType.TryGetValue( modelType, out model ) )
            {
                return model;
            }

            return null;
        }

        public T GetModel<T>() where T : class, IFeatureModel
        {
            Type modelType = typeof(T);
            return GetModel( modelType ) as T;
        }
    }
}
