﻿using Service;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class DailyOpsManager : MonoBehaviour
    {
        [SerializeField]
        private List<string> _imageNames = new List<string>();

        [SerializeField]
        private List<string> _backgroundNames = new List<string>();

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<DailyOpsService>().Initialize( 8, _imageNames, _backgroundNames );
        }
    }
}
