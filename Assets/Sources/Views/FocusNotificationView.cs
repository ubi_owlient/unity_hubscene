﻿using Service;
using UnityEngine;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class FocusNotificationView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _pointerParent = null;

        private void Start()
        {
            ServiceManager.Instance.GetService<ActivityService>().SetFocusNotificationParent( _pointerParent );
        }
    }
}
