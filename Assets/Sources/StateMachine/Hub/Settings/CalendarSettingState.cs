﻿namespace FSM.Hub.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CalendarSettingState : ASettingState
    {
        public CalendarSettingState()
        {
            _stateName = "Calendar setting";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }
        
        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
