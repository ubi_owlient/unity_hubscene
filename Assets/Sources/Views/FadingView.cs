﻿using Service;
using System;
using System.Collections;
using UnityEngine;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class FadingView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _root = null;

        [SerializeField]
        private Animator _animator = null;

        private void Start()
        {
            ServiceManager.Instance.GetService<LoadingService>().fadingScreenRequired += OnFadingScreenRequired;
        }

        private void OnDestroy()
        {
            ServiceManager.Instance.GetService<LoadingService>().fadingScreenRequired -= OnFadingScreenRequired;
        }

        public void FadeOut()
        {
            _animator.SetTrigger( "FadeOut" );
        }

        public void FadeIn()
        {
            _animator.SetTrigger( "FadeIn" );
        }

        private void OnFadingScreenRequired(double time, Action actionToDoOnLoading)
        {
            StartCoroutine( ShowFadingScreen( (float)time, actionToDoOnLoading ) );
        }

        private IEnumerator ShowFadingScreen(float time, Action actionToDoOnLoading)
        {
            float halfTime = time * 0.5f;

            _root.SetActive( true );

            FadeOut();

            yield return new WaitForSeconds( halfTime );

            actionToDoOnLoading();

            FadeIn();

            yield return new WaitForSeconds( halfTime);

            _root.SetActive( false );
        }
    }
}
