﻿using FeatureModels;
using System;
using System.ComponentModel;

namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IViewModel : INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Gets the view model id.
        /// </summary>
        string id
        {
            get;
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the view model is visible or not.
        /// </summary>
        bool isVisible
        {
            get;
            set;
        }

        IFeatureModel GetModel(Type modelType);

        T GetModel<T>() where T : class, IFeatureModel;
    }
}
