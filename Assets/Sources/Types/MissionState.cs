﻿using Enums;

namespace Types
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MissionState
    {
        private MissionAchievements _achievements;

        public bool isComplited
        {
            get
            {
                return achievements == MissionAchievements.ALL;
            }
        }

        public MissionAchievements achievements
        {
            get
            {
                return _achievements;
            }
        }

        public void SetAchievements(MissionAchievements achievements)
        {
            _achievements = achievements;
        }
    }
}
