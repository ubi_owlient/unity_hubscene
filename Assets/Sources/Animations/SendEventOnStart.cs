﻿using Service;
using UnityEngine;

namespace Animations
{
    public class SendEventOnStart : MonoBehaviour
    {
        public string[] outEventsToSend;
        bool isDone = false;
        int frameWaited = 0;
        public int frameToWait = 0;
        public bool isSendEachFrame = false;
        public bool isSendOnEnable = false;
        
        private void OnEnable()
        {
            if (isSendOnEnable == true)
            {
                isDone = false;
                frameWaited = 0;
            }
        }

        private void Update()
        {
            if (isDone == false)
            {
                ++frameWaited;
                if ((frameWaited >= frameToWait) || isSendEachFrame == true)
                {
                    isDone = frameWaited >= frameToWait;
                    ServiceManager.Instance.GetService<AnimationService>().SendEvent(outEventsToSend, gameObject);
                }
            }
        }
    }
}