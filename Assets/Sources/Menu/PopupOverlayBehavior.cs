﻿using UnityEngine;

namespace Menu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class PopupOverlayBehavior : MonoBehaviour
    {
        [SerializeField]
        private GameObject _root = null;

        private Animator _anim = null;

        private bool _isVisible = false;

        void Start()
        {
            if ( _root != null )
            {
                _anim = _root.GetComponent<Animator>();
            }
        }

        public void Show()
        {
            if ( _isVisible == false )
            {
                if ( _anim != null )
                {
                    _anim.SetTrigger( "Show" );
                }

                _isVisible = true;
            }
        }

        public void Hide()
        {
            if ( _isVisible == true )
            {
                if ( _anim != null )
                {
                    _anim.SetTrigger( "Hide" );
                }

                _isVisible = false;
            }
        }
    }
}
