﻿using Enums;
using Service;
using System;
using System.Collections;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ActivityManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject _activityFocusPrefab = null;
        
        private IList _testActivityPool        = null;
        private ActivityEnum? _currentTest     = null;

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<ActivityService>().Initialize( _activityFocusPrefab );

            _testActivityPool = Enum.GetValues( typeof(ActivityEnum) );
        }

        void Update()
        {
            if ( Input.GetKeyDown(KeyCode.A) )
            {
                if ( _currentTest.HasValue )
                {
                    ServiceManager.Instance.GetService<ActivityService>().UnfocusActivity( _currentTest.Value );
                    _currentTest = null;
                }

                int index = UnityEngine.Random.Range( 0, _testActivityPool.Count - 1 );
                _currentTest = (ActivityEnum)_testActivityPool[ index ];

                ServiceManager.Instance.GetService<ActivityService>().FocusActivity( _currentTest.Value );
            }

            ServiceManager.Instance.GetService<ActivityService>().Update();
        }
    }
}
