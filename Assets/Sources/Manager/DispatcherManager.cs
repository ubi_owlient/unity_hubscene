﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class DispatcherManager : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Update()
        {
            ServiceManager.Instance.GetService<DispatcherService>().Update();
        }
    }
}
