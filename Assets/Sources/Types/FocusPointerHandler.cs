﻿using Enums;
using Service;
using UnityEngine;
using Utils;

namespace Types
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class FocusPointerHandler : MonoBehaviour
    {
        [SerializeField]
        private Camera       _camera   = null;
        
        [SerializeField]
        private ActivityEnum _activity = ActivityEnum.CAMPAIGN;

        [SerializeField]
        private Transform    _target   = null;

        private Transform    _pointer  = null;

        private float _borderSize = 100f;

        void Start()
        {
            ServiceManager.Instance.GetService<ActivityService>().onActivityFocused   += OnActivityFocused;
            ServiceManager.Instance.GetService<ActivityService>().onActivityUnfocused += OnActivityUnfocused;
        }

        void OnDestroy()
        {
            ServiceManager.Instance.GetService<ActivityService>().onActivityFocused   -= OnActivityFocused;
            ServiceManager.Instance.GetService<ActivityService>().onActivityUnfocused -= OnActivityUnfocused;
        }
        
        void Update()
        {
            if ( _pointer != null )
            {
                Vector3 targetPositionScreenPoint = _camera.WorldToScreenPoint(_target.position);
                bool isOffScreen = targetPositionScreenPoint.x <= _borderSize || 
                                   targetPositionScreenPoint.x >= Screen.width - _borderSize || 
                                   targetPositionScreenPoint.y <= _borderSize || 
                                   targetPositionScreenPoint.y >= Screen.height - _borderSize ||
                                   targetPositionScreenPoint.z < 0;
                
                if (isOffScreen)
                {
                    Vector3 cappedTargetScreenPosition = targetPositionScreenPoint;
                    if (cappedTargetScreenPosition.x <= _borderSize) cappedTargetScreenPosition.x = _borderSize;
                    if (cappedTargetScreenPosition.x >= Screen.width - _borderSize) cappedTargetScreenPosition.x = Screen.width - _borderSize;
                    if (cappedTargetScreenPosition.y <= _borderSize) cappedTargetScreenPosition.y = _borderSize;
                    if (cappedTargetScreenPosition.y >= Screen.height - _borderSize) cappedTargetScreenPosition.y = Screen.height - _borderSize;
                    if ( cappedTargetScreenPosition.z > 0 )
                    {
                        _pointer.position = cappedTargetScreenPosition;
                        RotatePointerTowardsTargetPosition(targetPositionScreenPoint, cappedTargetScreenPosition);
                    }
                    else
                    {
                        if ( cappedTargetScreenPosition.x < Screen.width / 10 )
                        {
                            cappedTargetScreenPosition.x = Screen.width - _borderSize;
                            cappedTargetScreenPosition.y = _borderSize; // Set it at bottom
                            _pointer.position = cappedTargetScreenPosition;
                            _pointer.localEulerAngles = new Vector3(0, 0, -45);
                        }
                        else if ( cappedTargetScreenPosition.x > (Screen.width - Screen.width / 10) )
                        {
                            cappedTargetScreenPosition.x = _borderSize;
                            cappedTargetScreenPosition.y = _borderSize; // Set it at bottom
                            _pointer.position = cappedTargetScreenPosition;
                            _pointer.localEulerAngles = new Vector3(0, 0, -135);
                        }
                        else
                        {
                            cappedTargetScreenPosition.y = _borderSize; // Set it at bottom
                            cappedTargetScreenPosition.x = Screen.width - cappedTargetScreenPosition.x;
                            _pointer.position = cappedTargetScreenPosition;
                            _pointer.localEulerAngles = new Vector3(0, 0, -90);
                        }
                    }
                }
                else
                {
                    _pointer.position = targetPositionScreenPoint;
                    _pointer.localPosition = new Vector3( _pointer.localPosition.x - 30, _pointer.localPosition.y + 20, 0f);
                    _pointer.localEulerAngles = Vector3.zero;
                }
            }
        }

        private void RotatePointerTowardsTargetPosition(Vector3 to, Vector3 from)
        {
            Vector3 toPosition   = to;
            Vector3 fromPosition = from;
            fromPosition.z = 0f;
            Vector3 dir = (toPosition - fromPosition).normalized;
            float angle = Utility.GetAngleFromVectorFloat(dir);
            _pointer.localEulerAngles = new Vector3(0, 0, angle);
        }

        private bool IsInFrontOfPlayer()
        {
            Vector3 toTarget = (_target.position - _camera.transform.position).normalized;

            if (Vector3.Dot(toTarget, _camera.transform.forward) > 0)
            {
                Debug.Log( "In Front" );
                return true;
            }
            else
            {
                Debug.Log( "In Back" );
                return false;
            }
        }

        private void OnActivityFocused(ActivityService sender, ActivityEnum activity, GameObject pointer)
        {
            if ( _activity != activity )
            {
                return;
            }

            _pointer = pointer.transform;
            _target.gameObject.SetActive( true );
        }

        private void OnActivityUnfocused(ActivityService sender, ActivityEnum activity, GameObject pointer)
        {
            if ( _activity != activity )
            {
                return;
            }

            _pointer = null;
            _target.gameObject.SetActive( false );
        }
    }
}
