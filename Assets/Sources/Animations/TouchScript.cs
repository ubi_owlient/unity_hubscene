using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using Service;

namespace Animations
{
    public class TouchScript : Receiver
    {
        public GameObject targetObject;

        private bool isSelected = false;
        private bool isMoving = false;

        public bool canMove = false;
        public bool isValide = false;
        private Vector3 posZone;
        private Vector3 posInit;

        public string inActivateEvent;
        public string inDesactivateEvent;
        public string inStartEvent;
        public string inStopEvent;
        public string[] outPusheds;
        public string[] outReleaseds;
        public string[] outMoveds;
        public string[] outUnmoveds;
        public string inActiveSnap;
        public string inDesactiveSnap;

        private bool isStarted = false;


        void Start()
        {
            if (targetObject == null)
            {
                targetObject = gameObject;
            }

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            inActiveSnap       = animService.PrepareEventName(inActiveSnap, gameObject);
            inDesactiveSnap    = animService.PrepareEventName(inDesactiveSnap, gameObject);
            inStopEvent        = animService.PrepareEventName(inStopEvent, gameObject);
            inActivateEvent    = animService.PrepareEventName(inActivateEvent, gameObject);
            inDesactivateEvent = animService.PrepareEventName(inDesactivateEvent, gameObject);
            inStartEvent       = animService.PrepareEventName(inStartEvent, gameObject);


            if (inStartEvent.Length > 0)
            {
                animService.Register(inStartEvent, this);
            }
            else
            {
                isStarted = true;
            }

            animService.Register(inActiveSnap, this);
            animService.Register(inDesactiveSnap, this);
            animService.Register(inStopEvent, this);
            animService.Register(inActivateEvent, this);
            animService.Register(inDesactivateEvent, this);

            posInit = targetObject.transform.position;
        }

        private void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            animService.Unregister(inDesactiveSnap, this);
            animService.Unregister(inActiveSnap, this);
            animService.Unregister(inStartEvent, this);
            animService.Unregister(inStopEvent, this);
            animService.Unregister(inActivateEvent, this);
            animService.Unregister(inDesactivateEvent, this);
        }

        private bool IsPointerOverGameObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        private void OnMouseDown()
        {
            if (!IsPointerOverGameObject())
            {
                if (isStarted)
                {
                    isSelected = true;
                    isMoving = false;
                    //Debug.Log("PUSH");
                    ServiceManager.Instance.GetService<AnimationService>().SendEvent(outPusheds, targetObject.gameObject);
                }
            }
        }

        private void OnMouseUp()
        {
            if (!IsPointerOverGameObject())
            {
                if (isSelected && isStarted)
                {
                    isSelected = false;
                    ServiceManager.Instance.GetService<AnimationService>().SendEvent(outReleaseds, targetObject.gameObject);

                    if (isValide)
                    {
                        targetObject.transform.position = posZone;
                        canMove = false;
                    }

                    if (canMove == true)
                    {
                        targetObject.transform.position = posInit;
                    }
                }
            }
        }

        private void Update()
        {
            if (isStarted)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    {
                        RaycastHit hit;
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(ray, out hit, 200.0f))
                        {
                            if (hit.collider.gameObject == targetObject)
                            {
                                OnMouseDown();
                            }
                        }
                    }
                }

                if (Input.GetMouseButton(0))
                {
                    if (isSelected)
                    {
                        if (canMove == true)
                        {
                            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                            pos.z = targetObject.transform.position.z;
                            targetObject.transform.position = pos;

                            if (isMoving == false)
                            {
                                isMoving = true;
                                ServiceManager.Instance.GetService<AnimationService>().SendEvent(outMoveds, targetObject.gameObject);
                            }
                        }
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(ray, out hit, 200.0f))
                    {
                        if (hit.collider.gameObject == targetObject)
                            OnMouseUp();
                    }
                    ServiceManager.Instance.GetService<AnimationService>().SendEvent(outUnmoveds, targetObject.gameObject);
                }
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            if (eventName.Equals(inStartEvent))
            {
                isStarted = true;
            }

            if (eventName.Equals(inStopEvent))
            {
                isStarted = false;
            }

            if (eventName.Equals(inActivateEvent))
            {
                targetObject.SetActive(true);
            }

            if (eventName.Equals(inDesactivateEvent))
            {
                targetObject.SetActive(false);
            }

            if (eventName.Equals(inActiveSnap))
            {
                posZone = obj.transform.position;
                isValide = true;
            }

            if (eventName.Equals(inDesactiveSnap))
            {
                posZone = posInit;
                isValide = false;
            }
        }
    }
}