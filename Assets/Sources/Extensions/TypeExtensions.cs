﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Extensions
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Stores the static instance of an empty type array.
        /// </summary>
        private static readonly Type[] sEmptyTypes = new Type[0];

        /// <summary>
        /// Stores the set of inherited types by base type in cache to speed up 
        /// next call(s) of GetInheritedTypes().
        /// </summary>
        private static Dictionary<Type, List<Type>> sInheritedsByBaseType = new Dictionary<Type, List<Type>>();

        /// <summary>
        /// Checks whether the type is nullable or not.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNullableType(this Type type)
        {
            if ( type.IsGenericType &&
                 type.GetGenericTypeDefinition() == typeof(Nullable<>) )
            {
                return true;
            }
            else
            {
                return Nullable.GetUnderlyingType( type ) != null;
            }
        }

        /// <summary>
        /// Retrieves the inherited types from a supplied type whatever it is an
        /// interface, abstract or concret base class type ONLY in the very same assembly.
        /// </summary>
        /// <param name="baseType">The base object type</param>
        /// <returns>The set of inherited type(s) from the base type. At least an empty list.</returns>
        public static IEnumerable<Type> GetInheritedTypes(this Type baseType)
        {
            List<Type> inheriteds;
            if ( sInheritedsByBaseType.TryGetValue( baseType, out inheriteds ) )
            {
                return inheriteds;
            }
            else
            {
                // Else look in the assembly of the supplied type ONLY.
                inheriteds = new List<Type>( baseType.Assembly.GetTypes().Where( pElt => baseType.IsAssignableFrom( pElt ) && pElt.IsAbstract == false ) );
                sInheritedsByBaseType.Add( baseType, inheriteds );
                return inheriteds;
            }
        }

        /// <summary>
        /// Retrieves the inherited types from a supplied type whatever it is an
        /// interface, abstract or concret base class type IN ALL assemblies.
        /// </summary>
        /// <param name="baseType">The base object type</param>
        /// <returns>The set of inherited type(s) from the base type. At least an empty list.</returns>
        public static IEnumerable<Type> GetAllInheritedTypes(this Type baseType)
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany( pAssembly => pAssembly.GetTypes() ).Where( pType => baseType.IsAssignableFrom( pType ) && pType.IsAbstract == false );
        }

        /// <summary>
        /// Creates an instance of a type.
        /// </summary>
        /// <param name="type">The type to create.</param>
        /// <param name="parameters">The parameters to create an instance.</param>
        /// <returns></returns>
        public static object CreateInstance(this Type type, params object[] parameters)
        {
            if ( type.IsAbstract )
            {
                return null;
            }

            ConstructorInfo constructor;
            // Default constructor.
            if ( parameters == null ||
                 parameters.Length == 0 )
            {
                constructor = type.GetConstructor( sEmptyTypes );
            }
            else
            {
                Type[] paramTypes = parameters.Select( pElt => pElt.GetType() ).ToArray();
                constructor = type.GetConstructor( paramTypes );
            }

            if ( constructor != null )
            {
                return constructor.Invoke( parameters );
            }

            return null;
        }
    }
}
