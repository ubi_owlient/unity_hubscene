﻿using Enums;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Buttons
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CurrencyButton : StandardButton
    {
        [SerializeField] private Sprite _callToActionBackground = null;
        [SerializeField] private Sprite _notEnoughBackground    = null;

        [Header("Title")]
        [SerializeField]
        private Text  _title    = null;

        [Header("Currency")]
        [SerializeField]
        private Text  _price    = null;

        [SerializeField]
        private Image _currency = null;

        protected override void Initialize()
        {
            base.Initialize();

            this.AddState( ButtonStateEnum.CALL_TO_ACTION, _callToActionBackground, Color.white );
            this.AddState( ButtonStateEnum.NOT_ENOUGH,     _notEnoughBackground,    Color.white );
        }

        public void RefreshTitle(string title)
        {
            if ( _title != null )
            {
                _title.text = title;
            }
        }

        public void RefreshCurrency(string price, Sprite currencyIcon = null)
        {
            if ( _price != null )
            {
                _price.text = price;
            }

            if ( _currency != null )
            {
                _currency.sprite = currencyIcon;
            }
        }
    }
}
