﻿using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class EventModel
    {
        private bool   _isLocked            = false;
        private uint   _eventId             = 0;
        private string _eventTitle          = null;
        private string _eventDescription    = null;
        private string _eventImage          = null;
        private string _eventBackground     = null;
        private Dictionary<uint, MissionModel> _missionsById = new Dictionary<uint, MissionModel>();

        public uint eventId
        {
            get
            {
                return _eventId;
            }
        }

        public string eventTitle
        {
            get
            {
                return _eventTitle;
            }
        }

        public string eventDescription
        {
            get
            {
                return _eventDescription;
            }
        }

        public string eventImage
        {
            get
            {
                return _eventImage;
            }
        }

        public string eventBackground
        {
            get
            {
                return _eventBackground;
            }
        }

        public int missionCount
        {
            get
            {
                return _missionsById.Count;
            }
        }

        public IEnumerable<MissionModel> missions
        {
            get
            {
                return _missionsById.Values;
            }
        }

        public bool isLocked
        {
            get
            {
                return _isLocked;
            }
        }

        public EventModel(uint eventId, string eventTitle, string eventDesc, string eventImage, string eventBackground)
        {
            _eventId = eventId;
            _eventTitle = eventTitle;
            _eventDescription = eventDesc;
            _eventImage = eventImage;
            _eventBackground = eventBackground;
        }

        public void Lock()
        {
            _isLocked = true;
        }

        public void Unlock()
        {
            _isLocked = false;
        }

        public void AddMission(MissionModel mission)
        {
            _missionsById.Add(mission.missionId, mission);
        }

        public void RemoveMission(MissionModel mission)
        {
            RemoveMission( mission.missionId );
        }

        public void RemoveMission(uint missionId)
        {
            _missionsById.Remove(missionId);
        }
    }
}
