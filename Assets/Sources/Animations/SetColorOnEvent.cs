﻿using Service;
using UnityEngine;
using UnityEngine.UI;

namespace Animations
{
    public class SetColorOnEvent : Receiver
    {
        [System.Serializable]
        public struct sActions
        {
            public string inEventSetColor;
            public Color colorToSet;
            public GameObject targetObject;
        };

        public sActions[] actions;

        
        void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                actions[i].inEventSetColor = animService.PrepareEventName(actions[i].inEventSetColor, gameObject);
                animService.Register(actions[i].inEventSetColor, this);
            }
        }

        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                animService.Unregister(actions[i].inEventSetColor, this);
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            for (int i = 0; i < actions.Length; ++i)
            {
                sActions curA = actions[i];
                GameObject curobj = curA.targetObject != null ? curA.targetObject : gameObject;
                if (eventName.Equals(curA.inEventSetColor))
                {
                    SpriteRenderer sprRdr = (curobj.GetComponent<Renderer>() as SpriteRenderer);
                    if (sprRdr != null)
                    {
                        sprRdr.color = curA.colorToSet;
                    }
                    TextMesh txtMesh = (curobj.GetComponent<TextMesh>());
                    if (txtMesh != null)
                    {
                        txtMesh.color = curA.colorToSet;
                    }
                    Text txtTxt = (curobj.GetComponent<Text>());
                    if (txtTxt != null)
                    {
                        txtTxt.color = curA.colorToSet;
                    }
                    Image imIm = (curobj.GetComponent<Image>());
                    if (imIm != null)
                    {
                        imIm.color = curA.colorToSet;
                    }
                }
            }
        }
    }
}