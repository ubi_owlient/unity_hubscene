﻿using Scriptable.Tab;
using System;
using System.Collections.Generic;
using UnityEngine;
using Widgets;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class TabView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _tabHeaderRoot  = null;

        [SerializeField]
        private GameObject _tabContentRoot = null;

        [SerializeField]
        private List<TabDefinition> _tabDescriptions = new List<TabDefinition>();

        private List<TabWidget> _tabs = new List<TabWidget>();

        public event Action<TabView> isInitialized = null;
        public event Action<TabView, TabWidget> tabClicked  = null;

        void Start()
        {
            for ( uint curr = 0; curr < _tabDescriptions.Count; curr++ )
            {
                TabDefinition definition = _tabDescriptions[ (int)curr ];

                TabWidget widget = definition.CreateTab( curr, _tabHeaderRoot, _tabContentRoot );
                widget.onTabClick += OnTabClicked;
                _tabs.Add( widget );
            }

            NotifyInitialized();
        }

        void OnDestroy()
        {
            for (int curr = 0; curr < _tabs.Count; curr++)
            {
                _tabs[ curr ].Dispose();
            }
            _tabs.Clear();
        }

        private void NotifyInitialized()
        {
            if ( isInitialized != null )
            {
                isInitialized( this );
            }
        }

        private void OnTabClicked(TabWidget sender, TabHeaderView tabView)
        {
            SelectTab( sender.index );

            if ( tabClicked != null )
            {
                tabClicked( this, sender );
            }
        }

        public T GetTabContentComponent<T>(uint index) where T : class
        {
            if ( index < _tabs.Count )
            {
                return _tabs[ (int)index ].GetContentComponent<T>();
            }

            return null;
        }

        public void RefreshTitle(uint index, string title)
        {
            if ( index < _tabs.Count )
            {
                _tabs[ (int)index ].RefreshTitle( title.ToUpper() );
            }
        }

        public void RefreshContent(uint index)
        {
            if ( index < _tabs.Count )
            {
                _tabs[ (int)index ].RefreshContent();
            }
        }

        public void SelectTab(uint index)
        {
            for (int curr = 0; curr < _tabs.Count; curr++)
            {
                if ( curr == index )
                {
                    _tabs[ curr ].Select();
                }
                else
                {
                    _tabs[ curr ].Unselect();
                }
            }
        }
    }
}
