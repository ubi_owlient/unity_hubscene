using Service;
using UnityEngine;
using UnityEngine.UI;

namespace Animations
{
    public class SetSpriteOnEvent : Receiver
    {
        [System.Serializable]
        public struct sActions
        {
            public string inEventSetSprite;
            public Sprite spriteToSet;
            public GameObject targetObject;
        };

        public sActions[] actions;

        private void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                actions[i].inEventSetSprite = animService.PrepareEventName(actions[i].inEventSetSprite, gameObject);
                animService.Register(actions[i].inEventSetSprite, this);
            }
        }

        private void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                animService.Unregister(actions[i].inEventSetSprite, this);
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            for (int i = 0; i < actions.Length; ++i)
            {
                sActions curA = actions[i];
                GameObject curobj = curA.targetObject != null ? curA.targetObject : gameObject;
                if (eventName.Equals(curA.inEventSetSprite))
                {
                    SpriteRenderer r = (curobj.GetComponent<Renderer>() as SpriteRenderer);
                    if (r != null)
                    {
                        r.sprite = curA.spriteToSet;
                    }
                    Image img = curobj.GetComponent<Image>();
                    if (img != null)
                    {
                        img.sprite = curA.spriteToSet;
                    }
                }
            }
        }
    }
}