﻿using Models;
using Service;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class GachaPopulator : AItemPopulator<GachaTileView>
    {
        protected override void OnCustomInitialize()
        {
            //Populate( ServiceManager.Instance.GetService<CampaignService>().campaigns );
        }

        private void Populate(IEnumerable<GachaOffer> offers)
        {
            uint count = 0;
            foreach ( GachaOffer offer in offers )
            {
                GachaTileView widget = pool.GetItem( count );
                widget.SetOfferInfo( offer );
                count++;
            }
        }
    }
}
