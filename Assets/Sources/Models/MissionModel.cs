﻿using Types;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class MissionModel
    {
        private uint         _missionId   = 0;
        private string       _missionName = null;
        private MissionState _state       = null;

        public uint missionId
        {
            get
            {
                return _missionId;
            }
        }

        public string missionName
        {
            get
            {
                return _missionName;
            }
        }

        public MissionState state
        {
            get
            {
                return _state;
            }
        }

        public MissionModel(uint missionId, string missionName, MissionState state = null)
        {
            _missionId = missionId;
            _missionName = missionName;
            _state = state == null ? new MissionState() : state;
        }


    }
}
