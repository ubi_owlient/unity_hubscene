﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class SettingOptionModel
    {
        private string _settingOptionId;

        public string settingOptionId
        {
            get
            {
                return _settingOptionId;
            }
        }

        public SettingOptionModel(string id)
        {
            _settingOptionId = id;
        }
    }
}
