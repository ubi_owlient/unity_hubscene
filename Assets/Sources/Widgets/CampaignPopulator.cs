﻿using Models;
using Service;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CampaignPopulator : AItemPopulator<CampaignTileView>
    {
        protected override void OnCustomInitialize()
        {
            Populate( ServiceManager.Instance.GetService<CampaignService>().campaigns );
        }

        private void Populate(IEnumerable<CampaignModel> campaigns)
        {
            uint count = 0;
            foreach ( CampaignModel campaign in campaigns )
            {
                CampaignTileView widget = pool.GetItem( count );
                widget.SetCampaignInfo( campaign );
                count++;
            }
        }
    }
}
