﻿using Commands;
using FeatureModels;
using Service;
using System;
using UnityEngine;
using ViewModels;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class AView : MonoBehaviour, IView
    {
        public abstract IViewModel model
        {
            get;
        }

        public IFeatureModel GetModel(Type modelType)
        {
            return ServiceManager.Instance.GetService<ModelService>().GetModel( modelType );
        }

        public T GetModel<T>() where T : class, IFeatureModel
        {
            Type modelType = typeof(T);
            return GetModel( modelType ) as T;
        }

        public void Dispatch(ICommand command)
        {
            ServiceManager.Instance.GetService<DispatcherService>().Dispatch( command );
        }
    }
}
