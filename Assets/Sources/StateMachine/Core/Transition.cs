﻿using System;

namespace FSM
{
    /// <summary>
    /// Definition of the <see cref="Transition"/> class.
    /// 
    /// Represents a transition between two states.
    /// </summary>
    public struct Transition
    {
        public TransitionType TransitionType;
        public Type TargetStateType;
        public object[] Args;

        public Transition(TransitionType transitionType, Type targetStateType, object[] args)
        {
            TransitionType  = transitionType;
            TargetStateType = targetStateType;
            Args = args;
        }

        public override string ToString()
        {
            return TransitionType.ToString();
        }

        // None transition functions
        public static Transition None()
        {
            return new Transition(TransitionType.None, null, null);
        }

        // Inner transition functions
        
        public static Transition Inner(Type targetStateType, params object[] args)
        {
            return new Transition(TransitionType.Inner, targetStateType, args );
        }
        
        public static Transition Inner<TargetStateType>(params object[] args) where TargetStateType : AState
        {
            return Inner(typeof(TargetStateType), args);
        }
        
        // InnerEntry transition functions

        public static Transition InnerEntry(Type targetStateType, params object[] args)
        {
            return new Transition(TransitionType.InnerEntry, targetStateType, args );
        }
        
        public static Transition InnerEntry<TargetStateType>(params object[] args) where TargetStateType : AState
        {
            return InnerEntry(typeof(TargetStateType), args);
        }
        
        // Sibling transition functions
        
        public static Transition Sibling(Type targetStateType, params object[] args)
        {
            return new Transition(TransitionType.Sibling, targetStateType, args );
        }
        
        public static Transition Sibling<TargetStateType>(params object[] args) where TargetStateType : AState
        {
            return Sibling(typeof(TargetStateType), args);
        }
    }
}
