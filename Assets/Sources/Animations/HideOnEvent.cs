using Service;
using UnityEngine;

namespace Animations
{
    public class HideOnEvent : Receiver
    {
        public GameObject targetObject;
        public string inHide;
        public string inShow;
        public string inScriptDesactivate;
        public string inScriptActivate;

        void Start()
        {
            if (targetObject == null)
            {
                targetObject = gameObject;
            }

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            inHide = animService.PrepareEventName(inHide, targetObject.gameObject);
            inShow = animService.PrepareEventName(inShow, targetObject.gameObject);
            inScriptDesactivate = animService.PrepareEventName(inScriptDesactivate, targetObject.gameObject);
            inScriptActivate    = animService.PrepareEventName(inScriptActivate, targetObject.gameObject);

            animService.Register(inHide, this);
            animService.Register(inShow, this);
            animService.Register(inScriptDesactivate, this);
            animService.Register(inScriptActivate, this);
        }

        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            animService.Unregister(inHide, this);
            animService.Unregister(inShow, this);
            animService.Unregister(inScriptDesactivate, this);
            animService.Unregister(inScriptActivate, this);
        }

        public override void Receive(string eventName, GameObject obj)
        {
            if (eventName.Equals(inHide))
            {
                targetObject.SetActive(false);
            }
            if (eventName.Equals(inShow))
            {
                targetObject.SetActive(true);
            }
            if (eventName.Equals(inScriptDesactivate))
            {
                MonoBehaviour[] scriptComponents = targetObject.GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour script in scriptComponents)
                {
                    script.enabled = false;
                }
            }
            if (eventName.Equals(inScriptActivate))
            {
                MonoBehaviour[] scriptComponents = targetObject.GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour script in scriptComponents)
                {
                    script.enabled = true;
                }
            }
        }
    }
}