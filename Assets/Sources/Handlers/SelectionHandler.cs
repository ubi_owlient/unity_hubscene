﻿using Service;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Handlers
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SelectionHandler : MonoBehaviour, ISelectHandler, IDeselectHandler
    {
        [SerializeField]
        private GameObject _target = null;

        private ISelectHandler   _parentSelectHandler   = null;
        private IDeselectHandler _parentDeselectHandler = null;

        public event Action<GameObject> selected   = null;
        public event Action<GameObject> unselected = null;

        void Start()
        {
            if ( _target != null )
            {
                _parentSelectHandler   = _target.GetComponentInParent<ISelectHandler>();
                _parentDeselectHandler = _target.GetComponentInParent<IDeselectHandler>();
            }
        }

        public void OnDeselect(BaseEventData eventData)
        {
            if ( _parentDeselectHandler != null )
            {
                _parentDeselectHandler.OnDeselect( eventData );
            }

            if ( unselected != null )
            {
                unselected( eventData.selectedObject );
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            if ( _parentSelectHandler != null )
            {
                _parentSelectHandler.OnSelect( eventData );
            }

            if ( selected != null )
            {
                selected( eventData.selectedObject );
            }

            ServiceManager.Instance.GetService<SelectionService>().SetSelection( eventData.selectedObject.transform );
        }
    }
}
