﻿using ViewModels;

namespace Commands.HubMenu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HomeMenuCommand : ACommand
    {
        public HomeMenuCommand(IModelPresenter sender) : 
        base(sender)
        {

        }
    }
}
