﻿namespace Utils
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public static class Constants
    {
        public const string TEXTURE_UI_PATH = @"Textures\UI\";
        public const string TEXTURE_REWARD_PATH = @"Textures\Rewards\";
        public const string TEXTURE_BACKGROUND_PATH = @"Textures\Backgrounds\";

        public const string HUB_CAMERA_ID = "Hub_Camera";

        public const string OPTIONS_POPUP_ID  = "OptionsPopup";
        public const string MAILS_POPUP_ID    = "MailsPopup";
        public const string CALENDAR_POPUP_ID = "CalendarPopup";
        public const string MOTD_POPUP_ID     = "MOTDPopup";
        public const string CHAT_POPUP_ID     = "ChatPopup";

        public const string BUNDLE_CATEGORY    = "Bundles";
        public const string RESOURCES_CATEGORY = "Resources";

        public const string GACHA_FEATURE = "Gacha";
        public const string STORE_FEATURE = "Store";
        public const string GUILD_FEATURE = "Guild";
        public const string HUB_FEATURE   = "HUB";
        public const string SETTINGS_FEATURE = "Settings";
    }
}
