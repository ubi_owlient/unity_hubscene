﻿using Service;
using UnityEngine;

namespace Animations
{
    public class TimerManager : MonoBehaviour
    {
        [System.Serializable]
        public struct sTextColorFromTimer
        {
            public float percent;
            public Color color;
            public string[] outEvents;
        };

        public sTextColorFromTimer[] colors;

        private TextMesh text;
        private Transform FL;
        private Transform FR;
        private int iCurColor = -1;


        private void Start()
        {
            text = transform.Find("text").GetComponent<TextMesh>();
            FL = transform.Find("FL");
            FR = transform.Find("FR");
        }

        public void UpdateTimer(float curElaspedTime, float curTotalTime)
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            float dt = curTotalTime - curElaspedTime;
            text = transform.Find("text").GetComponent<TextMesh>();
            FL = transform.Find("FL");
            FR = transform.Find("FR");
            if ((text != null) && (FL != null) && (FR != null))
            {
                text.text = "" + Mathf.RoundToInt(dt);

                dt /= curTotalTime;

                for (int i = 1; i < colors.Length; ++i)
                {
                    if (dt > colors[i].percent)
                    {
                        if (iCurColor != i)
                        {
                            iCurColor = i;
                            animService.SendEvent(colors[i].outEvents, gameObject);
                        }
                        float coef = (dt - colors[i].percent) / (colors[i - 1].percent - colors[i].percent);
                        text.color = colors[i - 1].color * coef + colors[i].color * (1.0f - coef);
                        break;
                    }
                }

                if (dt > 0.5f)
                {
                    Quaternion rot = new Quaternion();
                    rot.eulerAngles = new Vector3(0, 0, dt * -360);
                    FL.transform.localRotation = rot;

                    rot.eulerAngles = new Vector3(0, 0, 0);
                    FR.transform.localRotation = rot;
                }
                else
                {
                    FL.gameObject.SetActive(false);

                    Quaternion rot = new Quaternion();
                    rot.eulerAngles = new Vector3(0, 0, dt * -360 + 180);
                    FR.transform.localRotation = rot;
                }
            }
        }
    }
}