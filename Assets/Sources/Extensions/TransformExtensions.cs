﻿using UnityEngine;

namespace Extensions
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public static class TransformExtensions
    {
        /// <summary>
        /// Clears all children.
        /// </summary>
        /// <param name="transform"></param>
        public static void ClearChildren(this Transform transform)
        {
            foreach (Transform child in transform )
            {
                GameObject.Destroy( child.gameObject );
            }
            transform.DetachChildren();
        }
    }
}
