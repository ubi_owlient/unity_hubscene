﻿using Commands;
using Commands.HubMenu;
using FeatureModels;
using Models;
using Service;

namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// Definition of the base <see cref="AHubState"/> abstract class.
    /// </summary>
    public abstract class AHubState : ABaseState
    {
        protected ActivityModel _activity = null;

        protected ActivityStep _step = null;

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);
            
            _activity = args[ 0 ] as ActivityModel;
            HubModel hubModel = GetModel<HubModel>();
            if ( _activity != null &&
                 hubModel != null )
            {
                hubModel.currentActivity = _activity;
            }
        }

        [DispatcherService.HandleCommand(commandType = typeof(EnterHubMenuCommand))]
        protected void OnMenuEntered(ICommand command)
        {
            _step = _activity.steps[ 0 ];

            HubModel hubModel = GetModel<HubModel>();
            if ( _step != null &&
                 hubModel != null )
            {
                hubModel.currentStep = _step;
            }
        }
        
        public override void OnExit()
        {
            base.OnExit();
            
            //if ( _activity != null )
            //{
            //    _activity.isOpen = false;
            //}

            _step = null;
        }
    }
}
