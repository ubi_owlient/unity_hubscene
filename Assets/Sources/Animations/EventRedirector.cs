using Service;
using System.Collections;
using UnityEngine;

namespace Animations
{
    public class EventRedirector : Receiver
    {
        [System.Serializable]
        public struct eventRedirection
        {
            public string inEvent;
            public string[] outEvents;
            public float delay;
        };

        public eventRedirection[] redirections;

        void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < redirections.Length; ++i)
            {
                redirections[i].inEvent = animService.PrepareEventName(redirections[i].inEvent, gameObject);
                animService.Register(redirections[i].inEvent, this);
            }
        }
        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < redirections.Length; ++i)
            {
                animService.Unregister(redirections[i].inEvent, this);
            }
        }

        IEnumerator DelaySend(eventRedirection events)
        {
            yield return new WaitForSeconds(events.delay);

            ServiceManager.Instance.GetService<AnimationService>().SendEvent(events.outEvents, gameObject);
        }

        public override void Receive(string eventName, GameObject obj)
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < redirections.Length; ++i)
            {
                if (eventName.Equals(redirections[i].inEvent))
                {
                    if (redirections[i].delay <= 0)
                    {
                        animService.SendEvent(redirections[i].outEvents, gameObject);
                    }
                    else if (gameObject.activeInHierarchy)
                    {
                        StartCoroutine(DelaySend(redirections[i]));
                    }
                }
            }
        }
    }
}