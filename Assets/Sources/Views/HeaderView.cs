﻿using Menu.Buttons;
using UnityEngine;
using Utils;
using ViewModels;
using Views;
using Commands.Popup;
using Commands.Settings;

namespace Hub
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HeaderView : AView
    {
        [SerializeField]
        private ImageButton _optionsButton  = null;

        [SerializeField]
        private ImageButton _mailButton     = null;

        [SerializeField]
        private ImageButton _calendarButton = null;

        [SerializeField]
        private ImageButton _motdButton     = null;

        [SerializeField]
        private ImageButton _chatButton     = null;

        public override IViewModel model
        {
            get
            {
                return null;
            }
        }

        void Start()
        {
            _optionsButton.onClick += OnOptionsClick;
            _mailButton.onClick += OnMailClick;
            _calendarButton.onClick += OnCalendarClick;
            _motdButton.onClick += OnMOTDClick;
            _chatButton.onClick += OnChatClick;
        }

        void OnDestroy()
        {
            _optionsButton.onClick -= OnOptionsClick;
            _mailButton.onClick -= OnMailClick;
            _calendarButton.onClick -= OnCalendarClick;
            _motdButton.onClick -= OnMOTDClick;
            _chatButton.onClick -= OnChatClick;
        }

        private void OnOptionsClick(ABaseButton button)
        {
            Dispatch( new ShowPopupCommand( this, Constants.OPTIONS_POPUP_ID ) );
        }

        private void OnMailClick(ABaseButton button)
        {
            Dispatch( new ShowPopupCommand( this, Constants.MAILS_POPUP_ID ) );
        }

        private void OnCalendarClick(ABaseButton button)
        {
            Dispatch( new ShowPopupCommand( this, Constants.CALENDAR_POPUP_ID ) );
        }

        private void OnMOTDClick(ABaseButton button)
        {
            Dispatch( new ShowPopupCommand( this, Constants.MOTD_POPUP_ID ) );
        }

        private void OnChatClick(ABaseButton button)
        {
            Dispatch( new ShowPopupCommand( this, Constants.CHAT_POPUP_ID ) );
        }
    }
}
