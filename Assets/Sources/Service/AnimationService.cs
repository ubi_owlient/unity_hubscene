﻿using Animations;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class AnimationService : AFeatureService
    {
        #region Fields

        private const int HIERARCHY_EXPLORATION_THRESHOLD = 5;
        
        private Dictionary<string, List<Receiver>> _receivers;

        private ConcurrentQueue<string> _delayedEvents;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimationService"/> class.
        /// </summary>
        public AnimationService()
        {
            _delayedEvents = new ConcurrentQueue<string>();
            _receivers = new Dictionary<string, List<Receiver>>();
        }

        #endregion Constructors

        #region Methods

        public void Initialize()
        {

        }

        public void Register(string eventName, Receiver receiver)
        {
            if ((eventName == null) || (eventName.Length == 0))
            {
                return;
            }

            if (_receivers.ContainsKey(eventName) == false)
            {

                _receivers.Add(eventName, new List<Receiver>());

            }

            if (_receivers[eventName].Contains(receiver) == false)
            {
                _receivers[eventName].Add(receiver);
            }
        }

        public void Unregister(string eventName, Receiver receiver)
        {
            if (eventName.Length == 0)
                return;

            if (_receivers.ContainsKey(eventName) == true)
            {
                if (_receivers[eventName].Contains(receiver) == true)
                {
                    _receivers[eventName].Remove(receiver);
                }
            }

        }

        public void UnregisterAll(Receiver receiver)
        {
            foreach (KeyValuePair<string, List<Receiver>> rcvs in _receivers)
            {
                if (rcvs.Value.Contains(receiver) == true)
                {
                    rcvs.Value.Remove(receiver);
                }
            }
        }

        public void Update()
        {
            string eventName = string.Empty;
            while( _delayedEvents.Count > 0 &&
                   _delayedEvents.TryDequeue( out eventName ) )
            {
                SendEvent( eventName );
            }
        }

        public void SendDelayedEvent(string eventName)
        {
            _delayedEvents.Enqueue( eventName );
        }

        public void SendEvent(string eventName)
        {
            SendEvent(eventName, null);
        }

        public void SendEvent(string[] eventName, GameObject obj)
        {
            if (eventName.Length == 0)
            {
                return;
            }

            for (int i = 0; i < eventName.Length; i++)
            {
                SendEvent(eventName[i], obj);
            }
        }

        public void SendEvent(string eventName, GameObject obj)
        {
            if (eventName.Length == 0)
            {
                return;
            }

            eventName = PrepareEventName(eventName, obj);
            bool bLocalMode = eventName.EndsWith("|") == true;

#if LOG_MOTION_EVENTS
            if (Debug.isDebugBuild == true)
            {
                if (eventName.Equals("ONPOSTRENDER") == false)
                {
                    Debug.Log("Send EVENT : " + eventName + " by " + (obj != null ? obj.name : "null"));
                }
            }
#endif
            if (_receivers.ContainsKey(eventName) == true)
            {
                List<Receiver> eventReceivers = _receivers[eventName];

                for (int i = 0; i < eventReceivers.Count; i++)
                {
                    Receiver currentReceiver = eventReceivers[i];

                    if (currentReceiver && (bLocalMode == false || obj == currentReceiver.gameObject))
                    {
                        currentReceiver.Receive(eventName, obj);
                    }
                }
            }
        }

        public string PrepareEventName(string eventName, GameObject obj)
        {
            if (eventName == null)
            {
                return string.Empty;
            }

            if (eventName.StartsWith("+") == true)
            {
                eventName = obj.name + eventName.Substring(1);
            }

            if (eventName.StartsWith("*") == true)
            {
                string firstObject = obj.name;
                int iterationCount = 0;
                while (obj.transform.parent != null && obj.name.StartsWith("_") == false && obj.CompareTag("UI_Motion_Object_Identifier") == false)
                {
                    obj = obj.transform.parent.gameObject;
                    iterationCount++;
                }

                if (iterationCount > HIERARCHY_EXPLORATION_THRESHOLD)
                {
                    Debug.LogWarning(
                        "<color=orange><b>GABRIEL ! JULIEN !</b> ATTENTION AUX EVENTS ON VOUS VOIT !</color>\n" +
                        ">> [" + eventName + "] is looking too far up in the hierarchy. Did you miss to add the tag UI_Motion_Object_Identifier ?\n" +
                        "Went from <" + firstObject + "> up to object <" + obj.name + "> with " + iterationCount + " iterations."
                    );
                }
                eventName = obj.name + eventName.Substring(1);
            }

            if (eventName.StartsWith("@") == true)
            {
                eventName = SceneManager.GetActiveScene().name + eventName.Substring(1);
            }

            return eventName;
        }

        private IEnumerator WaitToSendEvent(string eventName)
        {
            yield return null;
            SendEvent(eventName);
        }

        #endregion Methods
    }
}