# Unity_HubScene

Hub scene example using Unity

                                       # MainHub #

![PrototypeHub.png](https://bitbucket.org/repo/E6yeeA8/images/1435804081-PrototypeHub.png)

                                    # Interactive3DMenu #

![Dynamic3DMenu.png](https://bitbucket.org/repo/E6yeeA8/images/3914616628-Dynamic3DMenu.png)

                                     # Curved UI Menus #

![CurvedUIMenu.png](https://bitbucket.org/repo/E6yeeA8/images/2427786496-CurvedUIMenu.png)

                                     # SideMenusAndNotif #

![DynamicSideMenuAndNotifications.png](https://bitbucket.org/repo/E6yeeA8/images/2298627421-DynamicSideMenuAndNotifications.png)

                                 # FPS To TPS between activities #

![FPSToTPSCamChangeBetweenHubActivities.png](https://bitbucket.org/repo/E6yeeA8/images/1581446593-FPSToTPSCamChangeBetweenHubActivities.png)

                                            # Popups #

![Popups.png](https://bitbucket.org/repo/E6yeeA8/images/3047734699-Popups.png)