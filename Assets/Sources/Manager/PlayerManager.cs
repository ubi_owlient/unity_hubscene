﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class PlayerManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject _player = null;

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<PlayerService>().Initialize( _player );
        }
    }
}
