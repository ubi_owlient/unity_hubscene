﻿using System;

namespace Views
{
    /// <summary>
    /// Definition of the <see cref="IItemWidget"/> interface.
    /// </summary>
    public interface IItemWidget : IDisposable
    {
        /// <summary>
        /// Event fired on click on the widget (if clickable)
        /// </summary>
        event Action<IItemWidget> onClick;

        /// <summary>
        /// Gets the item id.
        /// </summary>
        uint id
        {
            get;
        }
    }
}
