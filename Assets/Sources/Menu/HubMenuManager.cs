﻿using Commands.HubMenu;
using Enums;
using FeatureModels;
using Hub;
using Menu.Buttons;
using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ViewModels;
using Views;

namespace UI
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HubMenuManager : AView, ISelectHandler, IDeselectHandler
    {
        [SerializeField]
        private ActivityEnum _activity = ActivityEnum.NONE;

        [SerializeField]
        private Text _title = null;

        [SerializeField]
        private Text _secondTitle = null;

        [SerializeField]
        private Text _description = null;

        [SerializeField]
        private Text _dialogText  = null;

        [SerializeField]
        private StandardButton _hubButton = null;

        [SerializeField]
        private StandardButton _subButton = null;

        private HubMenuViewModel _viewModel = null;

        private HubPlayerPlaceholder _placeHolder = null;
        private Typer                _typer       = null;
        private Animator             _menuAnim    = null;
        private bool                 _isMenuOn    = false;

        public event Action<HubMenuManager> onMenuSelected = null;
        public event Action<HubMenuManager> onMenuEntered  = null;

        public ActivityEnum activity
        {
            get
            {
                return _activity;
            }
        }
        
        public override IViewModel model
        {
            get
            {
                return _viewModel;
            }
        }

        void Awake()
        {
            _typer    = GetComponentInChildren<Typer>();
            _menuAnim = GetComponent<Animator>();
        }

        void Start()
        {
            _placeHolder = GetComponentInParent<HubPlayerPlaceholder>();
            
            _viewModel = new HubMenuViewModel( _activity, GetModel<HubModel>() );
            _viewModel.PropertyChanged += OnPropertyChanged;
            
            if ( _hubButton != null )
            {
                _hubButton.onClick += OnButtonClick;
            }

            if ( _subButton != null )
            {
                _subButton.onClick += OnSubButtonClick;
            }

            UpdateTitles();
            UpdateDescription();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
        {
            switch (eventArgs.PropertyName)
            {
                case "isVisible":
                    {
                        if ( _viewModel.isVisible )
                        {
                            gameObject.SetActive( true );
                        }
                        else
                        {
                            gameObject.SetActive( false );
                        }
                    }
                    break;
                case "isOpen":
                    {
                        if ( _viewModel.isOpen )
                        {
                            OpenMenu();
                        }
                        else
                        {
                            CloseMenu();
                        }
                    }
                    break;
                case "menuTitle":
                    {
                        UpdateTitles();
                    }
                    break;
                case "menuDescription":
                    {
                        UpdateDescription();
                    }
                    break;
            }
        }

        private void UpdateTitles()
        {
            if ( _title != null )
            {
                _title.text = _viewModel.menuTitle;
            }

            if ( _secondTitle != null )
            {
                _secondTitle.text = _viewModel.menuTitle;
            }
        }

        private void UpdateDescription()
        {
            if ( _description != null )
            {
                _description.text = _viewModel.menuDescription;
            }
        }

        void OnDestroy()
        {
            if ( _viewModel != null )
            {
                _viewModel.PropertyChanged -= OnPropertyChanged;
            }

            if ( _hubButton != null )
            {
                _hubButton.onClick -= OnButtonClick;
            }

            if ( _subButton != null )
            {
                _subButton.onClick -= OnSubButtonClick;
            }
        }

        private void OnButtonClick(ABaseButton button)
        {
            Dispatch( new SelectHubMenuCommand( this, _activity, _placeHolder.playerPosition, _placeHolder.playerLookAt ) );

            if ( onMenuSelected != null )
            {
                onMenuSelected( this );
            }
        }

        private void OnSubButtonClick(ABaseButton button)
        {
            Dispatch( new EnterHubMenuCommand( this ) );

            if ( onMenuEntered != null )
            {
                onMenuEntered( this );
            }
        }

        public void OnSelect(BaseEventData eventData)
        {
            
        }

        public void OnDeselect(BaseEventData eventData)
        {

        }
        
        /// <summary>
        /// Open menu method.
        /// </summary>
        private void OpenMenu()
        {
            if ( _isMenuOn == false )
            {
                _menuAnim.SetTrigger( "FadeIn" );
                _typer.StartCoroutine( "TypeIn" );
                
                _isMenuOn = true;

                _hubButton.Select();
            }
        }

        /// <summary>
        /// Close menu method.
        /// </summary>
        private void CloseMenu()
        {
            if ( _isMenuOn )
            {
                _menuAnim.SetTrigger( "FadeOut" );
                _typer.StartCoroutine( "TypeOff" );
                
                _isMenuOn = false;
            }
        }
    }
}
