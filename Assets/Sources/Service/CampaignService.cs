﻿using Models;
using System.Collections.Generic;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class CampaignService : AFeatureService
    {
        private int _campaignCount = 10;
        private IList<string> _imageNames = null;
        private IList<string> _backgroundNames = null;
        private Dictionary<uint, CampaignModel> _campaigns = new Dictionary<uint, CampaignModel>();

        private List<string> _defaultTitle = new List<string>() { "Umbra", "Allies", "Underworld", "Close Quarters", "Back to zoulouland", "Coconuts" };
        private List<string> _defaultDescription = new List<string>() { "Earn special rewards",
                                                                        "Only heroes can fight in",
                                                                        "Only villains can fight in",
                                                                        "Only users of SMG / Pistol weapons can fight in",
                                                                        "Only zoulou can fight in",
                                                                        "Only owner of coconuts can fight in" };

        public int campaignCount
        {
            get
            {
                return _campaignCount;
            }
        }

        public IEnumerable<CampaignModel> campaigns
        {
            get
            {
                return _campaigns.Values;
            }
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="CampaignService"/> class.
        /// </summary>
        public CampaignService()
        {

        }
        
        public void Initialize(int campaignCount, IEnumerable<string> imageNames, IEnumerable<string> backgroundNames)
        {
            _imageNames = new List<string>( imageNames );
            _backgroundNames = new List<string>( backgroundNames );

            _campaignCount = campaignCount;

            int titleIndex = 0;
            int imageIndex = 0;
            int backgroundIndex = 0;
            int imageCount = _imageNames.Count;
            int backgroundCount = _backgroundNames.Count;
            for (uint curr = 0; curr < _campaignCount; curr++)
            {
                int randomIndex = UnityEngine.Random.Range( 0, 5 );

                string campaignTitle = _defaultTitle[ titleIndex ];
                string campaignDescription = _defaultDescription[ titleIndex ];
                
                _campaigns.Add( curr, new CampaignModel( curr, 
                                                         campaignTitle, 
                                                         campaignDescription, 
                                                         _imageNames[ imageIndex ],
                                                         _backgroundNames[ backgroundIndex ] ) );

                imageIndex++;
                titleIndex++;
                backgroundIndex++;

                if ( imageIndex >= imageCount )
                {
                    imageIndex = 0;
                }

                if ( titleIndex >= 6 )
                {
                    titleIndex = 0;
                }

                if ( backgroundIndex >= backgroundCount )
                {
                    backgroundIndex = 0;
                }
            }
        }
    }
}
