﻿using Enums;
using Models;
using System.Collections.Generic;
using Utils;

namespace FeatureModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class HubModel : AFeatureModel
    {
        private ActivityModel _currentActivity;
        private ActivityStep  _currentStep;
        private Dictionary<ActivityEnum, ActivityModel> _modelByActivity = new Dictionary<ActivityEnum, ActivityModel>();
        
        public ActivityModel currentActivity
        {
            get
            {
                return _currentActivity;
            }
            set
            {
                NotifyPropertyChanged( ref _currentActivity, value );
            }
        }

        public ActivityStep currentStep
        {
            get
            {
                return _currentStep;
            }
            set
            {
                NotifyPropertyChanged( ref _currentStep, value );
            }
        }
        
        public uint activityCount
        {
            get
            {
                return (uint)_modelByActivity.Count;
            }
        }

        public IEnumerable<ActivityModel> activities
        {
            get
            {
                return _modelByActivity.Values;
            }
        }

        public HubModel(params ActivityModel[] activities)
        {
            _identifier = Constants.HUB_FEATURE;

            for (int curr = 0; curr < activities.Length; curr++)
            {
                AddActivity( activities[ curr ] );
            }
        }

        public void AddActivity(ActivityModel activity)
        {
            if ( activity == null )
            {
                return;
            }

            _modelByActivity[ activity.activity ] = activity;
        }
        
        public ActivityModel GetActivity(ActivityEnum activity)
        {
            ActivityModel activityModel = null;
            if ( _modelByActivity.TryGetValue( activity, out activityModel ) )
            {
                return activityModel;
            }

            return null;
        }
    }
}
