﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The event state.
    /// </summary>
    public class EventHubState : AHubState
    {
        public EventHubState()
        {
            _stateName = "Event hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);


        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);


        }

        public override void OnExit()
        {
            base.OnExit();


        }
    }
}
