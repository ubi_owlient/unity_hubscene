﻿using Commands.HubMenu;
using Extensions;
using Menu.Buttons;
using System;
using UnityEngine;
using Views;
using ViewModels;
using FeatureModels;
using System.ComponentModel;

namespace Menu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HubSubMenuManager : AView
    {
        [SerializeField]
        private GameObject _contentParent = null;

        [SerializeField]
        private GameObject _contentPrefab = null;
        
        private Animator _animator = null;
        private bool     _isMenuOn = false;

        private SubMenuHeaderView _subMenuHeader = null;

        private ActivityMenuViewModel _viewModel = null;

        public override IViewModel model
        {
            get
            {
                return _viewModel;
            }
        }

        public event Action<GameObject>                           initialized   = null;
        public event Action<HubSubMenuManager, SubMenuHeaderView> onBackClicked = null;
        public event Action<HubSubMenuManager, SubMenuHeaderView> onHomeClicked = null;

        void Start()
        {
            _viewModel = new ActivityMenuViewModel( GetModel<HubModel>() );
            _viewModel.PropertyChanged += OnPropertyChanged;

            _animator = GetComponent<Animator>();

            _subMenuHeader = this.GetComponentInChildren<SubMenuHeaderView>();
            if ( _subMenuHeader != null )
            {
                _subMenuHeader.onBackClicked += OnBackClicked;
                _subMenuHeader.onHomeClicked += OnHomeClicked;
            }

            if ( _contentPrefab != null )
            {
                Instantiate( _contentPrefab, _contentParent.transform );

                NotifyInitialized();
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
        {
            switch (eventArgs.PropertyName)
            {
                case "isVisible":
                    {
                        if ( _viewModel.isVisible )
                        {
                            gameObject.SetActive( true );
                        }
                        else
                        {
                            gameObject.SetActive( false );
                        }
                    }
                    break;
                case "step":
                    {
                        if ( _viewModel.step != null )
                        {
                            SetHeaderInfo( _viewModel.step.title, _viewModel.step.description );
                            SetContent( _viewModel.step.activityMenuPrefab );
                        }
                    }
                    break;
                case "isOpen":
                    {
                        if ( _viewModel.isOpen )
                        {
                            OpenMenu();
                        }
                        else
                        {
                            CloseMenu();
                        }
                    }
                    break;
            }
        }

        void OnDestroy()
        {
            if ( _viewModel != null )
            {
                _viewModel.PropertyChanged -= OnPropertyChanged;
            }

            if ( _subMenuHeader != null )
            {
                _subMenuHeader.onBackClicked -= OnBackClicked;
                _subMenuHeader.onHomeClicked -= OnHomeClicked;
            }
        }

        private void SetHeaderInfo(string title, string description)
        {
            if ( _subMenuHeader != null )
            {
                _subMenuHeader.SetMenuInfo( title, description );
            }
        }

        private void SetContent(string contentPrefabPath)
        {
            _contentPrefab = Resources.Load( contentPrefabPath ) as GameObject;
            
            if ( _contentPrefab != null )
            {
                _contentParent.transform.ClearChildren();

                Instantiate( _contentPrefab, _contentParent.transform );

                NotifyInitialized();
            }
            else
            {
                Debug.LogError( string.Format( "Cannot instanciate the missing resource {0}", contentPrefabPath ) );
            }
        }

        private void OnBackClicked(SubMenuHeaderView sender, ABaseButton button)
        {
            Dispatch( new BackMenuCommand( this ) );

            if ( onBackClicked != null )
            {
                onBackClicked( this, sender );
            }
        }

        private void OnHomeClicked(SubMenuHeaderView sender, ABaseButton button)
        {
            Dispatch( new HomeMenuCommand( this ) );

            if ( onHomeClicked != null )
            {
                onHomeClicked( this, sender );
            }
        }

        /// <summary>
        /// Open menu method.
        /// </summary>
        private void OpenMenu()
        {
            if ( _isMenuOn == false )
            {
                _animator.SetTrigger( "OpenMenu" );
                _isMenuOn = true;
            }
        }

        /// <summary>
        /// Close menu method.
        /// </summary>
        private void CloseMenu()
        {
            if ( _isMenuOn )
            {
                _animator.SetTrigger( "CloseMenu" );
                _isMenuOn = false;
            }
        }

        private void NotifyInitialized()
        {
            if ( initialized != null )
            {
                initialized( _contentParent );
            }
        }
    }
}
