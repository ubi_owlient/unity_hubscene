using Service;
using UnityEngine;

namespace Animations
{
    public class ActivateTrigger : Receiver
    {
        public enum Mode
        {
            Trigger = 0, // Just broadcast the action on to the target
            Replace = 1, // replace target with source
            Activate = 2, // Activate the target GameObject
            Enable = 3, // Enable a component
            Animate = 4, // Start animation on target
            Deactivate = 5, // Decativate target GameObject
            ScriptDisable = 6, // Decativate target Script 
            SendEvent = 7, // Send Event To Communicator 
            ScriptEnable = 8 // Activate target Script
        }

        /// The action to accomplish
        public Mode action = Mode.Activate;

        /// The game object to affect. If none, the trigger work on this game object
        public Object target;
        public GameObject source;
        public string inEventStart;
        public string inEventStop;
        public string actionParam;
        public string[] actionParams;
        public int triggerCount = 1;///
        public bool repeatTrigger = false;
        public bool doOnEnter = true;
        public GameObject[] checkOnly;
        public string[] checkOnlyName;

        bool isActivated = false;
        
        void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            inEventStart = animService.PrepareEventName(inEventStart, gameObject);
            inEventStop  = animService.PrepareEventName(inEventStop, gameObject);

            animService.Register(inEventStart, this);
            animService.Register(inEventStop, this);
            if (inEventStart.Length == 0)
            {
                isActivated = true;
            }

            if ((checkOnlyName != null) && (checkOnlyName.Length > 0))
            {
                checkOnly = new GameObject[checkOnlyName.Length];
                for (int i = 0; i < checkOnlyName.Length; ++i)
                {
                    checkOnly[i] = GameObject.Find(checkOnlyName[i]);
                }
            }
        }
        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            animService.Unregister(inEventStart, this);
            animService.Unregister(inEventStop, this);
        }
        public override void Receive(string eventName, GameObject obj)
        {
            if (eventName.Equals(inEventStart))
            {
                isActivated = true;
            }
            if (eventName.Equals(inEventStop))
            {
                isActivated = false;
            }
        }
        public void DoActivateTrigger()
        {
            triggerCount--;

            if (triggerCount == 0 || repeatTrigger)
            {
                Object currentTarget = target != null ? target : gameObject;
                Behaviour targetBehaviour = currentTarget as Behaviour;
                GameObject targetGameObject = currentTarget as GameObject;
                if (targetBehaviour != null)
                    targetGameObject = targetBehaviour.gameObject;

                switch (action)
                {
                    case Mode.Trigger:
                        targetGameObject.BroadcastMessage("DoActivateTrigger");
                        break;
                    case Mode.Replace:
                        if (source != null)
                        {
                            Object.Instantiate(source, targetGameObject.transform.position, targetGameObject.transform.rotation);
                            Object.Destroy(targetGameObject);
                        }
                        break;
                    case Mode.Activate:
                        targetGameObject.SetActive(true);
                        break;
                    case Mode.Enable:
                        if (targetBehaviour != null)
                            targetBehaviour.enabled = true;
                        break;
                    case Mode.Animate:
                        if (actionParams.Length > 0)
                        {
                            for (int i = 0; i < actionParams.Length; ++i)
                                targetGameObject.GetComponent<Animation>().Play(actionParams[i]);
                        }
                        else
                            targetGameObject.GetComponent<Animation>().Play();
                        break;
                    case Mode.Deactivate:
                        targetGameObject.SetActive(false);
                        break;
                    case Mode.ScriptDisable:
                        {
                            MonoBehaviour[] scriptComponents = targetGameObject.GetComponents<MonoBehaviour>();
                            foreach (MonoBehaviour script in scriptComponents)
                            {
                                script.enabled = false;

                            }
                        }
                        break;
                    case Mode.ScriptEnable:
                        {
                            MonoBehaviour[] scriptComponents = targetGameObject.GetComponents<MonoBehaviour>();
                            foreach (MonoBehaviour script in scriptComponents)
                            {
                                script.enabled = true;

                            }
                        }
                        break;
                    case Mode.SendEvent:
                        ServiceManager.Instance.GetService<AnimationService>().SendEvent(actionParams, gameObject);
                        break;
                }
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (isActivated && doOnEnter)
            {
                bool isOk = false;
                for (int i = 0; i < checkOnly.Length; ++i)
                {
                    if (checkOnly[i] == other.gameObject)
                        isOk = true;
                }
                if ((checkOnly.Length == 0) || isOk)
                {
                    DoActivateTrigger();
                }
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (isActivated && !doOnEnter)
            {
                bool isOk = false;
                for (int i = 0; i < checkOnly.Length; ++i)
                {
                    if (checkOnly[i] == other.gameObject)
                        isOk = true;
                }
                if ((checkOnly.Length == 0) || isOk)
                    DoActivateTrigger();
            }
        }
        void OnTriggerEnter2D(Collider2D other)
        {
            if (isActivated && doOnEnter)
            {
                bool isOk = false;
                for (int i = 0; i < checkOnly.Length; ++i)
                {
                    if (checkOnly[i] == other.gameObject)
                        isOk = true;
                }
                if ((checkOnly.Length == 0) || isOk)
                {
                    DoActivateTrigger();
                }
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (isActivated && !doOnEnter)
            {
                bool isOk = false;
                for (int i = 0; i < checkOnly.Length; ++i)
                {
                    if (checkOnly[i] == other.gameObject)
                        isOk = true;
                }

                if ((checkOnly.Length == 0) || isOk)
                {
                    DoActivateTrigger();
                }
            }
        }

    }
}