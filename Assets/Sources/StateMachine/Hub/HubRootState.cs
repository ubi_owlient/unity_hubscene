﻿using Commands;
using Commands.HubMenu;
using Commands.Settings;
using Enums;
using FeatureModels;
using Models;
using Service;

namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The main hub root state.
    /// </summary>
    public class HubRootState : ABaseState
    {
        private ActivityModel _current = null;

        private bool _enterSettings = false;

        public HubRootState()
        {
            _stateName = "Hub root";
        }

        public override void OnEnter()
        {
            base.OnEnter();
            

        }

        [DispatcherService.HandleCommand( commandType = typeof(SelectHubMenuCommand) )]
        private void OnActivityChanged(ICommand command)
        {
            SelectHubMenuCommand selectHubCommand = command as SelectHubMenuCommand;

            HubModel hubModel = GetModel<HubModel>();

            _current = hubModel.GetActivity( selectHubCommand.activity );
            
            if ( _current != null )
            {
                _current.hubPosition = selectHubCommand.hubPosition;
                _current.hubLookAt   = selectHubCommand.hubLookAt;
            }

        }

        [DispatcherService.HandleCommand(commandType = typeof(OpenSettingCommand))]
        private void OnSettingOpened(ICommand command)
        {
            Owner.GetService<PauseService>().Pause();

            _enterSettings = true;
        }
        
        public override void OnExit()
        {
            base.OnExit();
            
            _enterSettings = false;
        }
        
        public override Transition GetTransition()
        {
            if ( _enterSettings )
            {
                return Transition.Sibling<SettingsRootState>();
            }
            else if ( _current != null )
            {
                Transition transition = Transition.None();
                switch ( _current.activity )
                {
                    case ActivityEnum.CAMPAIGN:
                        transition = Transition.Inner<CampaignHubState>( _current );
                        break;
                    case ActivityEnum.STORE:
                        transition = Transition.Inner<StoreHubState>( _current );
                        break;
                    case ActivityEnum.GACHA:
                        transition = Transition.Inner<GachaStoreHubState>( _current );
                        break;
                    case ActivityEnum.EVENTS:
                        transition = Transition.Inner<EventHubState>( _current );
                        break;
                    case ActivityEnum.DAILY_OPS:
                        transition = Transition.Inner<DailyOpsHubState>( _current );
                        break;
                    case ActivityEnum.GUILDS:
                        transition = Transition.Inner<GuildHubState>( _current );
                        break;
                    case ActivityEnum.ARENA:
                        transition = Transition.Inner<ArenaHubState>( _current );
                        break;
                    case ActivityEnum.SOLDIERS:
                        transition = Transition.Inner<SoldierHubState>( _current );
                        break;
                }

                _current = null;

                return transition;
            }
            
            return Transition.None();
        }
    }
}
