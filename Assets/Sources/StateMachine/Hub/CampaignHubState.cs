﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The campaign state.
    /// </summary>
    public class CampaignHubState : AHubState
    {
        public CampaignHubState()
        {
            _stateName = "Campaign hub";
        }

        public override Transition GetTransition()
        {
            if ( _step != null )
            {
                return Transition.Sibling<CampaignChoiceState>( _activity, _step );
            }

            return base.GetTransition();
        }
    }
}
