﻿using System.Collections.Generic;
using Types;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class CampaignModel
    {
        private bool   _isLocked            = false;
        private uint   _campaignId          = 0;
        private string _campaignTitle       = null;
        private string _campaignDescription = null;
        private string _campaignImage       = null;
        private string _campaignBackground  = null;
        private Dictionary<uint, MissionModel> _missionsById = new Dictionary<uint, MissionModel>();

        public uint campaignId
        {
            get
            {
                return _campaignId;
            }
        }

        public string campaignTitle
        {
            get
            {
                return _campaignTitle;
            }
        }

        public string campaignDescription
        {
            get
            {
                return _campaignDescription;
            }
        }

        public string campaignImage
        {
            get
            {
                return _campaignImage;
            }
        }

        public string campaignBackground
        {
            get
            {
                return _campaignBackground;
            }
        }

        public int missionCount
        {
            get
            {
                return _missionsById.Count;
            }
        }

        public IEnumerable<MissionModel> missions
        {
            get
            {
                return _missionsById.Values;
            }
        }

        public bool isLocked
        {
            get
            {
                return _isLocked;
            }
        }

        public CampaignModel(uint campaignId, string campaignTitle, string campaignDesc, string campaignImage, string campaignBackground)
        {
            _campaignId = campaignId;
            _campaignTitle = campaignTitle;
            _campaignDescription = campaignDesc;
            _campaignImage = campaignImage;
            _campaignBackground = campaignBackground;
        }

        public void Lock()
        {
            _isLocked = true;
        }

        public void Unlock()
        {
            _isLocked = false;
        }

        public void AddMission(MissionModel mission)
        {
            _missionsById.Add(mission.missionId, mission);
        }

        public void RemoveMission(MissionModel mission)
        {
            RemoveMission( mission.missionId );
        }

        public void RemoveMission(uint missionId)
        {
            _missionsById.Remove(missionId);
        }
    }
}
