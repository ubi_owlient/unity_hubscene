﻿using Service;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Animations
{
    public class ChangeLevel : Receiver
    {
        #region STATIC
        static ArrayList lastLevels = null;

        static public void __GoTo(string levelName)
        {
            if (lastLevels == null)
            {
                lastLevels = new ArrayList();
            }
            bool isOld = false;
            for (int i = lastLevels.Count - 1; i >= 0; --i)
            {
                if (lastLevels[i].Equals(levelName))
                {
                    isOld = true;
                    lastLevels.RemoveRange(i, lastLevels.Count - i);
                    break;
                }
            }
            if (!isOld)
            {
                lastLevels.Add(SceneManager.GetActiveScene().name);
            }
            SceneManager.LoadScene(levelName);
        }

        static public void GoTo(string levelName)
        {
            __GoTo(levelName);
        }
        #endregion


        public string level;
        public string inChangeLevel;

        void Start()
        {
            if (lastLevels == null)
            {
                lastLevels = new ArrayList();
            }
            string curLevel = SceneManager.GetActiveScene().name;

            for (int i = lastLevels.Count - 1; i >= 0; --i)
            {
                if (lastLevels[i].Equals(curLevel))
                {
                    lastLevels.RemoveRange(i, lastLevels.Count - i);
                    break;
                }
            }

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            inChangeLevel = animService.PrepareEventName(inChangeLevel, gameObject);

            animService.Register(inChangeLevel, this);
        }

        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            animService.Unregister(inChangeLevel, this);
        }

        public void _GoTo(string levelName)
        {
            GoTo(levelName);
        }

        public override void Receive(string eventName, GameObject obj)
        {
            if (eventName.Equals(inChangeLevel))
            {
                GoTo(level);
            }
        }
    }
}