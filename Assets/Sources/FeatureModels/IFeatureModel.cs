﻿using System;
using System.ComponentModel;

namespace FeatureModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IFeatureModel : INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Gets the feature model identifier.
        /// </summary>
        string id
        {
            get;
        }
    }
}
