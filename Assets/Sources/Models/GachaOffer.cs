﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class GachaOffer : AOffer
    {
        public GachaOffer(uint offerId, string offerTitle, string offerDesc, string offerImage, string offerBackground, Price price, int frequency = -1) :
        base( offerId, offerTitle, offerDesc, offerImage, offerBackground, price, frequency )
        {

        }
    }
}
