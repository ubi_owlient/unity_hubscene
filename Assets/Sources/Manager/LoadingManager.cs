﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class LoadingManager : MonoBehaviour
    {
        private bool _isDisplayingWaitingScreen = false;

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<LoadingService>().Initialize();
        }

        void Update()
        {
            if ( Input.GetKeyDown( KeyCode.W ) )
            {
                if ( _isDisplayingWaitingScreen )
                {
                    ServiceManager.Instance.GetService<LoadingService>().HideWaitingScreen();
                    _isDisplayingWaitingScreen = false;
                }
                else
                {
                    ServiceManager.Instance.GetService<LoadingService>().ShowWaitingScreen();
                    _isDisplayingWaitingScreen = true;
                }
            }
        }
    }
}
