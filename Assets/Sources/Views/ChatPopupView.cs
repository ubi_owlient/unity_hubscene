﻿using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ChatPopupView : ASettingsPopupView
    {
        public override string id
        {
            get
            {
                return Constants.CHAT_POPUP_ID;
            }
        }
    }
}
