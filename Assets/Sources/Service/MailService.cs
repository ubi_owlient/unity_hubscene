﻿using Enums;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MailService : AFeatureService
    {
        private const uint _testMailCount = 10;

        private Dictionary<uint, MessageModel> _mails = new Dictionary<uint, MessageModel>();

        private List<string> _defaultSenders  = new List<string>() { "System", "Ubisoft", "Julien Larbi", "Bob The Sponge", "Developers", "Sam Fisher" };
        private List<string> _defaultObjects  = new List<string>() { "How to use materials", "You can play with friends", "So good", "New friend request", "Gift to start well", "Let's rock" };
        private List<string> _defaultMessages = new List<string>() { "The materials are used to upgrade your weapon and craft new ones so your soldiers become nightmare on the battlefield. Your ennemies will flee then.",
                                                                     "By creating a guild, you can play with friends and team up so you defeat harder ennemies together and share some loot.",
                                                                     "Did you see how I killed GU1? In fell down in 2 secs. Such a looser haha",
                                                                     "Hello, i would like to become one of your friends. Could you add me so we team up and play together?",
                                                                     "Hello commander, so you start your duty and be able to defeat your ennemies, we offer you this starter pack that allow you to upgrade some of your players. Use it well.",
                                                                     "New arena season opened. Time to fight against UMBRA's elite. Let's go." };
        private List<MessagePriorityEnum> _defaultPriorities = new List<MessagePriorityEnum>() { MessagePriorityEnum.NORMAL,
                                                                                                 MessagePriorityEnum.LOW,
                                                                                                 MessagePriorityEnum.MINIMAL,
                                                                                                 MessagePriorityEnum.HIGH,
                                                                                                 MessagePriorityEnum.MAXIMAL,
                                                                                                 MessagePriorityEnum.MAXIMAL };
        private List<List<RewardModel>> _defaultRewards = new List<List<RewardModel>>() {
                                                                                            new List<RewardModel>()
                                                                                            {
                                                                                                new RewardModel( 0, "Silicon", "Silicon", 35 ),
                                                                                                new RewardModel( 1, "Adhesive", "Adhesive", 35 ),
                                                                                            },
                                                                                            null,
                                                                                            null,
                                                                                            null,
                                                                                            new List<RewardModel>()
                                                                                            {
                                                                                                new RewardModel( 0, "Silicon", "Silicon", 150 ),
                                                                                                new RewardModel( 1, "Adhesive", "Adhesive", 200 ),
                                                                                                new RewardModel( 2, "Gold", "GoldReward", 500 ),
                                                                                            },
                                                                                            null,
                                                                                        };

        public Action<MailService, MessageModel> mailReceived = null;

        public int mailCount
        {
            get
            {
                return _mails.Count;
            }
        }

        public IEnumerable<MessageModel> mails
        {
            get
            {
                return _mails.Values;
            }
        }

        public IEnumerable<MessageModel> readMails
        {
            get
            {
                return _mails.Values.Where( mail => mail.isRead );
            }
        }

        public IEnumerable<MessageModel> unreadMails
        {
            get
            {
                return _mails.Values.Where( mail => mail.isRead == false );
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MailService"/> class.
        /// </summary>
        public MailService()
        {

        }

        public void Initialize()
        {
            for (uint curr = 0; curr < _testMailCount; curr++)
            {
                int index = (int)(curr % 6);
                string sender  = _defaultSenders[ index ];
                string title   = _defaultObjects[ index ];
                string message = _defaultMessages[ index ];
                MessagePriorityEnum priority = _defaultPriorities[ index ];
                IEnumerable<RewardModel> rewards = _defaultRewards[ index ];

                _mails.Add( curr, new MessageModel( curr, sender, title, message, priority, rewards ) );
            }
        }

        public void CreateMail(string sender, string title, string message, MessagePriorityEnum priority, IEnumerable<RewardModel> rewards = null)
        {
            MessageModel newMessage = new MessageModel( (uint)_mails.Count, sender, title, message, priority, rewards );
            _mails.Add( (uint)_mails.Count, newMessage );

            NotifyMailReceived( newMessage );
        }

        private void NotifyMailReceived(MessageModel message)
        {
            if ( mailReceived != null )
            {
                mailReceived( this, message );
            }
        }
    }
}
