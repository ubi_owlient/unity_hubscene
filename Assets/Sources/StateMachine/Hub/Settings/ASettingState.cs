﻿using ViewModels;
using Views;

namespace FSM.Hub.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ASettingState : ABaseState
    {
        protected IPopupViewModel _popup = null;

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            _popup = args[ 0 ] as IPopupViewModel;
            if ( _popup != null )
            {
                _popup.Show();
            }
        }
        
        public override void OnExit()
        {
            base.OnExit();

            if ( _popup != null )
            {
                _popup.Hide();
            }
        }
    }
}
