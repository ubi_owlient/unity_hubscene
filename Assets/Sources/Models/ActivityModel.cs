﻿using Enums;
using UnityEngine;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class ActivityModel
    {
        private uint   _activityId = 0;
        private string _activityTitle;
        private string _activityDescription;
        private ActivityStep[] _steps = null;
        private ActivityEnum _activity = ActivityEnum.NONE;
        
        public uint activityId
        {
            get
            {
                return _activityId;
            }
        }

        public ActivityEnum activity
        {
            get
            {
                return _activity;
            }
        }
        
        public string title
        {
            get
            {
                return _activityTitle;
            }
        }

        public string description
        {
            get
            {
                return _activityDescription;
            }
        }

        public Vector3 hubPosition
        {
            get;
            set;
        }

        public Vector3 hubLookAt
        {
            get;
            set;
        }

        public ActivityStep[] steps
        {
            get
            {
                return _steps;
            }
        }

        public ActivityModel(uint activityId, ActivityEnum activity, string activityTitle, string activityDescription, params ActivityStep[] steps)
        {
            _activityId = activityId;
            _activity   = activity;
            _activityTitle = activityTitle;
            _activityDescription = activityDescription;
            _steps      = steps;
            if ( _steps != null )
            {
                for (int curr = 0; curr < _steps.Length; curr++)
                {
                    _steps[curr].owner = this;
                }
            }
        }
    }
}
