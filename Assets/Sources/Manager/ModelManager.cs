﻿using Enums;
using FeatureModels;
using Models;
using Service;
using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ModelManager : MonoBehaviour
    {
        // TO DO: modify to load from config files....

        private Dictionary<ActivityEnum, string> _titleByActivity = new Dictionary<ActivityEnum, string>()
        {
            { ActivityEnum.CAMPAIGN, "Campaign" },
            { ActivityEnum.SOLDIERS, "Soldiers" },
            { ActivityEnum.STORE, "Store" },
            { ActivityEnum.GACHA, "Gacha" },
            { ActivityEnum.DAILY_OPS, "DailyOps" },
            { ActivityEnum.EVENTS, "Events" },
            { ActivityEnum.ARENA, "Arena" },
            { ActivityEnum.GUILDS, "Guild" },
        };

        private Dictionary<ActivityEnum, string> _descriptionByActivity = new Dictionary<ActivityEnum, string>()
        {
            { ActivityEnum.CAMPAIGN, "Click to enter" },
            { ActivityEnum.SOLDIERS, "Click to enter" },
            { ActivityEnum.STORE, "Click to enter" },
            { ActivityEnum.GACHA, "Click to enter" },
            { ActivityEnum.DAILY_OPS, "Click to enter" },
            { ActivityEnum.EVENTS, "Click to enter" },
            { ActivityEnum.ARENA, "Click to enter" },
            { ActivityEnum.GUILDS, "Click to enter" },
        };

        private Dictionary<ActivityEnum, ActivityStep[]> _stepsByActivity = new Dictionary<ActivityEnum, ActivityStep[]>()
        {
            { ActivityEnum.CAMPAIGN,  new ActivityStep[ 3 ] 
            {
                new ActivityStep( "", "", "CampaignChoiceContent"),
                new ActivityStep( "", "", "MissionChoiceContent"),
                new ActivityStep( "", "", "TeamChoiceContent")
            } },
            { ActivityEnum.SOLDIERS,  new ActivityStep[ 1 ] 
            {
                new ActivityStep( "", "", "SoldierSheetContent")
            } },
            { ActivityEnum.STORE,     new ActivityStep[ 3 ] 
            {
                new ActivityStep( "", "", "OfferChoiceContent"),
                new ActivityStep( "", "", "OfferInfoContent"),
                new ActivityStep( "", "", "OfferPurchaseContent"),
            } },
            { ActivityEnum.GACHA,     new ActivityStep[ 3 ] 
            {
                new ActivityStep( "", "", "GachaChoiceContent"),
                new ActivityStep( "", "", "GachaInfoContent"),
                new ActivityStep( "", "", "GachaPurchaseContent"),
            } },
            { ActivityEnum.DAILY_OPS, new ActivityStep[ 3 ] 
            {
                new ActivityStep( "", "", "OpsChoiceContent"),
                new ActivityStep( "", "", "MissionChoiceContent"),
                new ActivityStep( "", "", "TeamChoiceContent"),
            } },
            { ActivityEnum.EVENTS,    new ActivityStep[ 3 ] 
            {
                new ActivityStep( "", "", "EventChoiceContent"),
                new ActivityStep( "", "", "MissionChoiceContent"),
                new ActivityStep( "", "", "TeamChoiceContent"),
            } },
            { ActivityEnum.ARENA,     new ActivityStep[ 3 ] 
            {
                new ActivityStep( "", "", "ArenaHubContent"),
                new ActivityStep( "", "", "TeamChoiceContent"),
                new ActivityStep( "", "", "PvpChoiceContent"),
            } },
            { ActivityEnum.GUILDS,    new ActivityStep[ 4 ] 
            {
                new ActivityStep( "", "", "GuildChoiceContent"),
                new ActivityStep( "", "", "GuildHubContent"),
                new ActivityStep( "", "", "EnlistmentChoiceContent"),
                new ActivityStep( "", "", "WarChoiceContent"),
            } },
        };

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ModelService modelService = ServiceManager.Instance.GetService<ModelService>();

            uint counter = 0;
            Array enumValues = typeof(ActivityEnum).GetEnumValues();
            ActivityModel[] activities = new ActivityModel[ enumValues.Length - 1 ];
            foreach ( var value in enumValues )
            {
                ActivityEnum cast = (ActivityEnum)value;
                if ( cast == ActivityEnum.NONE )
                {
                    continue;
                }

                activities[ counter ] = new ActivityModel( counter, 
                                                           cast, 
                                                           _titleByActivity[ cast ], 
                                                           _descriptionByActivity[ cast ],
                                                           _stepsByActivity[ cast ] );
                counter++;
            }

            modelService.RegisterModel( new HubModel( activities ) );

            SettingOptionModel[] settings = new SettingOptionModel[ 5 ]
            {
                new SettingOptionModel( Constants.OPTIONS_POPUP_ID ),
                new SettingOptionModel( Constants.MAILS_POPUP_ID ),
                new SettingOptionModel( Constants.CHAT_POPUP_ID ),
                new SettingOptionModel( Constants.CALENDAR_POPUP_ID ),
                new SettingOptionModel( Constants.MOTD_POPUP_ID ),
            };

            modelService.RegisterModel( new SettingsModel( settings ) );
            modelService.RegisterModel( new StoreModel() );
            modelService.RegisterModel( new GachaModel() );
        }
    }
}
