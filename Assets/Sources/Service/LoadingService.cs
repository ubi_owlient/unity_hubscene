﻿using System;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class LoadingService : AFeatureService
    {
        public event Action<double, Action> fadingScreenRequired = null;

        public event Action showLoadingScreenRequired = null;
        public event Action<uint> updateLoadingScreenRequired = null;
        public event Action hideLoadingScreenRequired = null;

        public event Action showWaitingScreenRequired = null;
        public event Action hideWaitingScreenRequired = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadingService"/> class.
        /// </summary>
        public LoadingService()
        {

        }

        public void Initialize()
        {

        }

        public void ShowLoadingScreen()
        {
            if ( showLoadingScreenRequired != null )
            {
                showLoadingScreenRequired();
            }
        }

        public void UpdateLoadingScreen(uint percentage)
        {
            if ( updateLoadingScreenRequired != null )
            {
                updateLoadingScreenRequired( percentage );
            }
        }

        public void HideLoadingScreen()
        {
            if ( hideLoadingScreenRequired != null )
            {
                hideLoadingScreenRequired();
            }
        }

        public void ShowWaitingScreen()
        {
            if ( showWaitingScreenRequired != null )
            {
                showWaitingScreenRequired();
            }
        }

        public void HideWaitingScreen()
        {
            if ( hideWaitingScreenRequired != null )
            {
                hideWaitingScreenRequired();
            }
        }

        public void TriggerFadingScreen(double time, Action toDoOnLoading = null)
        {
            if ( fadingScreenRequired != null )
            {
                fadingScreenRequired( time, toDoOnLoading );
            }
        }
    }
}
