﻿using Characters;
using Service;
using UnityEngine;

namespace Hub
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class HubPlayerPlaceholder : MonoBehaviour
    {
        [SerializeField]
        private Transform _playerPosition = null;

        [SerializeField]
        private Transform _playerLookAt = null;
        
        private PointAndClickPlayerController _controller = null;

        public Vector3 playerPosition
        {
            get
            {
                return _playerPosition.position;
            }
        }

        public Vector3 playerLookAt
        {
            get
            {
                return _playerLookAt.position;
            }
        }

        public void MoveToPlace()
        {
            if ( _controller == null )
            {
                _controller = ServiceManager.Instance.GetService<PlayerService>().controller;
            }

            _controller.Move( playerPosition, playerLookAt );
        }
    }
}
