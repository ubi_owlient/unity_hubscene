﻿using Service;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class OptionsSettingView : MonoBehaviour
    {
        [SerializeField]
        private Toggle _enableAnim = null;

        private void Start()
        {
            if ( _enableAnim != null )
            {
                _enableAnim.onValueChanged.AddListener( OnEnableAnim );
            }

            _enableAnim.isOn = ServiceManager.Instance.GetService<SettingService>().AnimationEnabled;
        }

        private void OnDestroy()
        {
            if ( _enableAnim != null )
            {
                _enableAnim.onValueChanged.RemoveListener( OnEnableAnim );
            }
        }

        private void OnEnableAnim(bool enable)
        {
            ServiceManager.Instance.GetService<SettingService>().SetAnimationState( enable );
        }
    }
}
