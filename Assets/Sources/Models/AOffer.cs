﻿using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class AOffer
    {
        private uint   _offerId          = 0;
        private string _offerTitle       = null;
        private string _offerDescription = null;
        private string _offerImage       = null;
        private string _offerBackground  = null;
        private Price  _price            = null;
        private int    _frequency        = -1;
        private uint   _used             = 0;
        private Dictionary<uint, RewardModel> _rewardById = new Dictionary<uint, RewardModel>();

        public uint offerId
        {
            get
            {
                return _offerId;
            }
        }

        public string offerTitle
        {
            get
            {
                return _offerTitle;
            }
        }

        public string offerDescription
        {
            get
            {
                return _offerDescription;
            }
        }

        public string offerImage
        {
            get
            {
                return _offerImage;
            }
        }

        public string offerBackground
        {
            get
            {
                return _offerBackground;
            }
        }
        
        public Price price
        {
            get
            {
                return _price;
            }
        }

        public bool isFree
        {
            get
            {
                return _price.amount <= 0;
            }
        }

        public int frequency
        {
            get
            {
                return _frequency;
            }
        }

        public uint used
        {
            get
            {
                return _used;
            }
        }

        public bool isLocked
        {
            get
            {
                return canBuy == false;
            }
        }

        public bool canBuy
        {
            get
            {
                return _used < _frequency;
            }
        }

        public IEnumerable<RewardModel> rewards
        {
            get
            {
                return _rewardById.Values;
            }
        }

        protected AOffer(uint offerId, string offerTitle, string offerDesc, string offerImage, string offerBackground, Price price, int frequency = -1)
        {
            _offerId = offerId;
            _offerTitle = offerTitle;
            _offerDescription = offerDesc;
            _offerImage = offerImage;
            _offerBackground = offerBackground;
            _price = price;
            _frequency = frequency;
        }

        public void AddReward(RewardModel reward)
        {
            _rewardById.Add( reward.id, reward );
        }

        public void RemoveReward(RewardModel reward)
        {
            RemoveReward( reward.id );
        }

        public void RemoveReward(uint rewardId)
        {
            _rewardById.Remove( rewardId );
        }
    }
}
