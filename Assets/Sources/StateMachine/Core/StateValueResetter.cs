﻿namespace FSM
{
    /// <summary>
    /// Definition of the <see cref="StateValueResetter{T}"/> class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class StateValueResetter<T> : AStateValueResetter
    {
        private StateValue<T> _stateValue;
        private T _originalValue;

        public StateValue<T> StateValue { get { return _stateValue; } }

        public StateValueResetter(StateValue<T> stateValue)
        {
            _stateValue = stateValue;
            _originalValue = stateValue.valueToBeAccessedByStateMachineOnly;
        }

        public override void Reset()
        {
            _stateValue.valueToBeAccessedByStateMachineOnly = _originalValue;
            _stateValue = null; //@TODO: Add Dispose (or Finalize) that asserts that this is null (that Reset got called)
        }
    }

    internal abstract class AStateValueResetter
    {

        public virtual void Reset() { }
    }
}
