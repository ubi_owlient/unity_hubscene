﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace FSM
{
    /// <summary>
    /// Definition of the <see cref="StateMachine"/> class.
    /// </summary>
    public class StateMachine
    {
        private List<AState> _stateStack = new List<AState>();
        private object _owner = null;
        private TraceLevel _traceLevel = TraceLevel.None;
        private Type _initialStateType;

        public void Init<InitialStateType>(object owner = null) where InitialStateType : AState
        {
            Init(typeof(InitialStateType), owner);
        }

        public void Init(Type initialStateType, object owner = null)
        {
            _owner = owner;
            _initialStateType = initialStateType;
        }

        //@TODO: Add Dipose/Finalize that calls this
        public void Shutdown()
        {
            Stop();
        }

        // Stopping the state machine means popping the state stack so that all OnExits get called. Note that
        // calling Update afterwards will start up the state machine again (starting with the initial state).
        public void Stop()
        {
            PopStatesFromDepth(0);
        }

        public bool IsStarted()
        {
            return _stateStack.Count > 0; // Always has at least one state on the stack if started
        }

        public void Update(float deltaTime)
        {
            ProcessStateTransitions();
            UpdateStates(deltaTime);
        }

        public void ProcessStateTransitions()
        {
            bool isFinishedTransitioning = false;
            int loopCountdown = 100;
            while (!isFinishedTransitioning && --loopCountdown > 0)
            {
                if (loopCountdown == 4) // Something's wrong, start logging
                {
                    _traceLevel = TraceLevel.Diagnostic;
                }
                isFinishedTransitioning = ProcessStateTransitionsOnce();
            }

            if (loopCountdown == 0)
            {
                Debug.Print("Infinite loop detected !!!");
            }
        }

        public void UpdateStates(float aDeltaTime)
        {
            foreach (AState state in _stateStack)
            {
                state.Update(aDeltaTime);
            }
        }
        
        public object Owner { get { return _owner; } }

        public TraceLevel TraceLevel { get { return _traceLevel; } set { _traceLevel = value; } }
        
        public StateType FindState<StateType>() where StateType : AState
        {
            foreach (AState state in _stateStack)
            {
                StateType st = state as StateType;
                if (st != null)
                    return st;
            }
            return null;
        }

        public StateType GetState<StateType>() where StateType : AState
        {
            StateType result = FindState<StateType>();
            Debug.Assert(result != null, string.Format("Failed to get state on stack: {0}", typeof(StateType)));
            return result;
        }

        public bool IsInState<StateType>() where StateType : AState
        {
            return FindState<StateType>() != null;
        }

        public AState FindStateAtDepth(int depth)
        {
            if (depth >= 0 && depth < _stateStack.Count)
            {
                return _stateStack[depth];
            }

            return null;
        }

        public AState FindStateAtDepth<StateType>(int depth) where StateType : AState
        {
            return FindStateAtDepth(depth) as StateType;
        }

        public StateType FindOuterStateFromDepth<StateType>(int depth) where StateType : AState
        {
            Debug.Assert(depth >= 0 && depth < _stateStack.Count);
            for (int d = depth - 1; d >= 0; --d)
            {
                StateType st = _stateStack[d] as StateType;
                if (st != null)
                {
                    return st;
                }
            }

            return null;
        }

        public StateType FindInnerStateFromDepth<StateType>(int depth) where StateType : AState
        {
            Debug.Assert(depth >= 0 && depth < _stateStack.Count);
            for (int d = depth + 1; d < _stateStack.Count; ++d)
            {
                StateType st = _stateStack[d] as StateType;
                if (st != null)
                {
                    return st;
                }
            }

            return null;
        }

        public string GetStateStackAsString()
        {
            var s = new StringBuilder();
            foreach (AState state in _stateStack)
            {
                if (s.Length > 0)
                {
                    s.Append(" / ");
                }
                s.Append(state.GetType().ToString());
            }
            return s.ToString();
        }

        public List<AState> GetStateStack()
        {
            return _stateStack;
        }

        // State Stack Visitor functions

        // State visitor delegate - return true to keep visiting the next state on the stack, false to stop
        public delegate bool VisitState<StateType>(StateType state);

        public void VisitOuterToInner<StateType>(VisitState<StateType> visitor) where StateType : AState
        {
            VisitStates<StateType>(visitor, _stateStack);
        }

        public void VisitInnerToOuter<StateType>(VisitState<StateType> visitor) where StateType : AState
        {
            VisitStates<StateType>(visitor, CreateReverseIterator(_stateStack));
        }

        // State Stack Invoker functions - use to invoke a named method with arbitrary args on the
        // state stack. The only restriction is that the method return bool: true to keep invoking
        // on the state stack, false to stop. Note that this uses reflection, which can be costly.

        public void InvokeOuterToInner(string methodName, params object[] args)
        {
            InvokeStates(methodName, args, _stateStack);
        }

        public void InvokeInnerToOuter(string methodName, params object[] args)
        {
            InvokeStates(methodName, args, CreateReverseIterator(_stateStack));
        }

        // PRIVATE

        static private IEnumerable<T> CreateReverseIterator<T>(IList<T> list)
        {
            int count = list.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                yield return list[i];
            }
        }

        private void VisitStates<StateType>(VisitState<StateType> visitor, IEnumerable<AState> enumerable) where StateType : AState
        {
            foreach (AState state in enumerable)
            {
                bool keepGoing = visitor((StateType)state);
                if (keepGoing == false)
                {
                    return;
                }
            }
        }

        private void InvokeStates(string methodName, object[] args, IEnumerable<AState> enumerable)
        {
            foreach (AState state in enumerable)
            {
                MethodInfo methodInfo = state.GetType().GetMethod(methodName);
                if (methodInfo != null)
                {
                    bool keepGoing = (bool)methodInfo.Invoke(state, args);
                    if (keepGoing == false)
                    {
                        return;
                    }
                }
            }
        }

        private void LogTransition(TraceLevel traceLevel, int depth, string transitionName, Type targetStateType)
        {
            if (_traceLevel < traceLevel)
            {
                return;
            }

            string s = string.Format("HSM [{0}]:{1}{2,-11}{3}",
                (_owner != null ? _owner : "NoOwner"),
                new String(' ', depth),
                transitionName,
                targetStateType);

            Debug.Print( s );
        }

        private AState CreateState(Type stateType, int stackDepth)
        {
            AState state = (AState)Activator.CreateInstance(stateType);
            state._ownerStateMachine = this;
            state._stackDepth = stackDepth;
            state.OnInitialize();
            return state;
        }

        private void EnterState(AState state, object[] args)
        {
            bool haveArgs = args != null &&
                            args.Length > 0;
#if DEBUG
            string stateName = state.ToString();
            MethodInfo onEnterMethod = state.GetType().GetMethod("OnEnter", BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
            if (onEnterMethod != null) // OnEnter overridden on state
            {
                bool expectsArgs = onEnterMethod.GetParameters().Length > 0;

                if (!haveArgs && expectsArgs)
                {
                    Debug.Fail(String.Format("State {0} expects args, but none were passed in via Transition", stateName));
                }
                else if (haveArgs && !expectsArgs)
                {
                    Debug.Fail(String.Format("State {0} does not expect args, but some were passed in via Transition", stateName));
                }
            }
            else if (haveArgs)
            {
                Debug.Fail(String.Format("Args are being passed via Transition to State {0}, but State doesn't implement OnEnter(params)", stateName));
            }
#endif

            if ( haveArgs )
            {
                state.OnEnter(args);
            }
            else
            {
                state.OnEnter();
            }
        }

        private void ExitState(AState state)
        {
            state.OnExit();
            state.ResetAllStateValues();
        }

        private void PushState(Type stateType, object[] args, int stackDepth)
        {
            LogTransition(TraceLevel.Diagnostic, stackDepth, "(Push)", stateType);
            AState state = CreateState(stateType, stackDepth);
            _stateStack.Add(state);
            EnterState(state, args);
        }

        private void PopStatesFromDepth(int startDepthInclusive)
        {
            int endDepth = _stateStack.Count - 1;

            if (startDepthInclusive > endDepth) // Nothing to pop
            {
                return;
            }

            // From inner to outer
            for (int depth = endDepth; depth >= startDepthInclusive; --depth)
            {
                AState currState = _stateStack[depth];
                LogTransition(TraceLevel.Diagnostic, depth, "(Pop)", currState.GetType());
                ExitState(currState);
            }
            _stateStack.RemoveRange(startDepthInclusive, endDepth - startDepthInclusive + 1);
        }

        private bool HasStateAtDepth(int depth)
        {
            return depth < _stateStack.Count;
        }

        private AState GetStateAtDepth(int depth)
        {
            return depth < _stateStack.Count ? _stateStack[depth] : null;
        }

        // Returns true if state stack is unchanged after calling EvaluateTransition on each state (from outer to inner)
        private bool ProcessStateTransitionsOnce()
        {
            if (_stateStack.Count == 0)
            {
                LogTransition(TraceLevel.Basic, 0, new Transition(TransitionType.Inner, _initialStateType, null).ToString(), _initialStateType);
                PushState(_initialStateType, null, 0);
            }

            for (int currDepth = 0; currDepth < _stateStack.Count; ++currDepth)
            {
                AState currState = _stateStack[currDepth];
                Transition trans = currState.GetTransition();

                switch (trans.TransitionType)
                {
                    case TransitionType.None:
                        break;

                    case TransitionType.Inner:
                        // If state already on stack, continue to next state
                        AState immediateInnerState = GetStateAtDepth(currDepth + 1);
                        if (immediateInnerState != null && immediateInnerState.GetType() == trans.TargetStateType)
                            break;

                        // Pop states below (if any) and push new one
                        LogTransition(TraceLevel.Basic, currDepth + 1, trans.ToString(), trans.TargetStateType);
                        PopStatesFromDepth(currDepth + 1);
                        PushState(trans.TargetStateType, trans.Args, currDepth + 1);
                        return false;

                    case TransitionType.InnerEntry:
                        // Only if no state on stack below us do we push target state
                        if (HasStateAtDepth(currDepth + 1))
                            break;

                        LogTransition(TraceLevel.Basic, currDepth + 1, trans.ToString(), trans.TargetStateType);
                        PushState(trans.TargetStateType, trans.Args, currDepth + 1);
                        return false;

                    case TransitionType.Sibling:
                        LogTransition(TraceLevel.Basic, currDepth, trans.ToString(), trans.TargetStateType);
                        PopStatesFromDepth(currDepth);
                        PushState(trans.TargetStateType, trans.Args, currDepth);
                        return false; // State stack has changed, evaluate from root again

                }
            }

            return true; // State stack has settled, we're done!
        }
    }
}
