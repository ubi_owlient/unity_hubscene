﻿using Models;
using System;
using System.Collections.Generic;
using Utils;

namespace FeatureModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class SettingsModel : AFeatureModel
    {
        private SettingOptionModel _currentSetting;
        private Dictionary<string, SettingOptionModel> _modelSettingById = new Dictionary<string, SettingOptionModel>();

        public event Action<SettingsModel, SettingOptionModel, SettingOptionModel> settingChanged = null;
        
        public SettingOptionModel currentSetting
        {
            get
            {
                return _currentSetting;
            }
            set
            {
                if (_currentSetting != value )
                {
                    SettingOptionModel oldSetting = _currentSetting;
                    _currentSetting = value;
                    NotifySettingChanged( oldSetting, _currentSetting);
                }
            }
        }

        public SettingsModel(params SettingOptionModel[] settings)
        {
            _identifier = Constants.SETTINGS_FEATURE;

            for (int curr = 0; curr < settings.Length; curr++)
            {
                AddSettingOption( settings[ curr ] );
            }
        }

        public void AddSettingOption(SettingOptionModel setting)
        {
            _modelSettingById[ setting.settingOptionId ] = setting;
        }

        public SettingOptionModel GetSetting(string activity)
        {
            SettingOptionModel settingModel = null;
            if ( _modelSettingById.TryGetValue( activity, out settingModel ) )
            {
                return settingModel;
            }

            return null;
        }

        private void NotifySettingChanged(SettingOptionModel oldSetting, SettingOptionModel newSetting)
        {
            if ( settingChanged != null )
            {
                settingChanged( this, oldSetting, newSetting );
            }
        }
    }
}
