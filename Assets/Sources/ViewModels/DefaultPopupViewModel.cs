﻿namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class DefaultPopupViewModel : APopupViewModel
    {
        public DefaultPopupViewModel(string popupId) :
        base( popupId )
        {

        }
    }
}
