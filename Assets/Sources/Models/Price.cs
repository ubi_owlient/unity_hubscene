﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class Price
    {
        private float _amount  = 0;
        private int _currency = -1;

        public float amount
        {
            get
            {
                return _amount;
            }
        }

        public int  currency
        {
            get
            {
                return _currency;
            }
        }

        public Price(float amount, int currency)
        {
            _amount   = amount;
            _currency = currency;
        }
    }
}
