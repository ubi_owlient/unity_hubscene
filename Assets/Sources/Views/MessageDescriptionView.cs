﻿using Enums;
using Menu.Buttons;
using Models;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Widgets;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MessageDescriptionView : MonoBehaviour
    {
        [Header("Info")]
        [SerializeField]
        private Text   _from         = null;

        [SerializeField]
        private Text   _title        = null;

        [SerializeField]
        private Text   _message      = null;

        [Header("Rewards")]
        [SerializeField]
        private RewardPopulator _rewards = null;

        [SerializeField]
        private TitleButton _claimButton = null;

        private MessageModel _model = null;

        public void SetMessage(MessageModel model)
        {
            _model = model;
            
            if ( _from != null )
            {
                _from.text = model.sender;
            }

            if ( _title != null )
            {
                _title.text = model.title;
            }

            if ( _message != null )
            {
                _message.text = model.message;
            }

            if ( _rewards != null )
            {
                _rewards.Populate( model.rewards );

                if ( model.rewards != null &&
                     model.rewards.Count() > 0 )
                {
                    _claimButton.ChangeState( ButtonStateEnum.HIGHLIGHT );
                }
                else
                {
                    _claimButton.ChangeState( ButtonStateEnum.DISABLED );
                }
            }
        }
    }
}
