﻿using Menu;
using Service;
using UnityEngine;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class LoadingView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _root = null;

        private LoadingProgression _progression = null;

        void Start()
        {
            _progression = GetComponentInChildren<LoadingProgression>();

            LoadingService loadingService = ServiceManager.Instance.GetService<LoadingService>();
            loadingService.showLoadingScreenRequired += OnShowLoadingScreenRequired;
            loadingService.updateLoadingScreenRequired += OnUpdateLoadingScreenRequired;
            loadingService.hideLoadingScreenRequired += OnHideLoadingScreenRequired;
        }

        void OnDestroy()
        {
            LoadingService loadingService = ServiceManager.Instance.GetService<LoadingService>();
            loadingService.showLoadingScreenRequired -= OnShowLoadingScreenRequired;
            loadingService.updateLoadingScreenRequired -= OnUpdateLoadingScreenRequired;
            loadingService.hideLoadingScreenRequired -= OnHideLoadingScreenRequired;
        }

        private void OnShowLoadingScreenRequired()
        {
            _root.SetActive( true );
        }

        private void OnUpdateLoadingScreenRequired(uint percentage)
        {
            if ( _progression != null )
            {
                _progression.SetProgression( percentage );
            }
        }

        private void OnHideLoadingScreenRequired()
        {
            _root.SetActive( false );
        }
    }
}
