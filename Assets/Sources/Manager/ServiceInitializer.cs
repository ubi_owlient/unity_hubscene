﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ServiceInitializer : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad( this );
        }
        
        void Start()
        {
            ServiceManager.Instance.Initialize();
        }
    }
}
