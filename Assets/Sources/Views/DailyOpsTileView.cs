﻿using Menu.Buttons;
using Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class DailyOpsTileView : MonoBehaviour, IItemWidget
    {
        [SerializeField]
        private Text _dailyOpsTitle       = null;

        [SerializeField]
        private Text _dailyOpsDescription = null;

        [SerializeField]
        private Text _dailyOpsEntrance    = null;

        [SerializeField]
        private Image _dailyOpsImage      = null;

        [SerializeField]
        private Image _dailyOpsBackground = null;

        [SerializeField]
        private Image _border             = null;

        [SerializeField]
        private Image _topBar             = null;

        [SerializeField]
        private Image _bottomBar          = null;

        [SerializeField]
        private StandardButton _enterButton = null;

        [SerializeField]
        private ImageButton    _infoButton  = null;

        private DailyOpsModel _model = null;

        public DailyOpsModel model
        {
            get
            {
                return _model;
            }
        }

        public uint id
        {
            get
            {
                if ( _model != null )
                {
                    return _model.eventId;
                }

                return 0;
            }
        }
        
        public event Action<IItemWidget> onClick = null;

        void Start()
        {
            if ( _enterButton != null )
            {
                _enterButton.onClick += OnDailyOpsEnter;
            }
        }

        void OnDestroy()
        {
            Dispose();
        }

        public void SetDailyOpsInfo(DailyOpsModel model)
        {
            _model = model;

            if ( _dailyOpsTitle != null )
            {
                _dailyOpsTitle.text = model.eventTitle;
            }

            if ( _dailyOpsDescription != null )
            {
                _dailyOpsDescription.text = model.eventDescription;
            }

            if ( _dailyOpsEntrance != null )
            {
                _dailyOpsEntrance.text = model.isLocked ? "View" : "Enter";
            }

            if ( _dailyOpsImage != null &&
                 string.IsNullOrEmpty( model.eventImage ) == false )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.eventImage ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _dailyOpsImage.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }

            if ( _dailyOpsBackground != null &&
                 string.IsNullOrEmpty( model.eventBackground ) == false )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.eventBackground ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _dailyOpsBackground.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }

            if ( model.isLocked )
            {
                RefreshBorder( Color.grey );
            }
            else
            {
                RefreshBorder( Color.blue );
            }
        }

        private void RefreshBorder(Color color)
        {
            if ( _border != null )
            {
                _border.color = color;

            }

            if ( _topBar != null )
            {
                _topBar.color = color;
            }

            if ( _bottomBar != null )
            {
                _bottomBar.color = color;
            }
        }

        private void OnDailyOpsEnter(ABaseButton button)
        {
            if ( onClick != null )
            {
                onClick( this );
            }
        }

        public void Dispose()
        {
            if (_enterButton != null)
            {
                _enterButton.onClick -= OnDailyOpsEnter;
            }
        }
    }
}
