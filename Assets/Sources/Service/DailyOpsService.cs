﻿using Models;
using System.Collections.Generic;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class DailyOpsService : AFeatureService
    {
        private int _dailyCount = 10;
        private IList<string> _imageNames = null;
        private IList<string> _backgroundNames = null;
        private Dictionary<uint, DailyOpsModel> _dailies = new Dictionary<uint, DailyOpsModel>();

        private List<string> _defaultTitle = new List<string>() { "Energy boost", "Tactical drills", "Pay Day", "Drone Piloting", "Basic traning", "Material Matters" };
        private List<string> _defaultDescription = new List<string>() { "Earn campaign energy",
                                                                        "Gather auto win tokens",
                                                                        "Gather cash",
                                                                        "Gather order currency",
                                                                        "Gather training materials",
                                                                        "Gather gear materials" };

        public int dailyCount
        {
            get
            {
                return _dailyCount;
            }
        }

        public IEnumerable<DailyOpsModel> dailies
        {
            get
            {
                return _dailies.Values;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyOpsService"/> class.
        /// </summary>
        public DailyOpsService()
        {

        }
        
        public void Initialize(int dailyCount, IEnumerable<string> imageNames, IEnumerable<string> backgroundNames)
        {
            _imageNames = new List<string>( imageNames );
            _backgroundNames = new List<string>( backgroundNames );

            _dailyCount = dailyCount;

            int titleIndex = 0;
            int imageIndex = 0;
            int backgroundIndex = 0;
            int imageCount = _imageNames.Count;
            int backgroundCount = _backgroundNames.Count;
            for (uint curr = 0; curr < _dailyCount; curr++)
            {
                int randomIndex = UnityEngine.Random.Range( 0, 5 );

                string campaignTitle = _defaultTitle[ titleIndex ];
                string campaignDescription = _defaultDescription[ titleIndex ];
                
                _dailies.Add( curr, new DailyOpsModel( curr, 
                                                      campaignTitle, 
                                                      campaignDescription, 
                                                      _imageNames[ imageIndex ],
                                                      _backgroundNames[ backgroundIndex ] ) );

                imageIndex++;
                titleIndex++;
                backgroundIndex++;

                if ( imageIndex >= imageCount )
                {
                    imageIndex = 0;
                }

                if ( titleIndex >= 6 )
                {
                    titleIndex = 0;
                }

                if ( backgroundIndex >= backgroundCount )
                {
                    backgroundIndex = 0;
                }
            }
        }
    }
}
