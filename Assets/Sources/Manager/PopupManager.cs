﻿using Menu;
using Service;
using UnityEngine;
using ViewModels;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class PopupManager : MonoBehaviour
    {
        [SerializeField] private PopupOverlayBehavior _popupOverlay = null;

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            PopupService popupService = ServiceManager.Instance.GetService<PopupService>();
            popupService.onPopupOpen  += OnPopupOpen;    
            popupService.onPopupClose += OnPopupClose;
        }

        void OnDestroy()
        {
            PopupService popupService = ServiceManager.Instance.GetService<PopupService>();
            popupService.onPopupOpen  -= OnPopupOpen;    
            popupService.onPopupClose -= OnPopupClose;
        }

        private void OnPopupOpen(PopupService sender, IPopupViewModel popup)
        {
            if ( _popupOverlay != null )
            {
                _popupOverlay.Show();
            }
        }

        private void OnPopupClose(PopupService sender, IPopupViewModel popup)
        {
            if ( _popupOverlay != null )
            {
                _popupOverlay.Hide();
            }
        }
    }
}
