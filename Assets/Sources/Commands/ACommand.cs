﻿using ViewModels;

namespace Commands
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class ACommand : ICommand
    {
        private IModelPresenter _sender = null;

        public IModelPresenter sender
        {
            get
            {
                return _sender;
            }
        }

        protected ACommand(IModelPresenter sender)
        {
            _sender = sender;
        }

        public void Dispose()
        {
            _sender = null;

            OnCustomDispose();
        }

        protected virtual void OnCustomDispose()
        {

        }
    }
}
