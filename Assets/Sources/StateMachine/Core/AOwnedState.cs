﻿namespace FSM
{
    /// <summary>
    /// Definition of the base <see cref="AOwnedState{OwnerType}"/> abstract class.
    /// 
    /// NOTE: Utility base class for states that should be used to access Owner/Data more easily
    /// </summary>
    /// <typeparam name="OwnerType"></typeparam>
    public abstract class AOwnedState<OwnerType> : AState
    {
        private OwnerType _owner;

        /// <summary>
        /// Gets the state owner.
        /// </summary>
        public OwnerType Owner
        {
            get
            {
                if (_owner == null)
                {
                    _owner = (OwnerType)_ownerStateMachine.Owner;
                }

                return _owner;
            }
        }
    }
}
