﻿using FeatureModels;
using Models;

namespace FSM.Hub
{
    /// <summary>
    /// Athor: jlarbi
    /// 
    /// NOTE: This will display and allow user to choose a campaign.
    /// </summary>
    public class CampaignChoiceState : AActivityState
    {
        private CampaignModel     _selectedCampaign = null;
        
        public CampaignChoiceState()
        {
            _stateName = "Campaign choice";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);
            
            
        }

        
        public override void OnExit()
        {
            base.OnExit();
            
            
            
            _selectedCampaign = null;
        }

        protected override void OnBackClicked()
        {
            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.currentStep = null;
            }
        }

        protected override Transition OnRestore()
        {
            return Transition.Sibling<CampaignHubState>( _hubMenu );
        }

        protected override Transition OnQuit()
        {
            return Transition.Sibling<CampaignHubState>( _hubMenu );
        }

        public override Transition GetTransition()
        {
            if ( _selectedCampaign != null )
            {
                return Transition.Sibling<CampaignMissionChoiceState>( _subMenu, _selectedCampaign );
            }

            return base.GetTransition();
        }
    }
}
