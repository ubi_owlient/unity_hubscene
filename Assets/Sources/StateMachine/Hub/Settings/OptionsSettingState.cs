﻿
namespace FSM.Hub.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class OptionsSettingState : ASettingState
    {
        public OptionsSettingState()
        {
            _stateName = "Options setting";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }
        
        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
