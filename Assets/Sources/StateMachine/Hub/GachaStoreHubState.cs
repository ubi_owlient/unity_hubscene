﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The gacha store state.
    /// </summary>
    public class GachaStoreHubState : AHubState
    {
        public GachaStoreHubState()
        {
            _stateName = "Gacha Store hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);


        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);


        }

        public override void OnExit()
        {
            base.OnExit();


        }
    }
}
