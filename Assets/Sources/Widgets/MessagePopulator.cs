﻿using Models;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MessagePopulator : AItemPopulator<MessageTileView>
    {
        public void Populate(IEnumerable<MessageModel> messages)
        {
            pool.CleanUp();

            if ( messages == null )
            {
                return;
            }

            uint count = 0;
            foreach ( MessageModel message in messages )
            {
                MessageTileView widget = pool.GetItem( count );
                widget.SetMessage( message );
                count++;
            }
        }
    }
}
