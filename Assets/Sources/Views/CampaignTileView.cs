﻿using Menu.Buttons;
using Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CampaignTileView : MonoBehaviour, IItemWidget
    {
        [SerializeField]
        private Text _campaignTitle       = null;

        [SerializeField]
        private Text _campaignDescription = null;

        [SerializeField]
        private Text _campaignEntrance    = null;

        [SerializeField]
        private Image _campaignImage      = null;

        [SerializeField]
        private Image _campaignBackground = null;

        [SerializeField]
        private Image _border             = null;

        [SerializeField]
        private Image _topBar             = null;

        [SerializeField]
        private Image _bottomBar          = null;

        [SerializeField]
        private StandardButton _enterButton = null;

        [SerializeField]
        private ImageButton    _infoButton  = null;

        private CampaignModel _model = null;

        public CampaignModel model
        {
            get
            {
                return _model;
            }
        }

        public uint id
        {
            get
            {
                if ( _model != null )
                {
                    return _model.campaignId;
                }

                return 0;
            }
        }
        
        public event Action<IItemWidget> onClick = null;

        void Start()
        {
            if ( _enterButton != null )
            {
                _enterButton.onClick += OnCampaignEnter;
            }
        }

        void OnDestroy()
        {
            Dispose();
        }

        public void SetCampaignInfo(CampaignModel model)
        {
            _model = model;

            if ( _campaignTitle != null )
            {
                _campaignTitle.text = model.campaignTitle;
            }

            if ( _campaignDescription != null )
            {
                _campaignDescription.text = model.campaignDescription;
            }

            if ( _campaignEntrance != null )
            {
                _campaignEntrance.text = model.isLocked ? "View" : "Enter";
            }

            if ( _campaignImage != null &&
                 string.IsNullOrEmpty( model.campaignImage ) == false )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.campaignImage ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _campaignImage.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }

            if ( _campaignBackground != null &&
                 string.IsNullOrEmpty( model.campaignBackground ) == false )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_UI_PATH, model.campaignBackground ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _campaignBackground.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }

            if ( model.isLocked )
            {
                RefreshBorder( Color.grey );
            }
            else
            {
                RefreshBorder( Color.cyan );
            }
        }

        private void RefreshBorder(Color color)
        {
            if ( _border != null )
            {
                _border.color = color;

            }

            if ( _topBar != null )
            {
                _topBar.color = color;
            }

            if ( _bottomBar != null )
            {
                _bottomBar.color = color;
            }
        }

        private void OnCampaignEnter(ABaseButton button)
        {
            if ( onClick != null )
            {
                onClick( this );
            }
        }

        public void Dispose()
        {
            if (_enterButton != null)
            {
                _enterButton.onClick -= OnCampaignEnter;
            }
        }
    }
}
