﻿using Commands;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class DispatcherService : ACoreService
    {
        private const BindingFlags _flags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

        private Dictionary<Type, ICommandHandler> _instanceHandlerByType = new Dictionary<Type, ICommandHandler>();
        private Dictionary<Type, List<MethodInfo>> _handlersByCommandType = new Dictionary<Type, List<MethodInfo>>();

        private ConcurrentQueue<ICommand> _pendingCommands = new ConcurrentQueue<ICommand>();
        
        public void RegisterHandler(ICommandHandler handler)
        {
            if ( handler == null )
            {
                return;
            }

            Type handlerType = handler.GetType();
            _instanceHandlerByType[ handlerType ] = handler;

            MethodInfo[] methods = handlerType.GetMethods( _flags );
            for ( int curr = 0; curr < methods.Length; curr++ )
            {
                MethodInfo method = methods[ curr ];
                HandleCommandAttribute attribute = method.GetCustomAttribute( typeof(HandleCommandAttribute), true ) as HandleCommandAttribute;
                if ( attribute != null )
                {
                    List<MethodInfo> handlers = null;
                    if ( _handlersByCommandType.TryGetValue( attribute.commandType, out handlers ) == false )
                    {
                        handlers = new List<MethodInfo>();
                        _handlersByCommandType.Add( attribute.commandType, handlers );
                    }

                    handlers.Add( method );
                }
            }
        }

        public void Dispatch(ICommand command)
        {
            ProcessCommand( command );
        }

        public void DispatchAsync(ICommand command)
        {
            _pendingCommands.Enqueue( command );
        }

        public void Update()
        {
            ICommand command = null;
            while ( _pendingCommands.Count > 0 &&
                    _pendingCommands.TryDequeue( out command ) )
            {
                ProcessCommand( command );
            }
        }

        private void ProcessCommand(ICommand command)
        {
            Type commandType = command.GetType();
            List<MethodInfo> callbacks = null;
            if ( _handlersByCommandType.TryGetValue( commandType, out callbacks ) )
            {
                for ( int curr = 0; curr < callbacks.Count; curr++ )
                {
                    MethodInfo callback = callbacks[ curr ];
                    if ( callback.ReflectedType != null )
                    {
                        ICommandHandler handler = null;
                        if ( _instanceHandlerByType.TryGetValue( callback.ReflectedType, out handler ) )
                        {
                            callback.Invoke( handler, new object[] { command } );
                        }
                    }
                }
            }
        }
        
        [AttributeUsage(AttributeTargets.Method)]
        public class HandleCommandAttribute : Attribute
        {
            public Type commandType = null;
        }
    }
}
