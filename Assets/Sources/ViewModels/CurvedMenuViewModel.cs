﻿using FeatureModels;
using Models;
using System.ComponentModel;

namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CurvedMenuViewModel : AViewModel
    {
        public bool isOpen
        {
            get
            {
                if ( step != null )
                {
                    return true;
                }

                return false;
            }
        }

        public ActivityStep step
        {
            get
            {
                HubModel hubModel = GetModel<HubModel>();
                if ( hubModel != null )
                {
                    return hubModel.currentStep;
                }

                return null;
            }
        }

        public CurvedMenuViewModel(params IFeatureModel[] models) :
        base( models )
        {
            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.PropertyChanged += OnHubMenuPropertyChanged;
            }
        }

        protected override void OnDispose()
        {
            base.OnDispose();

            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.PropertyChanged -= OnHubMenuPropertyChanged;
            }
        }

        private void OnHubMenuPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if ( args.PropertyName == "currentStep" )
            {
                RaisePropertyChanged( "step" );
                RaisePropertyChanged( "isOpen" );
            }
        }
    }
}
