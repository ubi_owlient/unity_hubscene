﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class AnimationManager : MonoBehaviour
    {
        private AnimationService _animationService = null;

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            _animationService = ServiceManager.Instance.GetService<AnimationService>();
            _animationService.Initialize();
        }

        void Update()
        {
            if ( _animationService != null )
            {
                _animationService.Update();
            }
        }
    }
}
