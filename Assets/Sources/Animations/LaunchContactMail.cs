﻿using UnityEngine;

namespace Animations
{
    public class LaunchContactMail : MonoBehaviour
    {
        public string destinatioScene;
        public string mailToString;

        private void Start()
        {
#if UNITY_IPHONE
		Application.OpenURL (mailToString + "_IOS");
#elif UNITY_ANDROID
            Application.OpenURL(mailToString + "_ANDROID");
#else
		Application.OpenURL (mailToString);
#endif

            ChangeLevel.GoTo(destinatioScene);
        }
    }
}