﻿using Menu.Buttons;
using Service;
using System.Collections.Generic;
using Types.Notifications;
using UnityEngine;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class NotificationsView : MonoBehaviour
    {
        [SerializeField]
        private ImageButton _showButton  = null;

        [SerializeField]
        private ImageButton _closeButton = null;

        [SerializeField]
        private GameObject _ephemeralNotifPrefab = null;

        [SerializeField]
        private GameObject _ephemeralNotifParent = null;

        [SerializeField]
        private Animator   _ephemeralNotifAnim   = null;
        
        private Animator _allNotifAnim           = null;
        private bool _isAllNotifDisplayed        = false;

        private float _timer                     = 0.0f;
        private float _delay                     = 0.0f;
        private bool _isEphemeralDisplayed       = false;
        private Queue<ANotificationDescription> _pendingNotifications = new Queue<ANotificationDescription>();

        void Start()
        {
            _allNotifAnim = GetComponent<Animator>();

            _showButton.onClick  += OnShowClick;
            _closeButton.onClick += OnCloseClick;

            // Prepare the ephemeral notification.
            if ( _ephemeralNotifPrefab != null )
            {
                Instantiate( _ephemeralNotifPrefab, _ephemeralNotifParent.transform );
            }

            ServiceManager.Instance.GetService<NotificationService>().notificationReceived += OnNotificationReceived;
        }
        
        void OnDestroy()
        {
            _showButton.onClick  -= OnShowClick;
            _closeButton.onClick -= OnCloseClick;

            ServiceManager.Instance.GetService<NotificationService>().notificationReceived -= OnNotificationReceived;

            _pendingNotifications.Clear();
        }

        void Update()
        {
            // Already showing one?
            if ( _isEphemeralDisplayed )
            {
                _timer += Time.unscaledDeltaTime;
                float seconds = _timer % 60;
                if ( seconds >= _delay )
                {
                    HideEphemeralNoticiation();
                }
            }
            else if ( _pendingNotifications.Count > 0 )
            {
                // Enqueue a pending one if none displayed.
                ShowEphemeralNoticiation( _pendingNotifications.Dequeue() );
            }
        }

        private void OnShowClick(ABaseButton button)
        {
            ShowAllNotif();
        }

        private void OnCloseClick(ABaseButton button)
        {
            HideAllNotif();
        }

        private void OnNotificationReceived(NotificationService sender, ANotificationDescription args)
        {
            // Triggers an ephemeral notification.
            if ( ShowEphemeralNoticiation( args ) == false )
            {
                _pendingNotifications.Enqueue( args );
            }
        }

        private void ShowAllNotif()
        {
            if ( _isAllNotifDisplayed == false )
            {
                _allNotifAnim.SetTrigger( "OpenNotificationPanel" );

                _isAllNotifDisplayed = true;
            }
        }

        private void HideAllNotif()
        {
            if ( _isAllNotifDisplayed )
            {
                _allNotifAnim.SetTrigger( "CloseNotificationPanel" );

                _isAllNotifDisplayed = false;
            }
        }

        private bool ShowEphemeralNoticiation(ANotificationDescription args)
        {
            if ( _isEphemeralDisplayed == false )
            {
                _ephemeralNotifAnim.SetTrigger( "ShowEphemeralNotification" );

                _timer = 0.0f;
                _delay = args.notificationDelay.HasValue ? args.notificationDelay.Value : ServiceManager.Instance.GetService<NotificationService>().defaultDelay;

                SimpleNotificationWidget widget = _ephemeralNotifParent.GetComponentInChildren<SimpleNotificationWidget>();
                if ( widget != null )
                {
                    widget.SetNotification( args as SimpleMessageNotificationDescription );
                }

                _isEphemeralDisplayed = true;

                return true;
            }

            return false;
        }

        private void HideEphemeralNoticiation()
        {
            if ( _isEphemeralDisplayed )
            {
                _ephemeralNotifAnim.SetTrigger( "HideEphemeralNotification" );
                
                _isEphemeralDisplayed = false;
            }
        }
    }
}
