﻿using System;
using Types.Notifications;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class NotificationService : AFeatureService
    {
        private float _notificationDelay;

        /// <summary>
        /// Gets the default notification delay.
        /// </summary>
        public float defaultDelay
        {
            get
            {
                return _notificationDelay;
            }
        }
        
        /// <summary>
        /// Event fired on notification received.
        /// </summary>
        public event Action<NotificationService, ANotificationDescription> notificationReceived = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationService"/> class.
        /// </summary>
        public NotificationService()
        {

        }

        /// <summary>
        /// Initializes the service.
        /// </summary>
        /// <param name="notificationDelay">The delay in seconds.</param>
        public void Initialize(float notificationDelay)
        {
            _notificationDelay = notificationDelay;
        }

        /// <summary>
        /// Notifies something to the player.
        /// </summary>
        /// <param name="description"></param>
        public void Notify(ANotificationDescription description)
        {
            if ( description == null )
            {
                return;
            }

            // No delay set?
            if ( description.notificationDelay.HasValue == false )
            {
                // Assign the default one.
                description.notificationDelay = _notificationDelay;
            }

            if ( notificationReceived != null )
            {
                notificationReceived( this, description );
            }
        }
    }
}
