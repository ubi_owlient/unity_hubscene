﻿using ViewModels;

namespace Commands.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SelectSettingOptionCommand : ACommand
    {
        public string settingId
        {
            get;
            private set;
        }

        public SelectSettingOptionCommand(IModelPresenter sender, string settingId) :
        base(sender)
        {
            this.settingId = settingId;
        }
    }
}
