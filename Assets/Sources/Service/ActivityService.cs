﻿using Enums;
using FeatureModels;
using FSM;
using FSM.Hub;
using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class ActivityService : AFeatureService
    {
        private HubModel _hubModel = null;

        private GameObject _activityFocusPrefab = null;
        private GameObject _pointerParent       = null;
        private ActivityEnum _currentActivity   = ActivityEnum.NONE;
        private ActivityModel _currentMenu     = null;
        
        private Dictionary<ActivityEnum, GameObject> _focusPointersByActivity = new Dictionary<ActivityEnum, GameObject>();

        private HashSet<ActivityEnum> focusedActivities = new HashSet<ActivityEnum>();

        private StateMachine _activityStateMachine = new StateMachine();

        public event Action<ActivityService, ActivityEnum, GameObject> onActivityFocused   = null;

        public event Action<ActivityService, ActivityEnum, GameObject> onActivityUnfocused = null;

        public event Action<ActivityService, ActivityEnum, ActivityModel> onActivityChanged = null;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityService"/> class.
        /// </summary>
        public ActivityService()
        {

        }

        public void Initialize(GameObject activityFocusPrefab)
        {
            _activityFocusPrefab = activityFocusPrefab;

            _hubModel = GetModel<HubModel>();
            _hubModel.PropertyChanged += OnHubModelPropertyChanged;

            _activityStateMachine.Init<HubRootState>( ServiceManager.Instance );
            _activityStateMachine.TraceLevel = TraceLevel.Diagnostic;
        }

        private void OnHubModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            HubModel hubModel = sender as HubModel;

            if ( args.PropertyName == "currentActivity" )
            {
                _currentMenu     = hubModel.currentActivity;
                _currentActivity = _currentMenu.activity;

                if ( onActivityChanged != null )
                {
                    onActivityChanged( this, _currentActivity, _currentMenu );
                }
            }
        }

        public void SetFocusNotificationParent(GameObject parent)
        {
            _pointerParent = parent;
        }

        public void Update()
        {
            _activityStateMachine.Update( Time.deltaTime );
        }
        
        public void FocusActivity(ActivityEnum activity)
        {
            if ( focusedActivities.Contains( activity ) )
            {
                return;
            }

            focusedActivities.Add( activity );

            if ( onActivityFocused != null )
            {
                onActivityFocused( this, activity, GetOrCreatePointer( activity ) );
            }
        }

        public void UnfocusActivity(ActivityEnum activity)
        {
            if ( focusedActivities.Remove( activity ) == false )
            {
                return;
            }

            if ( onActivityUnfocused != null )
            {
                GameObject pointer = GetOrCreatePointer( activity );
                onActivityUnfocused( this, activity, pointer );
                DestroyPointer( activity, pointer );
            }
        }

        private GameObject GetOrCreatePointer(ActivityEnum activity)
        {
            GameObject pointer = null;
            if ( _focusPointersByActivity.TryGetValue( activity, out pointer ) )
            {
                return pointer;
            }

            pointer = GameObject.Instantiate( _activityFocusPrefab, _pointerParent.transform );
            _focusPointersByActivity[ activity ] = pointer;

            return pointer;
        }

        private void DestroyPointer(ActivityEnum activity, GameObject pointer)
        {
            _focusPointersByActivity.Remove( activity );
            GameObject.Destroy( pointer );
        }

        public void Dispose()
        {
            _activityStateMachine.Shutdown();
        }
    }
}
