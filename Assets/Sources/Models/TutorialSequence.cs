﻿using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class TutorialSequence : TutorialPart
    {
        private Queue<string> _nextParts = new Queue<string>();

        public TutorialSequence(string id, string explanation, string image, params string[] nextIds) : 
        base(id, explanation, image)
        {
            for (int curr = 0; curr < nextIds.Length; curr++)
            {
                _nextParts.Enqueue( nextIds[ curr ] );
            }
        }

        /// <summary>
        /// Consumes the next tutorial step in the sequence.
        /// </summary>
        /// <returns>The next tuto part id, null otherwise.</returns>
        public string Next()
        {
            if ( _nextParts.Count > 0)
            {
                return _nextParts.Dequeue();
            }

            return null;
        }
    }
}
