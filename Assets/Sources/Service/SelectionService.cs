﻿using System;
using UnityEngine;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class SelectionService : AFeatureService
    {
        private Transform _currentSelection = null;

        public Action<SelectionService, Transform, Transform> selectionChanged = null;
        
        public Transform currentSelection
        {
            get
            {
                return _currentSelection;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectionService"/> class.
        /// </summary>
        public SelectionService()
        {

        }
        
        public void SetSelection(Transform selection)
        {
            Transform oldSelection = _currentSelection;
            _currentSelection = selection;

            NotifySelectionChanged( oldSelection, _currentSelection );
        }

        private void NotifySelectionChanged(Transform oldSelection, Transform selection)
        {
            if ( selectionChanged != null )
            {
                selectionChanged( this, oldSelection, selection );
            }
        }
    }
}
