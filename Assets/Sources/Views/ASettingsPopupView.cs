﻿using Commands.Settings;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class ASettingsPopupView : APopupView
    {
        protected override void OnCustomShow()
        {
            base.OnCustomShow();

            Dispatch( new SelectSettingOptionCommand( this, id ) );
        }

        protected override void OnCustomHide()
        {
            base.OnCustomHide();

            Dispatch( new UnselectSettingOptionCommand( this, id ) );
        }
    }
}
