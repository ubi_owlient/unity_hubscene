﻿using Service;
using System;
using System.Collections;
using UnityEngine;
using Widgets;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class InboxView : MonoBehaviour
    {
        private const uint INBOX_TAB_COUNT = 2;

        [SerializeField]
        private TabView _tabView = null;

        [SerializeField]
        private MessageDescriptionView _description = null;

        private MessagePopulator[] _populators = new MessagePopulator[ INBOX_TAB_COUNT ];
        
        void Start()
        {
            _tabView.isInitialized += OnTabViewInitialized;
            _tabView.tabClicked    += OnTabClicked;


            _description.gameObject.SetActive( false );
        }

        private void OnTabClicked(TabView sender, TabWidget tab)
        {
            switch (tab.index)
            {
                case 0:
                    {
                        StartCoroutine( RefreshTabContent( RefreshUnread ) );
                    }
                    break;
                case 1:
                    {
                        StartCoroutine( RefreshTabContent( RefreshRead ) );
                    }
                    break;
            }
        }

        private void OnTabViewInitialized(TabView sender)
        {
            MessagePopulator unreadPopulator = _tabView.GetTabContentComponent<MessagePopulator>( 0 );
            if ( unreadPopulator != null )
            {
                unreadPopulator.Populate( ServiceManager.Instance.GetService<MailService>().unreadMails );
                unreadPopulator.onClick += OnMailClicked;
                _populators[ 0 ] = unreadPopulator;
            }

            MessagePopulator readPopulator = _tabView.GetTabContentComponent<MessagePopulator>( 1 );
            if ( readPopulator != null )
            {
                readPopulator.Populate( ServiceManager.Instance.GetService<MailService>().readMails );
                readPopulator.onClick += OnMailClicked;
                _populators[ 1 ] = readPopulator;
            }

            _tabView.RefreshTitle( 0, "Unread" );
            _tabView.RefreshTitle( 1, "Read" );
            _tabView.SelectTab( 0 );
        }

        private void RefreshUnread()
        {
            if ( _populators.Length > 0 )
            {
                _populators[ 0 ].Populate( ServiceManager.Instance.GetService<MailService>().unreadMails );
            }
        }

        private void RefreshRead()
        {
            if ( _populators.Length > 1 )
            {
                _populators[ 1 ].Populate( ServiceManager.Instance.GetService<MailService>().readMails );
            }
        }

        void OnDestroy()
        {
            _tabView.isInitialized -= OnTabViewInitialized;
            _tabView.tabClicked    -= OnTabClicked;

            for (int curr = 0; curr < _populators.Length; curr++)
            {
                _populators[ curr ].onClick -= OnMailClicked;
            }
        }

        private void OnMailClicked(AItemPopulator<MessageTileView> sender, MessageTileView mailView)
        {
            _description.gameObject.SetActive( true );
            
            StartCoroutine( UpdateCurrentMessage( mailView ) );
        }

        private IEnumerator RefreshTabContent(Action action)
        {
            yield return null;

            action();
        }

        private IEnumerator UpdateCurrentMessage(MessageTileView mailView)
        {
            yield return null;

            _description.SetMessage( mailView.model );
        }
    }
}
