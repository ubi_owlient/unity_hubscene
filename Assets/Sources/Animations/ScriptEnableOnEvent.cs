﻿using Service;
using UnityEngine;

namespace Animations
{
    public class ScriptEnableOnEvent : Receiver
    {
        public GameObject[] targetObjects;
        public MonoBehaviour[] targetBehaviours;
        public Collider[] targetColliders;
        public Collider2D[] target2DColliders;
        public string inEventEnable;
        public string inEventDisable;

        void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            inEventEnable  = animService.PrepareEventName(inEventEnable, gameObject);
            inEventDisable = animService.PrepareEventName(inEventDisable, gameObject);
            animService.Register(inEventEnable, this);
            animService.Register(inEventDisable, this);
        }

        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            animService.Unregister(inEventEnable, this);
            animService.Unregister(inEventDisable, this);
        }

        void DoEnableScripts(bool enable)
        {
            for (int i = 0; i < targetObjects.Length; ++i)
            {
                MonoBehaviour[] scriptComponents = targetObjects[i].GetComponents<MonoBehaviour>();
                foreach (MonoBehaviour script in scriptComponents)
                {
                    script.enabled = enable;
                }
            }

            for (int i = 0; i < targetBehaviours.Length; ++i)
            {
                targetBehaviours[i].enabled = enable;
            }

            for (int i = 0; i < targetColliders.Length; ++i)
            {
                targetColliders[i].enabled = enable;
            }

            for (int i = 0; i < target2DColliders.Length; ++i)
            {
                target2DColliders[i].enabled = enable;
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            if (eventName.Equals(inEventEnable))
            {
                DoEnableScripts(true);
            }

            if (eventName.Equals(inEventDisable))
            {
                DoEnableScripts(false);
            }
        }
    }
}