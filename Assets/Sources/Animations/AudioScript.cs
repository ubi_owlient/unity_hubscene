using Service;
using UnityEngine;

namespace Animations
{
    public class AudioScript : Receiver
    {

        [System.Serializable]
        public struct sSound
        {
            public float volume;
            public AudioClip clip;
            public string inActivateAudio;
            public string inDesactivateAudio;
            public string inSetVolume;
        };

        public string sourceName;
        public bool isPriority = false;
        public sSound[] sounds;

        private GameObject sourceObj;

        void Start()
        {
            if (sourceName.Length > 0)
            {
                //sourceObj = GameObject.Find(sourceName);
                Debug.LogWarning("Unable to find source object for performance reasons. Please reach a developer.");
            }
            else
            {
                sourceObj = gameObject;
            }

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < sounds.Length; ++i)
            {
                sounds[i].inActivateAudio    = animService.PrepareEventName(sounds[i].inActivateAudio, gameObject);
                sounds[i].inDesactivateAudio = animService.PrepareEventName(sounds[i].inDesactivateAudio, gameObject);
                sounds[i].inSetVolume        = animService.PrepareEventName(sounds[i].inSetVolume, gameObject);

                animService.Register(sounds[i].inActivateAudio, this);
                animService.Register(sounds[i].inDesactivateAudio, this);
                animService.Register(sounds[i].inSetVolume, this);
            }
        }

        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < sounds.Length; ++i)
            {
                animService.Unregister(sounds[i].inActivateAudio, this);
                animService.Unregister(sounds[i].inDesactivateAudio, this);
                animService.Unregister(sounds[i].inSetVolume, this);
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            for (int i = 0; i < sounds.Length; ++i)
            {
                if (eventName.Equals(sounds[i].inActivateAudio))
                {
                    if (isPriority || !sourceObj.GetComponent<AudioSource>().isPlaying)
                    {
                        sourceObj.GetComponent<AudioSource>().clip = sounds[i].clip;
                        sourceObj.GetComponent<AudioSource>().Play();
                    }
                }
                if (eventName.Equals(sounds[i].inDesactivateAudio))
                {
                    sourceObj.GetComponent<AudioSource>().Stop();
                }
                if (eventName.Equals(sounds[i].inSetVolume))
                {
                    sourceObj.GetComponent<AudioSource>().volume = sounds[i].volume;
                }
            }
        }
    }
}