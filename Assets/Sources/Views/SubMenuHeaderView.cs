﻿using Menu.Buttons;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SubMenuHeaderView : MonoBehaviour
    {
        [SerializeField]
        private ImageButton _backButton  = null;

        [SerializeField]
        private ImageButton _homeButton  = null;

        [SerializeField]
        private Text        _title       = null;

        [SerializeField]
        private Text        _description = null;

        public event Action<SubMenuHeaderView, ABaseButton> onBackClicked = null;
        public event Action<SubMenuHeaderView, ABaseButton> onHomeClicked = null;

        void Start()
        {
            _backButton.onClick += OnBackButtonClick;
            _homeButton.onClick += OnHomeButtonClick;
        }

        void OnDestroy()
        {
            _backButton.onClick -= OnBackButtonClick;
            _homeButton.onClick -= OnHomeButtonClick;
        }

        public void SetMenuInfo(string title, string description)
        {
            if ( _title != null )
            {
                _title.text = title.ToUpper();
            }

            if ( _description != null )
            {
                _description.text = description.ToUpper();
            }
        }

        private void OnBackButtonClick(ABaseButton button)
        {
            if ( onBackClicked != null )
            {
                onBackClicked( this, button );
            }
        }

        private void OnHomeButtonClick(ABaseButton button)
        {
            if ( onHomeClicked != null )
            {
                onHomeClicked( this, button );
            }
        }
    }
}
