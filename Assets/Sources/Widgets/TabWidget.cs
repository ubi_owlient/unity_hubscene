﻿using System;
using UnityEngine;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class TabWidget : IDisposable
    {
        private uint           _index      = 0;

        private TabHeaderView  _tabHeader  = null;
        private TabContentView _tabContent = null;

        public event Action<TabWidget, TabHeaderView> onTabClick = null;

        public uint index
        {
            get
            {
                return _index;
            }
        }

        public TabWidget(uint index, GameObject tabHeader, GameObject tabContent)
        {
            _index = index;

            _tabHeader  = tabHeader.GetComponent<TabHeaderView>();
            if ( _tabHeader != null )
            {
                _tabHeader.onTabClick += OnTabClick;
            }

            _tabContent = tabContent.GetComponent<TabContentView>();
        }
        
        private void OnTabClick(TabHeaderView sender, Menu.Buttons.ABaseButton button)
        {
            if ( onTabClick != null )
            {
                onTabClick( this, sender );
            }
        }

        public void RefreshTitle(string title)
        {
            if ( _tabHeader != null )
            {
                _tabHeader.RefreshTitle( title );
            }
        }

        public T GetContentComponent<T>() where T : class
        {
            T result = null;
            if ( _tabContent != null )
            {
                result = _tabContent.GetComponent<T>();
                if ( result == null )
                {
                    result = _tabContent.GetComponentInChildren<T>();
                }
            }

            return result;
        }

        public void RefreshContent()
        {
            
        }

        public void Select()
        {
            if ( _tabHeader != null )
            {
                _tabHeader.Select();
            }

            if ( _tabContent != null )
            {
                _tabContent.Show();
            }
        }

        public void Unselect()
        {
            if ( _tabHeader != null )
            {
                _tabHeader.Unselect();
            }

            if ( _tabContent != null )
            {
                _tabContent.Hide();
            }
        }

        public void Dispose()
        {
            if ( _tabHeader != null )
            {
                _tabHeader.onTabClick -= OnTabClick;
            }
        }
    }
}
