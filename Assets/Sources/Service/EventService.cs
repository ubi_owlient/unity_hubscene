﻿using Models;
using System.Collections.Generic;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class EventService : AFeatureService
    {
        private int _eventCount = 10;
        private IList<string> _imageNames = null;
        private IList<string> _backgroundNames = null;
        private Dictionary<uint, EventModel> _events = new Dictionary<uint, EventModel>();

        private List<string> _defaultTitle = new List<string>() { "Events", "Countdown", "Events", "Countdown", "Events", "Countdown" };
        private List<string> _defaultDescription = new List<string>() { "Gather Intel on El sueno",
                                                                        "Gather Intel on Caveira",
                                                                        "Gather Intel on Sam Fisher",
                                                                        "Gather Intel on GU1",
                                                                        "A friend needs",
                                                                        "Let us count down to zero" };

        public int eventCount
        {
            get
            {
                return _eventCount;
            }
        }

        public IEnumerable<EventModel> events
        {
            get
            {
                return _events.Values;
            }
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="EventService"/> class.
        /// </summary>
        public EventService()
        {

        }
        
        public void Initialize(int eventCount, IEnumerable<string> imageNames, IEnumerable<string> backgroundNames)
        {
            _imageNames = new List<string>( imageNames );
            _backgroundNames = new List<string>( backgroundNames );

            _eventCount = eventCount;

            int titleIndex = 0;
            int imageIndex = 0;
            int backgroundIndex = 0;
            int imageCount = _imageNames.Count;
            int backgroundCount = _backgroundNames.Count;
            for (uint curr = 0; curr < _eventCount; curr++)
            {
                int randomIndex = UnityEngine.Random.Range( 0, 5 );

                string campaignTitle = _defaultTitle[ titleIndex ];
                string campaignDescription = _defaultDescription[ titleIndex ];
                
                _events.Add( curr, new EventModel( curr, 
                                                   campaignTitle, 
                                                   campaignDescription, 
                                                   _imageNames[ imageIndex ],
                                                   _backgroundNames[ backgroundIndex ] ) );

                imageIndex++;
                titleIndex++;
                backgroundIndex++;

                if ( imageIndex >= imageCount )
                {
                    imageIndex = 0;
                }

                if ( titleIndex >= 6 )
                {
                    titleIndex = 0;
                }

                if ( backgroundIndex >= backgroundCount )
                {
                    backgroundIndex = 0;
                }
            }
        }
    }
}
