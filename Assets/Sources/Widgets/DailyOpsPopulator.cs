﻿using Models;
using Service;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class DailyOpsPopulator : AItemPopulator<DailyOpsTileView>
    {
        protected override void OnCustomInitialize()
        {
            Populate( ServiceManager.Instance.GetService<DailyOpsService>().dailies );
        }

        private void Populate(IEnumerable<DailyOpsModel> dailies)
        {
            uint count = 0;
            foreach ( DailyOpsModel dailyModel in dailies )
            {
                DailyOpsTileView widget = pool.GetItem( count );
                widget.SetDailyOpsInfo( dailyModel );
                count++;
            }
        }
    }
}
