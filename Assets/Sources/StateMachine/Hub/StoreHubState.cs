﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The store state.
    /// </summary>
    public class StoreHubState : AHubState
    {
        public StoreHubState()
        {
            _stateName = "Store hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            
        }

        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
