﻿using ViewModels;

namespace Commands.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class OpenSettingCommand : ACommand
    {
        public OpenSettingCommand(IModelPresenter sender) : 
        base(sender)
        {

        }
    }
}
