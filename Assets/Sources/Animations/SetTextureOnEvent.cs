﻿using Service;
using UnityEngine;

namespace Animations
{
    public class SetTextureOnEvent : Receiver
    {
        [System.Serializable]
        public struct sActions
        {
            public string inEvent;
            public Texture texture;
            public int shaderId;
            public GameObject targetObject;
        };

        public sActions[] actions;

        
        private void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                actions[i].inEvent = animService.PrepareEventName(actions[i].inEvent, gameObject);
                animService.Register(actions[i].inEvent, this);
            }
        }

        private void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                animService.Unregister(actions[i].inEvent, this);
            }
        }

        public override void Receive(string eventName, GameObject obj)
        {
            for (int i = 0; i < actions.Length; ++i)
            {
                sActions curA = actions[i];
                GameObject curobj = curA.targetObject != null ? curA.targetObject : gameObject;
                if (eventName.Equals(curA.inEvent))
                {
                    MeshRenderer meshMesh = (curobj.GetComponent<MeshRenderer>());
                    if (meshMesh != null)
                    {
                        meshMesh.material.SetTexture(curA.shaderId, curA.texture);
                    }
                    SkinnedMeshRenderer skinMesh = (curobj.GetComponent<SkinnedMeshRenderer>());
                    if (skinMesh != null)
                    {
                        skinMesh.material.SetTexture(curA.shaderId, curA.texture);
                    }
                }
            }
        }
    }
}