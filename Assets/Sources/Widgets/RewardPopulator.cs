﻿using Models;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class RewardPopulator : AItemPopulator<RewardTileView>
    {
        public void Populate(IEnumerable<RewardModel> rewards)
        {
            pool.CleanUp();

            if ( rewards == null )
            {
                return;
            }

            uint count = 0;
            foreach ( RewardModel reward in rewards )
            {
                RewardTileView widget = pool.GetItem( count );
                widget.SetReward( reward );
                count++;
            }
        }
    }
}
