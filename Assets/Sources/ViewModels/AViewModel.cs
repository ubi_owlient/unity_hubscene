﻿using FeatureModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class AViewModel : IViewModel
    {
        protected string _identifier = string.Empty;

        private bool _isDisposed = false;

        private bool _isVisible  = true;

        private Dictionary<Type, IFeatureModel> _models = new Dictionary<Type, IFeatureModel>();

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the view model id.
        /// </summary>
        public string id
        {
            get
            {
                return _identifier;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the view model is visible or not.
        /// </summary>
        public bool isVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                NotifyPropertyChanged( ref _isVisible, value );
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AViewModel"/> class.
        /// </summary>
        /// <param name="models">The set of model(s) needed for the view.</param>
        protected AViewModel(params IFeatureModel[] models)
        {
            for (int curr = 0; curr < models.Length; curr++)
            {
                IFeatureModel model = models[ curr ];
                Type modelType = model.GetType();
                _models[ modelType ] = model;
            }
        }

        public IFeatureModel GetModel(Type modelType)
        {
            IFeatureModel model = null;
            if ( _models.TryGetValue( modelType, out model ) )
            {
                return model;
            }

            return null;
        }

        public T GetModel<T>() where T : class, IFeatureModel
        {
            Type modelType = typeof(T);
            return GetModel( modelType ) as T;
        }

        /// <summary>
        /// Notifies that a property has changed after being sure the new value is different
        /// from the current one.
        /// TO DO: Add [CallerMemberName] as parameter attribute to auto detect the propertyname argument (Only available in .Net4.5)
        /// </summary>
        /// <param name="currentValue">The current value becoming the Old one</param>
        /// <param name="newValue">The new value being set.</param>
        /// <param name="propertyName">The changed property's name</param>
        /// <returns>True if updated, false if was already the same value.</returns>
        protected bool NotifyPropertyChanged<T>(ref T currentValue, T newValue, [CallerMemberName]string propertyName = null)
        {
            if ( newValue != null && 
                 newValue.Equals( currentValue ) )
            {
                return false;
            }

            currentValue = newValue;

            this.RaisePropertyChanged( propertyName );

            return true;
        }

        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        protected virtual void RaisePropertyChanged([CallerMemberName]string propertyName = null)
        {
            if ( this.PropertyChanged != null )
            {
                this.PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
            }
        }

        public void Dispose()
        {
            if ( _isDisposed == false )
            {
                OnDispose();

                _isDisposed = true;
            }
        }

        protected virtual void OnDispose()
        {

        }
    }
}
