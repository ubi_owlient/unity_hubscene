﻿namespace FSM.Hub.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MOTDSettingState : ASettingState
    {
        public MOTDSettingState()
        {
            _stateName = "MOTD setting";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }
        
        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
