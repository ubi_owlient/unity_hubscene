﻿using FeatureModels;
using Models;
using System.Collections.Generic;
using Utils;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class StoreService : AFeatureService
    {
        private StoreModel _storeModel = null;

        private int _offerCount = 10;
        private IList<string> _imageNames = null;
        private IList<string> _backgroundNames = null;

        private List<string> _defaultTitle = new List<string>() { "Offense gear bundle", "Specialist gear bundle", "Defense gear bundle", "Personal Training", "Campaign energy boost", "Arena energy boost", "Pile of cash", "Rack of cash", "Case of cash" };
        private List<string> _defaultDescription = new List<string>() { "Upgrade the gear grade of your offensive soldiers with this pack",
                                                                        "Upgrade the gear grade of your specialist soldiers with this pack",
                                                                        "Upgrade the gear grade of your defensive soldiers with this pack",
                                                                        "Some training materials",
                                                                        "Fill up on campaign energy & auto-win tokens",
                                                                        "Fill up on arena energy",
                                                                        "Get a pile of cash",
                                                                        "Get a rack of cash",
                                                                        "Get a case of cash" };
        private List<string> _defaultCategories  = new List<string>() { Constants.BUNDLE_CATEGORY,
                                                                        Constants.BUNDLE_CATEGORY,
                                                                        Constants.BUNDLE_CATEGORY,
                                                                        Constants.RESOURCES_CATEGORY,
                                                                        Constants.BUNDLE_CATEGORY,
                                                                        Constants.BUNDLE_CATEGORY,
                                                                        Constants.RESOURCES_CATEGORY,
                                                                        Constants.RESOURCES_CATEGORY,
                                                                        Constants.RESOURCES_CATEGORY
                                                                      };
        private List<Price> _defaultPrices       = new List<Price>() { new Price(   800, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(   800, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(   800, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(   100, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(    75, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(    60, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(   180, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(   875, (int)CurrencyHelper.GOLD_CURRENCY_ID ),
                                                                       new Price(  1800, (int)CurrencyHelper.GOLD_CURRENCY_ID ) };

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreService"/> class.
        /// </summary>
        public StoreService()
        {

        }
        
        public void Initialize(int offerCount, IEnumerable<string> imageNames, IEnumerable<string> backgroundNames)
        {
            _storeModel = this.GetModel<StoreModel>();

            _imageNames = new List<string>( imageNames );
            _backgroundNames = new List<string>( backgroundNames );

            _offerCount = offerCount;

            int titleIndex = 0;
            int imageIndex = 0;
            int backgroundIndex = 0;
            int imageCount = _imageNames.Count;
            int backgroundCount = _backgroundNames.Count;
            for (uint curr = 0; curr < _offerCount; curr++)
            {
                string offerTitle = _defaultTitle[ titleIndex ];
                string OfferDescription = _defaultDescription[ titleIndex ];

                _storeModel.AddOffer( new StoreOffer( curr, 
                                                      offerTitle, 
                                                      OfferDescription, 
                                                      _imageNames[ imageIndex ],
                                                      _backgroundNames[ backgroundIndex ],
                                                      _defaultCategories[ titleIndex ],
                                                      _defaultPrices[ titleIndex ] ) );

                imageIndex++;
                titleIndex++;
                backgroundIndex++;

                if ( imageIndex >= imageCount )
                {
                    imageIndex = 0;
                }

                if ( titleIndex >= 6 )
                {
                    titleIndex = 0;
                }

                if ( backgroundIndex >= backgroundCount )
                {
                    backgroundIndex = 0;
                }
            }
        }
    }
}
