﻿using Models;
using System.Collections.Generic;
using Utils;

namespace FeatureModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class StoreModel : AFeatureModel
    {
        private IEnumerable<StoreOffer> _emptyList = new StoreOffer[ 0 ];

        private Dictionary<uint, StoreOffer> _offersById = new Dictionary<uint, StoreOffer>();
        private Dictionary<string, List<StoreOffer>> _offersByCategory = new Dictionary<string, List<StoreOffer>>();
        
        public StoreOffer this[uint id]
        {
            get
            {
                StoreOffer offer = null;
                if ( _offersById.TryGetValue( id, out offer ) )
                {
                    return offer;
                }

                return null;
            }
        }

        public StoreOffer GetOffer(uint id)
        {
            return this[ id ];
        }

        public StoreModel()
        {
            _identifier = Constants.STORE_FEATURE;
        }

        public IEnumerable<StoreOffer> GetCategory(string category)
        {
            List<StoreOffer> offers = null;
            if ( _offersByCategory.TryGetValue( category, out offers ) )
            {
                return offers;
            }

            return _emptyList;
        }

        public void AddOffer(StoreOffer offer)
        {
            _offersById.Add(offer.offerId, offer);

            List<StoreOffer> offers = null;
            if ( _offersByCategory.TryGetValue( offer.category, out offers ) == false )
            {
                offers = new List<StoreOffer>();
                _offersByCategory[ offer.category ] = offers;
            }

            offers.Add( offer );
        }

        public void RemoveOffer(StoreOffer offer)
        {
            _offersById.Remove( offer.offerId );

            List<StoreOffer> offers = null;
            if ( _offersByCategory.TryGetValue( offer.category, out offers ) )
            {
                offers.Remove( offer );
            }
        }
    }
}
