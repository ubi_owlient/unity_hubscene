﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MailManager : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<MailService>().Initialize();
        }
    }
}
