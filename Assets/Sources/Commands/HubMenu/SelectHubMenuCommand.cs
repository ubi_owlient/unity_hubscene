﻿using Enums;
using UnityEngine;
using ViewModels;

namespace Commands.HubMenu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SelectHubMenuCommand : ACommand
    {
        public ActivityEnum activity
        {
            get;
            private set;
        }

        public Vector3 hubPosition
        {
            get;
            private set;
        }

        public Vector3 hubLookAt
        {
            get;
            private set;
        }

        public SelectHubMenuCommand(IModelPresenter sender, ActivityEnum activity, Vector3 hubPosition, Vector3 hubLookAt) :
        base(sender)
        {
            this.activity    = activity;
            this.hubPosition = hubPosition;
            this.hubLookAt   = hubLookAt;
        }
    }
}
