﻿using Models;
using System.Collections.Generic;
using Utils;

namespace FeatureModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class GachaModel : AFeatureModel
    {
        private Dictionary<uint, GachaOffer> _offersById = new Dictionary<uint, GachaOffer>();

        public GachaModel()
        {
            _identifier = Constants.GACHA_FEATURE;
        }

        public GachaOffer this[uint id]
        {
            get
            {
                GachaOffer offer = null;
                if ( _offersById.TryGetValue( id, out offer ) )
                {
                    return offer;
                }

                return null;
            }
        }

        public GachaOffer GetOffer(uint id)
        {
            return this[ id ];
        }
        
        public void AddOffer(GachaOffer offer)
        {
            _offersById.Add(offer.offerId, offer);
        }

        public void RemoveOffer(GachaOffer offer)
        {
            _offersById.Remove( offer.offerId );
        }
    }
}
