﻿namespace FSM.Hub.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ChatSettingState : ASettingState
    {
        public ChatSettingState()
        {
            _stateName = "Chat setting";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }
        
        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
