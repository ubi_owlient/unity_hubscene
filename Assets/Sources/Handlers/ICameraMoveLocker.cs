﻿namespace Handlers
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface ICameraMoveLocker
    {
        bool isLocked
        {
            get;
        }

        void LockCameraMovement();

        void UnlockCameraMovement();
    }
}
