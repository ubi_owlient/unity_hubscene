﻿using Handlers;
using UnityEditor;

namespace CustomEditors
{
    [CustomEditor(typeof(CinemachineMouseHandler))]
    public class CinemachineMouseHandlerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            CinemachineMouseHandler script = (CinemachineMouseHandler)target;
            DrawDefaultInspector();
            System.Type type = typeof(ICameraMoveLocker);
            //script.SetLocker( EditorGUILayout.ObjectField("Locker", script.GetLocker() as UnityEngine.Object, type, true ) as ICameraMoveLocker );
        }
    }
}
