﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class StoreOffer : AOffer
    {
        private string _category         = null;
        
        public string category
        {
            get
            {
                return _category;
            }
        }
        
        public StoreOffer(uint offerId, string offerTitle, string offerDesc, string offerImage, string offerBackground, string category, Price price, int frequency = -1) :
        base( offerId, offerTitle, offerDesc, offerImage, offerBackground, price, frequency )
        {
            _category = category;
        }
    }
}
