﻿using Service;
using System.Collections.Generic;
using UnityEngine;

namespace Animations
{
    public class PlayOnEvent : Receiver
    {
        public Animator anim;
        public Animation anim2;
        public EventAnim[] actions;
        public bool doSaving;
        public string playerPrefName;
        public bool doSingleSaving;
        public bool activeObjectIfNotActive = true;

        [System.Serializable]
        public struct EventAnim
        {
            public string inEventName;
            public string animName;
        };

        List<string> animToForce = new List<string>();
        List<string> animToPlay = new List<string>();


        // Use this for initialization
        void Start()
        {

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            if ((anim == null) && (anim2 == null))
            {
                anim = GetComponent<Animator>();
                anim2 = GetComponent<Animation>();
            }

            for (int i = 0; i < actions.Length; ++i)
            {
                actions[i].inEventName = animService.PrepareEventName(actions[i].inEventName, gameObject);
                if (actions[i].inEventName.Length == 0)
                {
                    animToPlay.Add(actions[i].animName);
                }
                else
                {
                    animService.Register(actions[i].inEventName, this);
                }
            }

        }
        void Update()
        {
            if ((animToForce != null) && (animToForce.Count > 0))
            {
                //Debug.Log("FORCE PLAYING ANIMATION " + animToForce[0]);
                if (anim != null)
                {
                    anim.enabled = true;
                    anim.Play(animToForce[0], -1, 1.0f);
                }
                if (anim2 != null)
                {
                    anim2.enabled = true;
                    anim2.Play(animToForce[0]);
                    anim2[animToForce[0]].normalizedTime = 1.0f;
                }

                animToForce.RemoveAt(0);
            }
            if (animToPlay.Count > 0)
            {
                Play(animToPlay[0]);
                animToPlay.RemoveAt(0);
            }
        }
        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < actions.Length; ++i)
            {
                animService.Unregister(actions[i].inEventName, this);
            }
        }

        void Play(string animName)
        {
            if (anim != null)
            {
                if (!anim.gameObject.activeSelf)
                {
                    if (activeObjectIfNotActive)
                    {
                        anim.gameObject.SetActive(true);
                    }
                }
                if (anim.gameObject.activeInHierarchy)
                {
                    if (animName.Equals("-"))
                    {
                        anim.enabled = false;
                    }
                    else
                    {
                        anim.enabled = true;
                        anim.Play(animName, -1, 0);
                    }
                }
#if LOG_MOTION_EVENTS
                else
                {
                    Debug.LogWarningFormat("Anim <{0}> has not been played because gameObject {1} was not active in hierarchy", animName, anim.gameObject.name);
                }
#endif
            }
            if (anim2 != null)
            {
                if (!anim2.gameObject.activeSelf)
                {
                    if (activeObjectIfNotActive)
                    {
                        anim2.gameObject.SetActive(true);
                    }
                }
                if (anim2.gameObject.activeInHierarchy)
                {
                    if (animName.Equals("-"))
                    {
                        anim2.enabled = false;
                    }
                    else
                    {
                        anim2.enabled = true;
                        anim2.Play(animName);
                        anim2[animName].normalizedTime = 0.0f;
                    }
                }
            }


        }
        public override void Receive(string eventName, GameObject obj)
        {
            for (int i = 0; i < actions.Length; ++i)
            {
                EventAnim cur = actions[i];
                if (eventName.Equals(cur.inEventName))
                {
                    Play(cur.animName);
                    break;
                }
            }
        }
    }
}