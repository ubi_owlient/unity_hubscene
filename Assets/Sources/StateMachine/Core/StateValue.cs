﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSM
{
    /// <summary>
    /// Definition of the <see cref="StateValue{T}"/> class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StateValue<T>
    {
        // Do not access this value from states - would normally be private if I could declare friendship
        internal T valueToBeAccessedByStateMachineOnly;

        public T Value
        {
            get
            {
                return valueToBeAccessedByStateMachineOnly;
            }
        }

        public StateValue()
        {

        }

        public StateValue(T aInitialValue)
        {
            valueToBeAccessedByStateMachineOnly = aInitialValue;
        }

        // Use to read value of StateValue

        public static implicit operator T(StateValue<T> aStateValue)
        {
            return aStateValue.Value;
        }
    }
}
