﻿using Service;
using System.Collections;
using UnityEngine;

namespace Animations
{
    public class AnimatorManager : AnimManager
    {
        private Animator _animator;
        private sUnityAnimation _currentAnimation;

        [System.Serializable]
        public struct sUnityAnimation
        {
            public Transform targetObject;
            public string inStartEvent;
            public string inOffEvent;
            public string[] outEndEvents;
        };

        public sUnityAnimation[] unityAnimations;

        protected override void Start()
        {
            _animator = GetComponent<Animator>();
            activAnimation = new ArrayList();

            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < unityAnimations.Length; ++i)
            {
                if (unityAnimations[i].targetObject == null)
                {
                    unityAnimations[i].targetObject = transform;
                }

                unityAnimations[i].inStartEvent = animService.PrepareEventName(unityAnimations[i].inStartEvent, unityAnimations[i].targetObject.gameObject);
                unityAnimations[i].inOffEvent   = animService.PrepareEventName(unityAnimations[i].inOffEvent, unityAnimations[i].targetObject.gameObject);
                
                animService.Register(unityAnimations[i].inStartEvent, this);
                animService.Register(unityAnimations[i].inOffEvent, this);

                if (unityAnimations[i].inStartEvent.Length == 0)
                {

                    addAnim(i);
                }
            }
        }

        protected override void Update() { }

        public override void Receive(string eventName, GameObject obj)
        {
            //Debug.Log("BUBBLE RECEIVE : "+ eventName);

            for (int i = 0; i < unityAnimations.Length; ++i)
            {
                if (eventName.Equals(unityAnimations[i].inStartEvent))
                {
                    addAnim(i);
                }
            }

            for (int i = 0; i < activAnimation.Count; ++i)
            {
                sUnityAnimation curAnim = (sUnityAnimation)activAnimation[i];
                if (eventName.Equals(curAnim.inOffEvent))
                {
                    activAnimation.RemoveAt(i);
                    --i;
                }
            }
        }

        protected override void addAnim(int id)
        {
            sUnityAnimation curAnim = new sUnityAnimation();
            _currentAnimation = curAnim = unityAnimations[id];
            activAnimation.Add(curAnim);

            _animator.SetTrigger(unityAnimations[id].inStartEvent);
            //Debug.Log("SETTRIGGER : "+unityAnimations[id].inStartEvent);
        }

        #region animatorEvent
        public void OnAnimationEnd()
        {
            ServiceManager.Instance.GetService<AnimationService>().SendEvent(_currentAnimation.outEndEvents, gameObject);
        }
        #endregion
    }
}