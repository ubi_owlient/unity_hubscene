﻿using Service;
using UnityEngine;
using UnityEngine.UI;

namespace Animations
{
    public class DebugSendEvent : MonoBehaviour
    {
        public InputField text;

        public void FDebugSendEvent()
        {
            ServiceManager.Instance.GetService<AnimationService>().SendEvent(text.text);
        }
    }
}