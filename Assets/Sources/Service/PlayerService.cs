﻿using Characters;
using UnityEngine;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class PlayerService : AFeatureService
    {
        private GameObject _player = null;
        
        public GameObject player
        {
            get
            {
                return _player;
            }
        }

        public PointAndClickPlayerController controller
        {
            get
            {
                if ( player != null )
                {
                    return player.GetComponent<PointAndClickPlayerController>();
                }

                return null;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerService"/> class.
        /// </summary>
        public PlayerService()
        {

        }

        public void Initialize(GameObject player)
        {
            _player = player;
        }
    }
}
