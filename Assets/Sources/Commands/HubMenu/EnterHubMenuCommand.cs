﻿using ViewModels;

namespace Commands.HubMenu
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class EnterHubMenuCommand : ACommand
    {
        public EnterHubMenuCommand(IModelPresenter sender) :
        base(sender)
        {
            
        }
    }
}
