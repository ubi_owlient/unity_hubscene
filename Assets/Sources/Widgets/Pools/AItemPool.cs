﻿using System;
using UnityEngine;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi.
    /// </summary>
    public abstract class AItemPool : MonoBehaviour
    {
        [SerializeField]
        protected GameObject _itemTree   = null;

        [SerializeField]
        protected GameObject _itemPrefab = null;

        public event Action<AItemPool, object> onClick = null;

        /// <summary>
        /// Gets the item count.
        /// </summary>
        public abstract uint itemCount
        {
            get;
        }

        /// <summary>
        /// Removes an item.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public abstract bool RemoveItem(uint index);

        /// <summary>
        /// Cleans up.
        /// </summary>
        public abstract void CleanUp();

        protected void NotifyItemClicked(object item)
        {
            if ( onClick != null )
            {
                onClick( this, item );
            }
        }
    }
}
