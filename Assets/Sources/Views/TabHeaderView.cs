﻿using Menu.Buttons;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class TabHeaderView : MonoBehaviour
    {
        [SerializeField] private TabButton _button  = null;

        [SerializeField] private Image _selectImage = null;

        public event Action<TabHeaderView, ABaseButton> onTabClick = null;

        void Start()
        {
            _button.onClick += OnTabClick;
        }

        void OnDestroy()
        {
            _button.onClick -= OnTabClick;
        }

        private void OnTabClick(ABaseButton sender)
        {
            if ( onTabClick != null )
            {
                onTabClick( this, sender );
            }
        }

        public void RefreshTitle(string title)
        {
            if ( _button != null )
            {
                _button.RefreshTitle( title );
            }
        }

        public void Select()
        {
            if ( _selectImage != null )
            {
                _selectImage.color = Color.cyan;
            }
        }

        public void Unselect()
        {
            if ( _selectImage != null )
            {
                _selectImage.color = Color.grey;
            }
        }
    }
}
