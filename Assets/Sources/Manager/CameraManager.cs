﻿using Cinemachine;
using Service;
using UnityEngine;
using Utils;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CameraManager : MonoBehaviour
    {
        [SerializeField]
        private Camera _camera = null;

        [SerializeField]
        private CinemachineStateDrivenCamera _playerHubCamera = null;
        
        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<CameraService>().Initialize( _camera );
            ServiceManager.Instance.GetService<CameraService>().AddCamera( Constants.HUB_CAMERA_ID, _playerHubCamera );
        }

        public void OnVirtualCameraChanged(ICinemachineCamera oldCam, ICinemachineCamera newCam)
        {
            ServiceManager.Instance.GetService<CameraService>().OnVirtualCameraChanged( oldCam, newCam );
        }
    }
}
