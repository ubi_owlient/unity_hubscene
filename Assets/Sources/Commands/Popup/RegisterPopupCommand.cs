﻿using ViewModels;

namespace Commands.Popup
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class RegisterPopupCommand : ACommand
    {
        public RegisterPopupCommand(IModelPresenter sender) :
        base(sender)
        {

        }
    }
}
