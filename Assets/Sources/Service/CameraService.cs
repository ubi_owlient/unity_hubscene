﻿using Cinemachine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class CameraService : AFeatureService
    {
        private Camera _camera = null;

        private bool _isCameraMoveLocked = false;

        private Dictionary<string, object> _camerasByName = new Dictionary<string, object>();
        
        public Action<ICinemachineCamera, ICinemachineCamera> onCameraChanged = null;

        public Camera MainCamera
        {
            get
            {
                return _camera;
            }
        }

        public bool isCameraMoveLocked
        {
            get
            {
                return _isCameraMoveLocked;
            }
        }

        /// <summary>
        /// Gets the camera having the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public object this[string id]
        {
            get
            {
                return GetCameraById( id );
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CameraService"/> class.
        /// </summary>
        public CameraService()
        {

        }
        
        public void Initialize(Camera mainCamera)
        {
            _camera = mainCamera;
        }

        /// <summary>
        /// Adds a new camera
        /// </summary>
        /// <param name="id"></param>
        /// <param name="camera"></param>
        public void AddCamera(string id, ICinemachineCamera camera)
        {
            _camerasByName[ id ] = camera;
        }

        /// <summary>
        /// Adds a new camera.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="camera"></param>
        public void AddCamera(string id, Camera camera)
        {
            _camerasByName[ id ] = camera;
        }

        /// <summary>
        /// Gets a camera by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public object GetCameraById(string id)
        {
            object camera = null;
            if ( _camerasByName.TryGetValue( id, out camera ) )
            {
                return camera;
            }

            return null;
        }

        /// <summary>
        /// Gets a camera by its id.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetCameraById<T>(string id) where T : class
        {
            return GetCameraById( id ) as T;
        }

        public bool EnableCamera(string id)
        {
            object camera = null;
            if ( _camerasByName.TryGetValue( id, out camera ) )
            {
                //if ( camera is Camera )
                //{
                //    (camera as Camera).enabled = true;
                //}
                //else if ( camera is CinemachineVirtualCameraBase )
                //{
                //    (camera as CinemachineVirtualCameraBase).enabled = true;
                //}

                if ( camera is Behaviour )
                {
                    (camera as Behaviour).enabled = true;
                }

                return true;
            }

            return false;
        }

        public bool DisableCamera(string id)
        {
            object camera = null;
            if ( _camerasByName.TryGetValue( id, out camera ) )
            {
                //if ( camera is Camera )
                //{
                //    (camera as Camera).enabled = false;
                //}
                //else if ( camera is CinemachineVirtualCameraBase )
                //{
                //    (camera as CinemachineVirtualCameraBase).enabled = false;
                //}

                if ( camera is Behaviour )
                {
                    (camera as Behaviour).enabled = false;
                }

                return true;
            }

            return false;
        }

        public void OnVirtualCameraChanged(ICinemachineCamera oldCam, ICinemachineCamera newCam)
        {
            if ( onCameraChanged != null )
            {
                onCameraChanged( oldCam, newCam );
            }
        }

        public void LockCameraMovement()
        {
            _isCameraMoveLocked = true;
        }

        public void UnlockCameraMovement()
        {
            _isCameraMoveLocked = false;
        }
    }
}
