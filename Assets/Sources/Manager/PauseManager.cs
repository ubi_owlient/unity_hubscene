﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class PauseManager : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<PauseService>().Initialize();
        }

        void Update()
        {
            if ( Input.GetKeyDown(KeyCode.P) )
            {
                PauseService pauseService = ServiceManager.Instance.GetService<PauseService>();
                if ( pauseService.isPaused )
                {
                    pauseService.Resume();
                }
                else
                {
                    pauseService.Pause();
                }
            }
        }
    }
}
