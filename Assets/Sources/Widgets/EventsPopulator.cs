﻿using Models;
using Service;
using System.Collections.Generic;
using Views;

namespace Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class EventsPopulator : AItemPopulator<EventTileView>
    {
        protected override void OnCustomInitialize()
        {
            Populate( ServiceManager.Instance.GetService<EventService>().events );
        }

        private void Populate(IEnumerable<EventModel> events)
        {
            uint count = 0;
            foreach ( EventModel eventModel in events )
            {
                EventTileView widget = pool.GetItem( count );
                widget.SetEventInfo( eventModel );
                count++;
            }
        }
    }
}
