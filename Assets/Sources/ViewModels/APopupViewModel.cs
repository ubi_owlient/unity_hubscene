﻿namespace ViewModels
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class APopupViewModel : AViewModel, IPopupViewModel
    {
        protected APopupViewModel(string popupId)
        {
            _identifier = popupId;
            isVisible   = false;
        }

        /// <summary>
        /// Shows the popup.
        /// </summary>
        public virtual void Show()
        {
            isVisible = true;
        }

        /// <summary>
        /// Hides the popup.
        /// </summary>
        public virtual void Hide()
        {
            isVisible = false;
        }
    }
}
