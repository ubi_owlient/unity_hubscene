﻿namespace FSM
{
    /// <summary>
    /// Definition of the <see cref="TransitionType"/> enumerations.
    /// </summary>
    public enum TransitionType
    {
        /// <summary>
        /// No transition
        /// </summary>
        None,

        /// <summary>
        /// Inner transition.
        /// </summary>
        Inner,

        /// <summary>
        /// Inner entry transition.
        /// </summary>
        InnerEntry,

        /// <summary>
        /// Sibling transition.
        /// </summary>
        Sibling
    };
}
