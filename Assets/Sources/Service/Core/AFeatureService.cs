﻿using FeatureModels;
using System;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class AFeatureService : IFeatureService
    {
        public IFeatureModel GetModel(Type modelType)
        {
            return ServiceManager.Instance.GetService<ModelService>().GetModel( modelType );
        }

        public T GetModel<T>() where T : class, IFeatureModel
        {
            Type modelType = typeof(T);
            return GetModel( modelType ) as T;
        }

        void IService.Initialize()
        {
            DispatcherService dispatcherService = ServiceManager.Instance.GetService<DispatcherService>();
            if ( dispatcherService != null )
            {
                dispatcherService.RegisterHandler( this );
            }

            OnCustomInitialize();
        }

        protected virtual void OnCustomInitialize()
        {

        }
    }
}
