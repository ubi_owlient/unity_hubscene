﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The soldier state.
    /// </summary>
    public class SoldierHubState : AHubState
    {
        public SoldierHubState()
        {
            _stateName = "Soldier hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);


        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);


        }

        public override void OnExit()
        {
            base.OnExit();


        }
    }
}
