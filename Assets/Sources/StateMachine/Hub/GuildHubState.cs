﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The Guild state.
    /// </summary>
    public class GuildHubState : AHubState
    {
        public GuildHubState()
        {
            _stateName = "Guild hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);


        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);


        }

        public override void OnExit()
        {
            base.OnExit();


        }
    }
}
