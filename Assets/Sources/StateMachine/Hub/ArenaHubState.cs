﻿namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// The Arena state.
    /// </summary>
    public class ArenaHubState : AHubState
    {
        public ArenaHubState()
        {
            _stateName = "Arena hub";
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            
        }

        public override void OnExit()
        {
            base.OnExit();

            
        }
    }
}
