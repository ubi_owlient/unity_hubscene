﻿using Cinemachine;
using Service;
using UnityEngine;

namespace Handlers
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class CinemachineMouseHandler : MonoBehaviour
    {
        private const string MOUSE_X_AXIS_NAME = "Mouse X";
        private const string MOUSE_Y_AXIS_NAME = "Mouse Y";

        [SerializeField]
        private bool _isMouseLeft = true;
        
        private int buttonKey = 0; // Left key code.

        void Start()
        {
            if ( _isMouseLeft )
            {
                buttonKey = 0;
            }
            else
            {
                buttonKey = 1;
            }
            
            CinemachineCore.GetInputAxis = GetAxisCustom;
        }

        /// <summary>
        /// Override cinemachine axis values.
        /// </summary>
        /// <param name="axisName"></param>
        /// <returns></returns>
        public float GetAxisCustom(string axisName)
        {
            if ( ServiceManager.Instance.GetService<CameraService>().isCameraMoveLocked )
            {
                return 0;
            }

            if (axisName == MOUSE_X_AXIS_NAME )
            {
                if (Input.GetMouseButton(buttonKey))
                {
                    return Input.GetAxis(MOUSE_X_AXIS_NAME);
                }
                else
                {
                    return 0;
                }
            }
            else if (axisName == MOUSE_Y_AXIS_NAME)
            {
                if (Input.GetMouseButton(buttonKey))
                {
                    return Input.GetAxis(MOUSE_Y_AXIS_NAME);
                }
                else
                {
                    return 0;
                }
            }
            return Input.GetAxis(axisName);
        }
    }
}
