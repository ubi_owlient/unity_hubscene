﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class TutorialPart
    {
        private string _id = string.Empty;

        private string _image = string.Empty;

        private string _explanation = string.Empty;

        public string id
        {
            get
            {
                return _id;
            }
        }

        public string image
        {
            get
            {
                return _image;
            }
        }

        public string explanation
        {
            get
            {
                return _explanation;
            }
        }

        public TutorialPart(string id, string explanation, string image)
        {
            _id = id;
            _explanation = explanation;
            _image = image;
        }
    }
}
