﻿using Service;
using UnityEngine;
using UnityEngine.UI;

namespace Animations
{
    public class CanvasToggleSendEvent : MonoBehaviour
    {
        public string outEventOn;
        public string outEventOff;

        private Toggle toggle;

        void Start()
        {
            toggle = GetComponent<Toggle>();
            toggle.onValueChanged.AddListener(ToggleChanged);
        }

        public void ToggleChanged(bool value)
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            if (value)
            {
                animService.SendEvent(outEventOn, gameObject);
            }
            else
            {
                animService.SendEvent(outEventOff, gameObject);
            }
        }
    }
}