﻿using ViewModels;

namespace Commands.Settings
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class UnselectSettingOptionCommand : SelectSettingOptionCommand
    {
        public UnselectSettingOptionCommand(IModelPresenter sender, string settingId) : 
        base(sender, settingId)
        {

        }
    }
}
