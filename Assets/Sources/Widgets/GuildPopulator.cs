﻿using Models;
using Service;
using System.Collections.Generic;
using Views;
using Widgets;

namespace Assets.Sources.Widgets
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class GuildPopulator : AItemPopulator<GuildTileView>
    {
        protected override void OnCustomInitialize()
        {
            ServiceManager.Instance.GetService<GuildService>().guildCreated += OnGuildCreated;
            
            Populate( ServiceManager.Instance.GetService<GuildService>().guilds );
        }
        
        private void Populate(IEnumerable<GuildModel> guilds)
        {
            uint count = 0;
            foreach ( GuildModel guild in guilds )
            {
                GuildTileView widget = pool.GetItem( count );
                widget.SetGuildInfo( guild );
                count++;
            }
        }

        private void OnGuildCreated(GuildService sender, GuildModel guild)
        {
            GuildTileView widget = pool.Next();
            widget.SetGuildInfo( guild );
        }
    }
}
