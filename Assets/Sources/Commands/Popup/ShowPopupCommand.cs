﻿using ViewModels;

namespace Commands.Popup
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class ShowPopupCommand : ACommand
    {
        public string popupId
        {
            get;
            private set;
        }

        public ShowPopupCommand(IModelPresenter sender, string popupId) :
        base(sender)
        {
            this.popupId = popupId;
        }
    }
}
