﻿using Service;
using UnityEngine;

namespace Animations
{
    public class LocalCommunicatorWrapper : MonoBehaviour
    {
        public void SendEvent(string eventName)
        {
            ServiceManager.Instance.GetService<AnimationService>().SendEvent(eventName, gameObject);
        }
    }
}