﻿using Service;
using UnityEngine;

namespace Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SelectionManager : MonoBehaviour
    {
        //[SerializeField]
        //private Camera _camera = null;
        
        //[SerializeField]
        //private float _raycastDistance = 1000.0f;

        [SerializeField]
        private string[] _selectableTag = new string[0];

        private int _layerMask = Physics.DefaultRaycastLayers;
        private GameObject _player = null;
        
        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            //ServiceManager.Instance.GetService<SelectionService>().Initialize();

            _layerMask = LayerMask.GetMask(_selectableTag);
        }
        
        void Update()
        {
            // Checks whether excape is pressed.
            if ( Input.GetKeyDown("escape") )
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }

            // Left mouse pressed?
            if ( Input.GetMouseButtonDown( 0 ) )
            {
                if ( _player == null )
                {
                    _player = ServiceManager.Instance.GetService<PlayerService>().player;
                }

                //PointerEventData pointerData = new PointerEventData(EventSystem.current);
                //pointerData.position = Input.mousePosition;
                //List<RaycastResult> uiResults = new List<RaycastResult>();
                //EventSystem.current.RaycastAll(pointerData, uiResults);
                //if ( uiResults.Count > 0 )
                //{
                //    List<SelectionHandler> selectedHandlers = uiResults.Select( selection => selection.gameObject.GetComponent<SelectionHandler>() ).Where( handler => handler != null ).ToList();
                //    if ( selectedHandlers.Count > 0 )
                //    {
                //        // (VectorA-VectorB).sqrMagnitude is way faster
                //        List<SelectionHandler> orderedSelection = selectedHandlers.OrderBy( hit => ( hit.target.transform.position - _camera.transform.position ).sqrMagnitude ).ToList();
                //        if ( ServiceManager.Instance.GetService<SelectionService>().currentSelection == orderedSelection[ 0 ].target.transform )
                //        {
                //            // Already selected, check for another one behind it. User convenience and solve the hub menu overlap case.
                //            if ( orderedSelection.Count > 1 )
                //            {
                //                // Anything behind, select it.
                //                ServiceManager.Instance.GetService<SelectionService>().SetSelection( orderedSelection[ 1 ].target.transform );
                //            }
                //        }
                //        else
                //        {
                //            // Select it.
                //            ServiceManager.Instance.GetService<SelectionService>().SetSelection( orderedSelection[ 0 ].target.transform );
                //        }
                //    }
                //}

                //Ray ray = _camera.ScreenPointToRay( Input.mousePosition );
                //RaycastHit[] results = Physics.RaycastAll( ray, _raycastDistance, _layerMask );
                //if ( results != null && results.Length > 0 )
                //{
                //    // (VectorA-VectorB).sqrMagnitude is way faster
                //    List<RaycastHit> orderedSelection = results.OrderBy( hit => ( hit.transform.position - _camera.transform.position ).sqrMagnitude ).ToList();
                //    if ( ServiceManager.Instance.GetService<SelectionService>().currentSelection == orderedSelection[ 0 ].transform )
                //    {
                //        // Already selected, check for another one behind it. User convenience and solve the hub menu overlap case.
                //        if ( orderedSelection.Count > 1 )
                //        {
                //            // Anything behind, select it.
                //            ServiceManager.Instance.GetService<SelectionService>().SetSelection( orderedSelection[ 1 ].transform );
                //        }
                //    }
                //    else
                //    {
                //        // Select it.
                //        ServiceManager.Instance.GetService<SelectionService>().SetSelection( orderedSelection[ 0 ].transform );
                //    }
                //}
            }
        }
    }
}
