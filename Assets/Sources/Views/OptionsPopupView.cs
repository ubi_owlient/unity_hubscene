﻿using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class OptionsPopupView : ASettingsPopupView
    {
        public override string id
        {
            get
            {
                return Constants.OPTIONS_POPUP_ID;
            }
        }
    }
}
