﻿using Service;
using Types.Notifications;
using UnityEngine;

namespace Assets.Sources.Manager
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class NotificationsManager : MonoBehaviour
    {
        [SerializeField]
        private float _notificationDelay = 2.0f;

        private ANotificationDescription _notifTest = null;

        void Awake()
        {
            DontDestroyOnLoad( this );
        }

        void Start()
        {
            ServiceManager.Instance.GetService<NotificationService>().Initialize( _notificationDelay );

            _notifTest = new SimpleMessageNotificationDescription( "Notification test." );
        }

        void Update()
        {
            if ( Input.GetKeyDown(KeyCode.N) )
            {
                ServiceManager.Instance.GetService<NotificationService>().Notify( _notifTest );
            }
        }
    }
}
