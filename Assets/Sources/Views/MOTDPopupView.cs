﻿using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MOTDPopupView : ASettingsPopupView
    {
        public override string id
        {
            get
            {
                return Constants.MOTD_POPUP_ID;
            }
        }
    }
}
