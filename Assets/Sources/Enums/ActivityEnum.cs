﻿namespace Enums
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public enum ActivityEnum
    {
        NONE      = 0,
        CAMPAIGN  = 1,
        STORE     = 2,
        GACHA     = 3,
        EVENTS    = 4,
        DAILY_OPS = 5,
        GUILDS    = 6,
        ARENA     = 7,
        SOLDIERS  = 8,
    }
}
