﻿using UnityEngine;
using Widgets;

namespace Scriptable.Tab
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    [CreateAssetMenu(menuName = "Definitions/Tab")]
    public class TabDefinition : ScriptableObject
    {
        [SerializeField]
        private GameObject _tabHeaderPrefab  = null;

        [SerializeField]
        private GameObject _tabContentPrefab = null;

        public TabWidget CreateTab(uint index, GameObject headerParent, GameObject contentParent)
        {
            GameObject tabHeader  = Instantiate( _tabHeaderPrefab,  headerParent.transform );
            GameObject tabContent = Instantiate( _tabContentPrefab, contentParent.transform );

            return new TabWidget( index, tabHeader, tabContent );
        }
    }
}
