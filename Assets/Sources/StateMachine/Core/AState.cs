﻿using System.Collections.Generic;
using UnityEngine;

namespace FSM
{
    /// <summary>
    /// Definition of the base <see cref="AState"/> abstract class.
    /// </summary>
    public abstract class AState
    {
        private static int _indexCounter = 0;
        private int _id = 0;
        internal StateMachine _ownerStateMachine;
        internal List<AStateValueResetter> _stateValueResetters = null;
        internal int _stackDepth;

        public AState()
        {
            _id = _indexCounter++;
        }

        ///////////////////////////////
        // Overridables
        ///////////////////////////////

        public virtual void OnInitialize() { }

        public virtual void OnEnter() { }
        public virtual void OnEnter(object[] args) { }
        public virtual void OnExit() { }
        public virtual Transition GetTransition() { return Transition.None(); }
        public virtual void Update(float deltaTime) { }
        
        ///////////////////////////////
        // Accessors
        ///////////////////////////////

        public int id
        {
            get
            {
                return _id;
            }
        }

        public StateMachine StateMachine { get { return _ownerStateMachine; } }
        
        public StateType FindState<StateType>() where StateType : AState { return _ownerStateMachine.FindState<StateType>(); }
        public StateType GetState<StateType>() where StateType : AState { return _ownerStateMachine.GetState<StateType>(); }
        public bool IsInState<StateType>() where StateType : AState { return FindState<StateType>() != null; }

        public StateType FindOuterState<StateType>() where StateType : AState { return _ownerStateMachine.FindOuterStateFromDepth<StateType>(_stackDepth); }
        public StateType GetOuterState<StateType>() where StateType : AState
        {
            StateType result = FindOuterState<StateType>();
            Debug.Assert(result != null, string.Format("Failed to get outer state on stack: {0}", typeof(StateType)));
            return result;
        }
        public bool IsInOuterState<StateType>() where StateType : AState { return FindOuterState<StateType>() != null; }

        public StateType FindInnerState<StateType>() where StateType : AState { return _ownerStateMachine.FindInnerStateFromDepth<StateType>(_stackDepth); }
        public StateType GetInnerState<StateType>() where StateType : AState
        {
            StateType result = FindInnerState<StateType>();
            Debug.Assert(result != null, string.Format("Failed to get inner state on stack: {0}", typeof(StateType)));
            return result;
        }
        public bool IsInInnerState<StateType>() where StateType : AState { return FindInnerState<StateType>() != null; }

        public StateType FindImmediateInnerState<StateType>() where StateType : AState { return FindImmediateInnerState<StateType>(); }
        public StateType GetImmediateInnerState<StateType>() where StateType : AState
        {
            StateType result = FindImmediateInnerState<StateType>();
            Debug.Assert(result != null, string.Format("Failed to get immeidate inner state on stack: {0}", typeof(StateType)));
            return result;
        }
        public bool IsInImmediateInnerState<StateType>() where StateType : AState { return FindImmediateInnerState<StateType>() != null; }
        // Returns generic State, might be useful to query whether current state has an inner state at all
        public AState FindImmediateInnerState() { return _ownerStateMachine.FindStateAtDepth(_stackDepth + 1); }


        ///////////////////////////////
        // StateValues
        ///////////////////////////////
        
        // Use to set value-type StateValue
        public void SetStateValue<T>(StateValue<T> aStateValue, T aValue)
        {
            if (!IsStateValueInResetterList(aStateValue))
                _stateValueResetters.Add(new StateValueResetter<T>(aStateValue));

            aStateValue.valueToBeAccessedByStateMachineOnly = aValue;
        }

        internal void ResetAllStateValues()
        {
            if (_stateValueResetters == null)
                return;

            foreach (AStateValueResetter resetter in _stateValueResetters)
                resetter.Reset();

            _stateValueResetters.Clear();
        }

        private bool IsStateValueInResetterList<T>(StateValue<T> aStateValue)
        {
            if (_stateValueResetters == null) // First time, lazily create list
            {
                _stateValueResetters = new List<AStateValueResetter>();
            }
            else
            {
                foreach (AStateValueResetter resetter in _stateValueResetters)
                {
                    var r = resetter as StateValueResetter<T>;
                    if (r != null && r.StateValue == aStateValue)
                        return true;
                }
            }
            return false;
        }
    }
}
