﻿using Types.Notifications;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class SimpleNotificationWidget : MonoBehaviour
    {
        [SerializeField]
        private Text _message = null;

        [SerializeField]
        private Image _icon = null;

        public void SetNotification(SimpleMessageNotificationDescription description)
        {
            if ( description == null )
            {
                return;
            }

            if ( _message != null )
            {
                _message.text = description.message;
            }
        }
    }
}
