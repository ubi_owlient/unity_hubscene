﻿using ViewModels;

namespace Commands.Popup
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class UnregisterPopupCommand : ACommand
    {
        public UnregisterPopupCommand(IModelPresenter sender) :
        base(sender)
        {

        }
    }
}
