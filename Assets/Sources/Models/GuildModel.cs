﻿using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class GuildModel
    {
        private string        _guildName = null;
        private string        _iconName  = null;
        private string        _guildDescription  = null;
        private string        _motd      = null;
        private uint          _guildId   = 0;
        private HashSet<uint> _memberIds = new HashSet<uint>();

        public uint guildId
        {
            get
            {
                return _guildId;
            }
        }

        public string guildName
        {
            get
            {
                return _guildName;
            }
        }

        public string iconName
        {
            get
            {
                return _iconName;
            }
        }

        public string guildDescription
        {
            get
            {
                return _guildDescription;
            }
        }

        public string motd
        {
            get
            {
                return _motd;
            }
        }

        public int memberCount
        {
            get
            {
                return _memberIds.Count;
            }
        }

        public IEnumerable<uint> members
        {
            get
            {
                return _memberIds;
            }
        }

        public GuildModel(uint guildId, string guildName, string iconName, string guildDescription, string motd)
        {
            _guildId   = guildId;
            _guildName = guildName;
            _iconName  = iconName;
            _guildDescription = guildDescription;
            _motd = motd;
        }

        public void AddMember(uint memberId)
        {
            _memberIds.Add( memberId );
        }

        public void Removemember(uint memberId)
        {
            _memberIds.Remove( memberId );
        }
    }
}
