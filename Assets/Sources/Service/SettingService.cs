﻿using Commands;
using Commands.Settings;
using System;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class SettingService : AFeatureService
    {
        private bool _enableAnim = true;
        private bool _isSettingContentOpened = false;
        
        /// <summary>
        /// Gets the flag indicating whether any settings content has been opened or not.
        /// </summary>
        public bool isSettingContentOpened
        {
            get
            {
                return _isSettingContentOpened;
            }
        }

        public bool AnimationEnabled
        {
            get
            {
                return _enableAnim;
            }
        }
        
        public Action<SettingService, bool> onAnimationEnabled = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingService"/> class.
        /// </summary>
        public SettingService()
        {

        }

        [DispatcherService.HandleCommand(commandType = typeof(SelectSettingOptionCommand))]
        public void EnterSettingContent(ICommand command)
        {
            _isSettingContentOpened = true;
        }

        [DispatcherService.HandleCommand(commandType = typeof(UnselectSettingOptionCommand))]
        public void ExitSettingContent(ICommand command)
        {
            _isSettingContentOpened = false;
        }
        
        public void SetAnimationState(bool enable)
        {
            _enableAnim = enable;

            NotifyAnimationEnabled( _enableAnim );
        }

        private void NotifyAnimationEnabled(bool enable)
        {
            if ( onAnimationEnabled != null )
            {
                onAnimationEnabled( this, enable );
            }
        }
    }
}
