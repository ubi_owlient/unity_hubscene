﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class ActivityStep
    {
        private ActivityModel _owner       = null;
        private string _stepTitle;
        private string _stepDescription;
        private string _stepMenuPrefab = string.Empty;

        public ActivityModel owner
        {
            get
            {
                return _owner;
            }
            internal set
            {
                _owner = value;
            }
        }

        public string title
        {
            get
            {
                return _stepTitle;
            }
        }

        public string description
        {
            get
            {
                return _stepDescription;
            }
        }

        public string activityMenuPrefab
        {
            get
            {
                return _stepMenuPrefab;
            }
        }

        public ActivityStep(string stepTitle, string stepDescription, string stepMenuPrefab)
        {
            _stepTitle = stepTitle;
            _stepDescription = stepDescription;
            _stepMenuPrefab = stepMenuPrefab;
        }
    }
}
