﻿namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IService
    {
        void Initialize();
    }
}
