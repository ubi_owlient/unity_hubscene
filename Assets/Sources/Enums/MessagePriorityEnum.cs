﻿namespace Enums
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public enum MessagePriorityEnum
    {
        NONE = 0,

        MINIMAL,

        LOW,

        NORMAL,

        HIGH,

        MAXIMAL,
    }
}
