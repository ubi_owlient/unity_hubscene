﻿namespace Types.Notifications
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class ANotificationDescription
    {
        private float? _notificationDelay = null;

        public float? notificationDelay
        {
            get
            {
                return _notificationDelay;
            }
            set
            {
                _notificationDelay = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ANotificationDescription"/> class.
        /// </summary>
        /// <param name="notificationDelay">A custom delay the notif will remain visible.</param>
        protected ANotificationDescription(float? notificationDelay = null)
        {
            _notificationDelay = notificationDelay;
        }
    }
}
