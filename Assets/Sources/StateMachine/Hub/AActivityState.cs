﻿using Commands;
using Commands.HubMenu;
using FeatureModels;
using Models;
using Service;
using Utils;

namespace FSM.Hub
{
    /// <summary>
    /// Author: jlarbi
    /// 
    /// Definition of the base <see cref="AActivityState"/> abstract class.
    /// </summary>
    public abstract class AActivityState : ABaseState
    {
        protected ActivityModel _hubMenu = null;

        protected ActivityStep  _subMenu = null;
        
        private bool                _requireExit      = false;
        private bool                _requireBack      = false;

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);
            
            _hubMenu = args[ 0 ] as ActivityModel;
            _subMenu = args[ 1 ] as ActivityStep;

            CameraService cameraService = Owner.GetService<CameraService>();

            if ( _hubMenu != null )
            {
                //_hubMenu.isOpen = false;
                
                // Orientate camera.
                cameraService.MainCamera.transform.LookAt( _hubMenu.hubLookAt );
            }
            
            cameraService.LockCameraMovement();
            cameraService.DisableCamera( Constants.HUB_CAMERA_ID );
        }
        
        public override void OnExit()
        {
            base.OnExit();
            
            //if ( _hubMenu != null )
            //{
            //    _hubMenu.isOpen = true;
            //}

            CameraService cameraService = Owner.GetService<CameraService>();
            cameraService.UnlockCameraMovement();
            cameraService.EnableCamera( Constants.HUB_CAMERA_ID );

            _requireBack = false;
            _requireExit = false;
        }

        [DispatcherService.HandleCommand(commandType = typeof(HomeMenuCommand))]
        protected void OnHomeClicked(ICommand command)
        {
            _requireExit = true;

            HubModel hubModel = GetModel<HubModel>();
            if ( hubModel != null )
            {
                hubModel.currentStep = null;
            }
        }

        [DispatcherService.HandleCommand(commandType = typeof(BackMenuCommand))]
        protected void OnBackClicked(ICommand command)
        {
            _requireBack = true;

            OnBackClicked();
        }

        protected virtual void OnBackClicked()
        {

        }

        public override Transition GetTransition()
        {
            if ( _requireBack )
            {
                return OnRestore();
            }
            else if ( _requireExit )
            {
                return OnQuit();
            }

            return base.GetTransition();
        }

        protected virtual Transition OnRestore()
        {
            return Transition.None();
        }

        protected virtual Transition OnQuit()
        {
            return Transition.None();
        }
    }
}
