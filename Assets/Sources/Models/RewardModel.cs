﻿namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class RewardModel
    {
        private uint   _id      = 0;
        private string _name    = null;
        private string _image   = null;
        private uint   _amount  = 0;

        public uint id
        {
            get
            {
                return _id;
            }
        }

        public string name
        {
            get
            {
                return _name;
            }
        }

        public string image
        {
            get
            {
                return _image;
            }
        }

        public uint amount
        {
            get
            {
                return _amount;
            }
        }

        public RewardModel(uint id, string name, string image, uint amount)
        {
            _id = id;
            _name = name;
            _image = image;
            _amount = amount;
        }
    }
}
