﻿using Service;
using UnityEngine;

namespace Handlers
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class StateChangedHandler : MonoBehaviour
    {
        private void OnApplicationStateChanged(int state)
        {
            ServiceManager.Instance.GetService<AnimationService>().SendDelayedEvent( string.Format( "EvStateChanged_{0}", "" ) );
        }
    }
}
