﻿using Service;
using UnityEngine;
using UnityEngine.UI;

namespace Animations
{
    public class ChangeSpriteFromTagOnEvent : Receiver
    {

        [System.Serializable]
        public struct TagSprite
        {
            public string tag;
            public Sprite sprite;
            public Color color;
        };

        [System.Serializable]
        public struct TagEvent
        {
            public string inEvent;
            public TagSprite[] tagInfo;
        };

        public TagEvent[] events;

        void Start()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < events.Length; ++i)
            {
                events[i].inEvent = animService.PrepareEventName(events[i].inEvent, gameObject);
                animService.Register(events[i].inEvent, this);
            }
        }

        void Stop()
        {
            AnimationService animService = ServiceManager.Instance.GetService<AnimationService>();
            for (int i = 0; i < events.Length; ++i)
            {
                animService.Unregister(events[i].inEvent, this);
            }
        }

        public void DoChangeSprite(TagSprite info)
        {
            GameObject[] objs = GameObject.FindGameObjectsWithTag(info.tag);
            //Debug.Log(objs.Length);
            foreach (GameObject o in objs)
            {
                SpriteRenderer s = o.GetComponent<SpriteRenderer>();
                if (s != null)
                {
                    s.sprite = info.sprite;
                    s.color = info.color;
                }
                Image i = o.GetComponent<Image>();
                if (i != null)
                {
                    i.sprite = info.sprite;
                    i.color = info.color;
                }
            }
        }
        public override void Receive(string eventName, GameObject obj)
        {
            for (int i = 0; i < events.Length; ++i)
            {
                TagEvent e = events[i];
                if (eventName.Equals(e.inEvent))
                {
                    foreach (TagSprite t in e.tagInfo)
                    {
                        DoChangeSprite(t);
                    }
                }
            }
        }
    }
}