﻿namespace FSM
{
    /// <summary>
    /// Definition of the <see cref="TraceLevel"/> enumeration.
    /// </summary>
    public enum TraceLevel
    {
        /// <summary>
        /// Log nothing
        /// </summary>
        None = 0,

        /// <summary>
        /// Basic log.
        /// </summary>
        Basic = 1,

        /// <summary>
        /// Full log to diagnostic.
        /// </summary>
        Diagnostic = 2
    }
}
