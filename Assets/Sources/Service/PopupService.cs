﻿using Commands;
using Commands.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using ViewModels;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class PopupService : AFeatureService
    {
        private IPopupViewModel _currentPopup = null;
        private Dictionary<string, IPopupViewModel> _popupsById = new Dictionary<string, IPopupViewModel>();

        public Action<PopupService, IPopupViewModel> onPopupOpen  = null;
        public Action<PopupService, IPopupViewModel> onPopupClose = null;

        public IPopupViewModel this[string popupId]
        {
            get
            {
                IPopupViewModel popup = null;
                if ( _popupsById.TryGetValue( popupId, out popup ) )
                {
                    return popup;
                }

                return null;
            }
        }

        public IPopupViewModel currentPopup
        {
            get
            {
                return _currentPopup;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PopupService"/> class.
        /// </summary>
        public PopupService()
        {

        }

        [DispatcherService.HandleCommand(commandType = typeof(ShowPopupCommand))]
        private void OnShowPopup(ICommand command)
        {
            ShowPopupCommand selectCommand = command as ShowPopupCommand;

            IPopupViewModel popup = this[ selectCommand.popupId ];
            if ( popup != null )
            {
                if ( currentPopup != null &&
                     popup != currentPopup &&
                     currentPopup.isVisible == true )
                {
                    currentPopup.Hide();
                }

                popup.Show();

                SetCurrentPopup( popup );
            }
        }
        
        [DispatcherService.HandleCommand(commandType = typeof(ClosePopupCommand))]
        private void OnClosePopup(ICommand command)
        {
            ClosePopupCommand selectCommand = command as ClosePopupCommand;

            IPopupViewModel popup = this[ selectCommand.popupId ];
            if ( popup != null )
            {
                popup.Hide();
            }

            if ( popup == this.currentPopup )
            {
                SetCurrentPopup( null );
            }
        }
        
        [DispatcherService.HandleCommand(commandType = typeof(RegisterPopupCommand))]
        private void RegisterPopup(ICommand command)
        {
            RegisterPopupCommand registerCommand = command as RegisterPopupCommand;
            IPopupViewModel popup = registerCommand.sender.model as IPopupViewModel;

            if ( _popupsById.ContainsKey( popup.id ) )
            {
                return;
            }

            popup.PropertyChanged += OnPopupPropertyChanged;
            _popupsById.Add( popup.id, popup );
        }

        private void OnPopupPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            IPopupViewModel popup = sender as IPopupViewModel;

            if ( args.PropertyName == "isVisible" )
            {
                if ( popup.isVisible )
                {
                    OnPopupOpen( popup );
                }
                else
                {
                    OnPopupClose( popup );
                }
            }
        }
        
        [DispatcherService.HandleCommand(commandType = typeof(UnregisterPopupCommand))]
        public bool UnregisterPopup(ICommand command)
        {
            UnregisterPopupCommand unregisterCommand = command as UnregisterPopupCommand;
            IPopupViewModel popup = unregisterCommand.sender.model as IPopupViewModel;

            popup.PropertyChanged -= OnPopupPropertyChanged;
            
            return _popupsById.Remove( popup.id );
        }

        private void SetCurrentPopup(IPopupViewModel popup)
        {
            _currentPopup = popup;
        }

        private void OnPopupOpen(IPopupViewModel popup)
        {
            if ( onPopupOpen != null )
            {
                onPopupOpen( this, popup );
            }
        }

        private void OnPopupClose(IPopupViewModel popup)
        {
            if ( onPopupClose != null )
            {
                onPopupClose( this, popup );
            }
        }
    }
}
