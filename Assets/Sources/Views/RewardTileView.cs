﻿using Models;
using System;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class RewardTileView : MonoBehaviour, IItemWidget
    {
        [SerializeField]
        private Text _name     = null;

        [SerializeField]
        private Image _image   = null;

        [SerializeField]
        private Text _amount   = null;

        private RewardModel _model = null;

        public RewardModel model
        {
            get
            {
                return _model;
            }
        }

        public uint id
        {
            get
            {
                if ( _model != null )
                {
                    return _model.id;
                }

                return 0;
            }
        }

        public event Action<IItemWidget> onClick;

        void OnDestroy()
        {
            Dispose();
        }

        public void SetReward(RewardModel model)
        {
            _model = model;

            if ( _name != null )
            {
                _name.text = model.name;
            }

            if ( _image != null )
            {
                Texture2D imageTexture = Resources.Load( string.Format( "{0}{1}", Constants.TEXTURE_REWARD_PATH, model.image ) ) as Texture2D;
                if ( imageTexture != null )
                {
                    Rect textureRect = new Rect(0, 0, imageTexture.width, imageTexture.height);
                    _image.sprite = Sprite.Create( imageTexture, textureRect, new Vector2( 0.5f, 0.5f ) );

                }
            }

            if ( _amount != null )
            {
                _amount.text = string.Format( "x{0}", model.amount );
            }
        }

        public void Dispose()
        {
            
        }
    }
}
