﻿using Commands;

namespace Service
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public interface IFeatureService : IService, ICommandHandler
    {
        
    }
}
