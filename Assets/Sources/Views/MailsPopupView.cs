﻿using Utils;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public class MailsPopupView : ASettingsPopupView
    {
        public override string id
        {
            get
            {
                return Constants.MAILS_POPUP_ID;
            }
        }
    }
}
