﻿using Commands.Popup;
using Menu.Buttons;
using Service;
using System;
using System.ComponentModel;
using UnityEngine;
using ViewModels;

namespace Views
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class APopupView : AView
    {
        [SerializeField]
        private GameObject  _root          = null;
        
        [SerializeField]
        private ImageButton _closeButton   = null;

        [SerializeField]
        private GameObject  _contentParent = null;

        [SerializeField]
        private GameObject  _contentPrefab = null;

        public event Action<APopupView> onOpen  = null;
        public event Action<APopupView> onClose = null;

        private IPopupViewModel _viewModel = null;

        public override IViewModel model
        {
            get
            {
                return _viewModel;
            }
        }

        /// <summary>
        /// Gets the popup unique identifier.
        /// </summary>
        public abstract string id
        {
            get;
        }

        protected virtual IPopupViewModel CreateViewModel()
        {
            return new DefaultPopupViewModel( id );
        }

        private void Start()
        {
            if ( _closeButton != null )
            {
                _closeButton.onClick += OnCloseClick;
            }

            if ( _contentPrefab != null &&
                 _contentParent != null )
            {
                Instantiate( _contentPrefab, _contentParent.transform );
            }

            _viewModel = CreateViewModel();
            _viewModel.PropertyChanged += OnViewModelPropertyChanged;

            Dispatch( new RegisterPopupCommand( this ) );

            OnCustomStart();
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if ( args.PropertyName == "isVisible" )
            {
                if ( _viewModel.isVisible )
                {
                    Show();
                }
                else
                {
                    Hide();
                }
            }
        }

        protected virtual void OnCustomStart()
        {

        }

        private void OnDestroy()
        {
            if ( _viewModel != null )
            {
                _viewModel.PropertyChanged -= OnViewModelPropertyChanged;
            }

            if ( _closeButton != null )
            {
                _closeButton.onClick += OnCloseClick;
            }

            OnCustomDestroy();
        }

        protected virtual void OnCustomDestroy()
        {

        }

        private void OnCloseClick(ABaseButton button)
        {
            Dispatch( new ClosePopupCommand( this, id ) );
        }

        private void Show()
        {
            _root.SetActive( true );

            OnCustomShow();

            if ( onOpen != null )
            {
                onOpen( this );
            }
        }

        protected virtual void OnCustomShow()
        {

        }

        private void Hide()
        {
            _root.SetActive( false );

            OnCustomHide();

            if ( onClose != null )
            {
                onClose( this );
            }
        }

        protected virtual void OnCustomHide()
        {

        }
    }
}
