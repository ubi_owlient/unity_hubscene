﻿using Enums;
using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public sealed class MessageModel
    {
        private uint   _id       = 0;
        private bool   _isRead   = false;
        private string _sender   = null;
        private string _title    = null;
        private string _message  = null;
        private MessagePriorityEnum _priority = MessagePriorityEnum.NONE;
        private IEnumerable<RewardModel> _rewards = null;

        public uint id
        {
            get
            {
                return _id;
            }
        }

        public bool isRead
        {
            get
            {
                return _isRead;
            }
            set
            {
                _isRead = value;
            }
        }

        public string sender
        {
            get
            {
                return _sender;
            }
        }

        public string title
        {
            get
            {
                return _title;
            }
        }

        public string message
        {
            get
            {
                return _message;
            }
        }

        public MessagePriorityEnum priority
        {
            get
            {
                return _priority;
            }
        }

        public IEnumerable<RewardModel> rewards
        {
            get
            {
                return _rewards;
            }
        }

        public MessageModel(uint id, string sender, string title, string message, MessagePriorityEnum priority, IEnumerable<RewardModel> rewards = null)
        {
            _id = id;
            _sender = sender;
            _title = title;
            _message = message;
            _priority = priority;
            _rewards = rewards;
        }
    }
}
