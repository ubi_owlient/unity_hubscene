﻿using Commands;
using FeatureModels;
using Service;
using System;
using UnityEngine;

namespace FSM
{
    /// <summary>
    /// Author: jlarbi
    /// </summary>
    public abstract class ABaseState : AOwnedState<IServiceManager>, ICommandHandler
    {
        protected string _stateName = string.Empty;

        public override void OnInitialize()
        {
            base.OnInitialize();

            DispatcherService dispatcherService = Owner.GetService<DispatcherService>();
            if ( dispatcherService != null )
            {
                dispatcherService.RegisterHandler( this );
            }
        }

        public override void OnEnter()
        {
            base.OnEnter();

            Debug.Log( string.Format( "Entering {0}({1}) state...", _stateName, id ) );
        }

        public override void OnEnter(object[] args)
        {
            base.OnEnter(args);

            Debug.Log( string.Format( "Entering {0}({1}) state...", _stateName, id ) );
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            Debug.Log( string.Format( "In {0}({1}) state...", _stateName, id ) );
        }

        public override void OnExit()
        {
            base.OnExit();

            Debug.Log( string.Format( "Exiting {0}({1}) state...", _stateName, id ) );
        }

        public IFeatureModel GetModel(Type modelType)
        {
            return Owner.GetService<ModelService>().GetModel( modelType );
        }

        public T GetModel<T>() where T : class, IFeatureModel
        {
            Type modelType = typeof(T);
            return GetModel( modelType ) as T;
        }
    }
}
