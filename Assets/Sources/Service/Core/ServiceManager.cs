﻿using System;
using Extensions;
using System.Collections.Generic;
using UnityEngine;

namespace Service
{
    /// <summary>
    /// Service builder delegate prototype definition.
    /// </summary>
    /// <param name="serviceType">The service type to create.</param>
    /// <param name="parameters">The parameter(s) to build the service.</param>
    /// <returns></returns>
    public delegate IService ServiceBuilderDelegate(Type serviceType, params object[] parameters);

    /// <summary>
    /// Author: jlarbi
    /// </summary>
    internal sealed class ServiceManager : IServiceManager
    {
        #region Fields

        /// <summary>
        /// Stores the base service interface type.
        /// </summary>
        private static Type _serviceBaseType = typeof(IService);

        /// <summary>
        /// Stores the service builder.
        /// </summary>
        private ServiceBuilderDelegate _builder = null;

        /// <summary>
        /// Stores the service manager unique instance.
        /// The variable is declared to be volatile to ensure that assignment to the 
        /// instance variable completes before the instance variable can be accessed.
        /// </summary>
        private static volatile ServiceManager sInstance;

        /// <summary>
        /// Stores the sync root to lock on the manager rather than locking on the
        /// type itself to avoid deadlocks.
        /// </summary>
        private static object sSyncRoot = new object();

        /// <summary>
        /// Stores the registered services
        /// </summary>
        private Dictionary<Type, IService> _registeredServices;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the service manager handle.
        /// </summary>
        public static ServiceManager Instance
        {
            get
            {
                // This double-check locking approach solves the thread concurrency problems
                if ( sInstance == null )
                {
                    // Lock on
                    lock ( sSyncRoot )
                    {
                        // Delay instantiation until the object is first accessed
                        if ( sInstance == null )
                        {
                            sInstance = new ServiceManager();
                        }
                    }
                }

                return sInstance;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceManager"/> class.
        /// </summary>
        private ServiceManager()
        {
            _registeredServices = new Dictionary<Type, IService>();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes the service manager.
        /// </summary>
        /// <param name="builder"></param>
        public void Initialize(ServiceBuilderDelegate builder = null)
        {
            _builder = builder != null ? builder : delegate(Type serviceType, object[] parameters)
            {
                IService newService = serviceType.CreateInstance( parameters ) as IService;
                return newService;
            };

            PrefetchServices();
        }

        /// <summary>
        /// Creates the core services.
        /// </summary>
        private void PrefetchServices()
        {
            IEnumerable<Type> types = typeof(IService).GetInheritedTypes();
            foreach ( Type type in types )
            {
                IService service = CreateService( type );
                if ( service != null )
                {
                    service.Initialize();

                    this._registeredServices[ type ] = service;
                }
            }
        }

        /// <summary>
        /// Retrieves the requested service.
        /// </summary>
        /// <param name="type">The service type to look for.</param>
        /// <returns>The found service, NULL otherwise.</returns>
        public IService GetService(Type type)
        {
            if ( type == _serviceBaseType )
            {
                // Avoid crap call.
                return null;
            }

            IService service = null;
            if ( this._registeredServices.TryGetValue( type, out service ) )
            {
                return service;
            }

            // If not having in cache the service by matching type
            // try to look for inheritance and cache the service 
            // with that other type.
            var enumerator = this._registeredServices.Keys.GetEnumerator();
            while ( enumerator.MoveNext() &&
                    service == null )
            {
                if ( type.IsAssignableFrom( enumerator.Current ) )
                {
                    service = this._registeredServices[ enumerator.Current ];
                }
            }

            // Create it if does not exist yet.
            if ( service == null )
            {
                service = CreateService( type );

                service.Initialize();
            }

            if ( service != null )
            {
                // Cache for the next time a user attempt to look 
                // for that service using that other type.
                this._registeredServices[ type ] = service;
            }

            return service;
        }

        /// <summary>
        /// Retrieves the requested service.
        /// </summary>
        /// <typeparam name="T">The service type</typeparam>
        /// <returns>The found service, NULL otherwise.</returns>
        public T GetService<T>() where T : class, IService
        {
            Type serviceType = typeof(T);
            return this.GetService( serviceType ) as T;
        }

        /// <summary>
        /// Helper method creating the service corresponding to the given type.
        /// </summary>
        /// <param name="serviceType">The service type to create.</param>
        /// <param name="parameters">The constructor parameters if needed.</param>
        /// <returns>The created service, null if any failure or already exist in the ServiceManager.</returns>
        private static IService CreateService(Type serviceType, params object[] parameters)
        {
            if ( serviceType == null || // Don t allow creation if Null
                 serviceType == _serviceBaseType || // Don t allow creation if crap type
                 serviceType.IsAbstract || // Don t allow creation if abstract
                 ServiceManager.Instance._registeredServices.ContainsKey( serviceType ) ) // Don t allow creation if already existing.
            {
                return null;
            }

            try
            {
                // Gets the default contructor straight. Faster than Activator.Create.
                IService newService = ServiceManager.Instance._builder( serviceType, parameters );
                return newService;
            }
            catch
            {
                Debug.Log( string.Format( "Error trying to create {0}", serviceType ) );
            }

            return null;
        }

        #endregion Methods
    }
}
